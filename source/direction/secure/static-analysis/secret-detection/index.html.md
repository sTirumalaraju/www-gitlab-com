---
layout: markdown_page
title: "Category Direction - Secret Detection"
description: "Secret Detection aims to prevent sensitive information, like passwords, authentication tokens, and private keys being leaked as part of the repository content."
canonical_path: "/secure/static-analysis/secret-detection/"
---

- TOC
{:toc}

## Description

### Overview

A recurring problem when developing applications is that developers may [unintentionally commit secrets and credentials to their remote repositories](https://www.ndss-symposium.org/wp-content/uploads/2019/02/ndss2019_04B-3_Meli_paper.pdf). If other people have access to the source, or if the project is public, this sensitive information is then exposed and can be leveraged by malicious users to gain access to resources like deployment environments. This has very real and tangible costs including: breached application data through credential leaks and compute costs for resources spun up and abused with your cloud resource API keys. [GitLab's Security Trends analysis](https://about.gitlab.com/blog/2020/04/02/security-trends-in-gitlab-hosted-projects/#top-vulnerability-types) found that 18% of projects hosted on GitLab had identified leaked secrets with Secret Detection. 

Secret Detection aims to prevent the unintentional leak of sensitive information including: passwords, authentication tokens, and private keys. It checks source files and configuration files to detect well-known and common patterns that look like secrets or credentials and reports findings that are potentially risky to share.

Secret detection doesn't target a specific language so it can easily be applied to any project. The most common approach to detect secrets is to look for [regex patterns of common credentials](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/scanner/gitleaks/gitleaks.toml) like AWS tokens, API keys, and more. 

Security tools like Secret Detection are best when integrated directly into the [Devops Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/) and every project can benefit from secret scans, which is why we include it on-by-default in [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/). 

### Goal

Overall we want to help developers write better code and worry less about common security mistakes. Our goal is to provide Secret Detection as a part of the standard software development lifecycle (SDLC). This means that Secret Detection is executed every time a new commit is pushed to a branch.

Secret Detection results can be consumed in the merge request, where only new vulnerabilities, introduced by the new code, are shown.

The importance of these goals is validated by GitLab's [2020 DevSecOps Landscape Survey](https://about.gitlab.com/developer-survey/#security). With 3,650 respondents from 21 countries, the survey found: 

* Only 13% of companies give developers access to the results of application security tests.
* Over 42% said testing happens too late in the lifecycle. 
* 36% reported it was hard to understand, process, and fix any discovered vulnerabilities. 

*User success metrics*

The following measures would help us know how successful we are in achieving our goals:
- Tracking the # of Secret Detection configurations (default, out of date, customized)
- Tracking the # of Secret Detection jobs (increase coverage across repos)
- Tracking the # of issues identified by Secret Detection
- Tracking the # of issues resolved that were identified by Secret Detection
- % of Project Files covered by secret detection

## Maturity Plan

 [The Secret Detection Category Maturity level](https://about.gitlab.com/direction/maturity/#secure) is currently at `Minimal`. We plan to mature it to `Viable` by July 2020. 

### Roadmap

 - [Secret Detection Direction Epic](https://gitlab.com/groups/gitlab-org/-/epics/3253)
 - Next Maturity Milestone Epic: [Secret Detection to Viable](https://gitlab.com/groups/gitlab-org/-/epics/1514)
 
### What's Next & Why

We are currently working on making [Secrete Detection a standalone scan type](https://gitlab.com/groups/gitlab-org/-/epics/3098). This move decouples Secret Detection from SAST which provides us more flexibility with how we handle secrets in the future. This sets us up to add support for adding [custom secret detection rules](https://gitlab.com/gitlab-org/gitlab/-/issues/211387) and to experiment with [suggested solutions](https://gitlab.com/groups/gitlab-org/-/epics/2451) to removing identified secrets.

With Secret Detection existing as its own scan type and being one simple security scanner, the [Static Analysis team ](https://about.gitlab.com/handbook/product/product-categories/#static-analysis-group) can use Secret Detection to explore and experiment with more complex changes that we would like to introduce to our other [SAST scanners](https://about.gitlab.com/direction/secure/static-analysis/sast/) like custom rulesets, suggested solutions and more. 

**Why is this important?**

Secret Detection is a simple but very powerful tool to help developers avoid costly mistakes. Secrets are leaked in [source code daily](https://www.zdnet.com/article/over-100000-github-repos-have-leaked-api-or-cryptographic-keys/). In fact, there are publically available tools to watch for leaked secrets like [Shhgit](https://shhgit.darkport.co.uk/). We want to enable as many projects as possible to avoid leaking secrets.

**Differentiation**

Gitlab uniquely has opportunities within the entire DevOps lifecycle. We can integrate across different DevSecOps stages leveraging data, insight, and functionality from other steps to enrich and automate based on Secret Detection findings.
We even allow [integration with partners and competitors](https://about.gitlab.com/partners/#security) to ensure flexibility. This allows teams to choose specific Secret Detection and Management solutions that fit their unique needs without GitLab being a constraint. This centers GitLab as the system of control and allows people to [extend and integrate other solutions](https://docs.gitlab.com/ee/development/integrations/secure.html) into the GitLab DevSecOps workflow.

## Recent Noteworthy Features
- [13.0 - Secret Detection for Full Git History](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/#secret-detection-for-the-full-history-of-a-repository)

## Competitive Landscape

There are a variety of vendors and open source projects offering Secret Detection soltuions: 
* [LookingGlass](https://www.lookingglasscyber.com/products/cyber-threat-intelligence/compromised-credential-information-cci/)
* [GitGuardian](https://gitguardian.com/)
* [Vericlouds](https://www.vericlouds.com/compromised-credentials/)
* [GitHub Secret Scanning](https://help.github.com/en/github/administering-a-repository/about-secret-scanning)
* [Da Tree](https://www.datree.io/)
* [Auth0's Repo Supervisor](https://github.com/auth0/repo-supervisor)
* [Yelp's Detect Secrets](https://github.com/Yelp/detect-secrets)
* [Duo Security's Secret Bridge](https://github.com/duo-labs/secret-bridge)
* [Yelp's Detect Secrets](https://github.com/Yelp/detect-secrets)

GitLab has a unique position to deeply integrate into the development lifecycle, with the ability to leverage CI/CD pipelines to perform the security tests. There is no need to connect the remote source code repository, or to use a different interface.

## Analyst Landscape

We want to engage analysts to make them aware of the security features already available in GitLab. They also perform analysis of vendors in the space and have an eye on the future. We will blend analyst insights with what we hear from our customers, prospects, and the larger market as a whole to ensure we’re adapting as the landscape evolves. 

While Secret Detection is not a standalone category of tools covered by analysts, it is mentioned in Gartner's Application Security Testing (AST) Critical Capabilities. 

* [2020 Gartner Quadrant: Application Security Testing](https://www.gartner.com/en/documents/3984345/magic-quadrant-for-application-security-testing)
* [2020 Gartner Research: How to Deploy and Perform Application Security Testing](https://www.gartner.com/en/documents/3982363/how-to-deploy-and-perform-application-security-testing)
* [2020 GitLab DevSecOps Landscape Survey](https://about.gitlab.com/developer-survey/)
* [Gartner Application Security Testing Reviews](https://www.gartner.com/reviews/market/application-security-testing)
* [2019 Forester State of Application Security](https://www.forrester.com/report/The+State+Of+Application+Security+2019/-/E-RES145135)

## Top Customer Success/Sales Issue(s)
[Full list of Secret Detection issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASecret%20Detection)

## Top user issue(s)
* [Full list of Secret Detection issues with Customer Label](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASecret%20Detection&label_name[]=customer)

## Related Categories
* [Secret Management](https://about.gitlab.com/direction/release/secrets_management/) - Managing Release Configuration Secrets
* [SAST](https://about.gitlab.com/direction/secure/static-analysis/sast/) - Static Application Security Testing
* [Vulnerability Management](https://about.gitlab.com/direction/defend/vulnerability_management/) - Security Dashboard, Reports, and interacting with Vulnerabilities


Last Reviewed: 2020-05-03
Last Updated: 2020-05-03
