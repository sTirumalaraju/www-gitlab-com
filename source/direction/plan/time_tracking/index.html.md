---
layout: markdown_page
title: "Category Direction - Time Tracking"
description: View the category strategy for Time Tracking in GitLab; which is part of the Plan stage's Project Management group. Learn more!
canonical_path: "/direction/plan/time_tracking/"
---

Last reviewed: 2020-04-13

- TOC
{:toc}

## ⏱ Time Tracking

|          |                                |
| -------- | ------------------------------ |
| Stage    | [Plan](/direction/plan/)       |
| Maturity | [Viable](/direction/maturity/) |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category strategy page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/product-categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->

👋 This is the category strategy for Time Tracking in GitLab; which is part of the Plan stage's [Project Management](/handbook/categories/#project-management-group) group. Please reach out to the group's Product Manager, Gabe Weaver ([E-mail](mailto:gweaver@gitlab.com)), if you'd like to provide feedback or ask any questions related to this product category.

This strategy is a work in progress and everyone can contribute:

- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aproject%20management) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AIssue%20Tracking) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
- Please share feedback directly via email, Twitter, or on a video call.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

#### Purpose

GitLab's mission is to build software so that **everyone can contribute**. Issues are the fundamental medium for enabling collaboration on ideas and tracking that idea as it turns into reality.

#### Essential Intent

The goal of a Category's "Essential Intent" is to provide a concrete, inspirational statement. Another way to think of it is answering this single question -- *"If Time Tracking can be truly excellent at only one thing, what would it be?"* This is Time Tracking's Essential Intent:

> To enable teams to build trust through practicing fiscal responsibility.

#### Target Audience

<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

User personas that will regularly track time:

- [Parker (Product Manager)](/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)
- [Delaney (Development Team Lead)](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
- [Presley (Product Designer)](/handbook/marketing/product-marketing/roles-personas/#presley-product-designer)
- [Sasha (Software Developer)](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
- [Devon (DevOps Engineer)](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
- [Sydney (Systems Administrator)](/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)

Additionally, several of the [buyer personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#buyer-personas) will likely maintain budgets, define resource allocation targets, and view reports associated with time tracking and capacity management.

#### Challenges to address

<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

- While time tracking is a relatively straightforward problem to solve, the real challenge emerges upstream. Organizations are struggling to align enterprise strategy to value.
- In order to determine the ROI for any given initiative, there needs to be a clear way to measure both cost -- which time tracking helps solve -- and captured value. The later is a challenge very few organizations have successfully solved.
- Time Tracking within GitLab is still young. For it to provide true enterprise value, it needs to integrate seamlessly with a larger budgeting, resource allocation, and capacity management toolset.

### Where we are Headed

<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized -->

We've written a [mock press release](/direction/plan/project_management/one_year_plan) describing where we intend to be by 2020-09-01. We will maintain this and update it as we sense and respond to our customers and the wider community.

#### What's Next & Why

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

GitLab has basic time tracking functionality (estimation and actuals) for issues and merge requests. This forms the baseline layer of project management functionality relevant to time tracking, but we have yet to expose time tracking reports or integrate time estimates and time spent into other areas of the product like epics. This is where need to focus next.

These are the issues we will be working on over the next few releases:

- [Time tracking report per person in a given group](https://gitlab.com/gitlab-org/gitlab/issues/18170)
  - **Why:** So that a basic time report for a given group can be generated automatically. We implemented the [first step](https://gitlab.com/gitlab-org/gitlab/issues/10741). Now we will focus on a UI within GitLab to provide more robust time tracking reporting within GitLab.

#### What is Not Planned Right Now

<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

- There are currently no any open issues that cotradict our general direction for this category.

#### Maturity Plan

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/product-categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

This category is currently at the 😊**Viable** maturity level, and our next maturity target is 😁**Complete** by 2020-06-22.

We are tracking our progress against this target via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1803).

### User success metrics

<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->

We are currently using the [loose Stage Monthly Active Users (SMAU) definition](https://about.gitlab.com/handbook/product/product-categories/plan/#metrics) and intend on migrating to the strict definition as soon as we've implemented the necessary telemtry to measure the defined events.

### Why is this important?

<!--
- Why is GitLab building this feature?
- Why impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->

Financial and budget management is a key capability identified by analsyst as something every modern Enterprise Agile Planning (EAP) tool needs to support. Tracking time is still the primary measure that a lot of organizations rely upon to measure velcoity. In the long run, there may be other units of measure such as story points that are used to align scope to budget.

### Competitive Landscape

<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

For simple team based time trackers:

- [Time Doctor](https://www.timedoctor.com/?a_aid=codeinwp)
- [HubStaff](https://hubstaff.com/)
- [Harvest](https://www.getharvest.com/)

Larger companies use Enterprise Resource Planning (ERP) platforms. Among the most popular are:

- [Netsuite ERP](https://www.netsuite.com/portal/products/erp.shtml)
- [SAP ERP](https://www.sap.com/products/erp.html)
- [Sage ERP](https://www.sage.com/en-us/erp/)

### Analyst Landscape

<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

- EAP tools, which is a market in which GitLab is competing, must integrate financial and budget management. Most of them don't, so there is a gap between PPM tools, which have more traditional support for budgets and resource tracking but lack support for modern agile workflows, and EAP tools which have robust support for modern agile workflows but poor support for integrated financial and budgetment management.
- Analytsts have also surfaced that no EAP solution is particularly successful at enabling organizations to track both cost and value produced in a way that enables them to meaningfully measure ROI on a granular level.

### Top Customer Success/Sales issue(s)

<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- Roll up [time spent and time remaining on epics](https://gitlab.com/gitlab-org/gitlab/issues/9476), their parents, and the roadmap.
- [Time tracking API](https://gitlab.com/gitlab-org/gitlab/issues/16724) (👍 65, +1, +6)

### Top user issue(s)

<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Support time logging in commit messages](https://gitlab.com/gitlab-org/gitlab/issues/16543) (👍 100, +7, +6)
- [Show total estimate per user](https://gitlab.com/gitlab-org/gitlab/issues/17159) (👍 81, +1, +6)
- [Add estimated time as an option to quantify milestone progress](https://gitlab.com/gitlab-org/gitlab/issues/4004) (👍 71, +8, +2)
- [Add time tracking information to search results](https://gitlab.com/gitlab-org/gitlab/issues/17146) (👍 51, +3, +0)
- [Add time tracking column totals to Issue Board](https://gitlab.com/gitlab-org/gitlab/issues/18166) (👍 42, +1, +3)
- [Provide buttons to interact with tracking feature](https://gitlab.com/gitlab-org/gitlab/issues/16330) (👍 27, +1), +0

### Top internal customer issue(s)

<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

- [Categorize time spent](https://gitlab.com/gitlab-org/gitlab/issues/20842) (👍 3, +0, +0)
- [Time tracking visualization](https://gitlab.com/gitlab-org/gitlab/issues/10728) (👍 0, +0, +0)

### Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->

For those tracking time, one of our one year goals is to allow individuals to start and stop timers from anywhere within GitLab and more easily attach time estimates to issues.

For those interested in initiative level time spent and time remaining, we need to build out more robust reporting at the group level and expose time tracking information in strategic places like epics and the roadmap.
