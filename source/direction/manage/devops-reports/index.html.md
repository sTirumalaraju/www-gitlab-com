---
layout: markdown_page
title: "Category Direction - DevOps Reports"
canonical_path: "/direction/manage/devops-reports/"
---

- TOC
{:toc}

## Manage Stage

| Category | **DevOps Reports** |
| --- | --- |
| Stage | [Manage](https://about.gitlab.com/handbook/product/product-categories/#manage-stage) |
| Group | [Analytics](https://about.gitlab.com/handbook/product/product-categories/#analytics-group) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-02-12` |

### Introduction and how you can help
Thanks for visiting the direction page for DevOps Reports in GitLab. This page is being actively maintained by the Product Manager for the [Analytics group](https://about.gitlab.com/handbook/product/product-categories/#access-group). If you'd like to contribute or provide feedback on our category direction, you can:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/2640).
1. Find issues in this category accepting merge requests. 

### Overview

![Dev Overview](/images/direction/dev/vsm-framework.png)

With [Value Stream Management](https://about.gitlab.com/direction/manage/value_stream_management/) gaining momentum in the marketplace, GitLab is trying to improve organizations' understanding of how their people and codebase interact in order to improve the speed and quality of software delivery. The key term in Value Stream Management is "value": ultimately, software is being developed in service of a higher level goal - the value that we're delivering to our users. Measuring outcomes, in fact, is the most critical part of anything else we can measure and understand about our SDLC.  

Since GitLab serves teams across the entire DevOps lifecycle, our application has the potential to provide a level of insight that few others can match. While our aspiration is to help organizations draw insights into how the usage of GitLab's different components correlates with and improves efficiency, we hope to go beyond stage-specific analytics (like Planning Analytics). Showcasing the power of a GitLab-powered organization will make it clear how our application is helping you ship software faster than ever before, even providing a quantifiable measure of return on investment. DevOps Reports has two primary goals:
* Tangibly measure the value of GitLab. Our starting point will be [relatively straightforward](https://gitlab.com/gitlab-org/gitlab/issues/193435) measurements of usage, but we plan on iterating toward more valuable measurements of value like [DORA](https://gitlab.com/gitlab-org/gitlab/issues/37139) metrics.
* Drive deeper adoption of the entire DevOps lifecycle inside GitLab. Both self-managed and GitLab.com should be able to compare their software practices with the rest of the community and see clearly actionable areas for improvement.

### Where we are Headed

Our first attempt at showcasing the value of GitLab was [DevOps Score](https://docs.gitlab.com/ee/user/instance_statistics/dev_ops_score.html) (previously named ConvDev Index). This feature was a worthwhile start, but our hope is to draw a clearer relationship between deeper adoption of GitLab and valuable outcomes for your SDLC.

The goal of DevOps is to increase quality, velocity and performance and we are helping customers to improve across these dimenions with [Value Stream Management](https://about.gitlab.com/direction/manage/value_stream_management/). There are multiple levels of detail required to manage and improve bottlenecks, but it's also important to define clear top line metrics that can be comparable across industries and companies. We believe the 4 main DORA metrics for DevOps success are a great start to that, which we will complement with other measures that companies have found powerful in providing a holistic picture of the health of their software development. 

We will continue our journey by conducting research into how the usage of the different stages of GitLab correlates with positive results and attempt to quantify the relationships in a systematic, automated fashion. In the future, we hope we will be able to provide actionable recommendations to improve adoption and software development flows.

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
We believe once developed, the score and related metrics would be of great interest to many Engineering and DevOps Managers, who are seeking to understand the adoption of the tool in more detail and how to better utilize it.

### What's Next & Why
Currently, we're focused on providing a simple perspective on what value GitLab is delivering via an [instance-level activity overview](https://gitlab.com/gitlab-org/gitlab/issues/193435). We'll iterate on this further at the [group-level](https://gitlab.com/gitlab-org/gitlab/issues/205380) for GitLab.com.

### Maturity Plan
This category is currently **Minimal**. The next step in our maturity plan is achieving a **Viable** state of maturity.
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

For DevOps Reports to be Viable, organizations must be able to clearly measure a valuable outcome of their SDLC. 
* You can track our journey toward this level of maturity in the [Viable maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/1499) for this category.
