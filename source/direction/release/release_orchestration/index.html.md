---
layout: markdown_page
title: "Category Direction - Release Orchestration"
description: Release Orchestration is the ability to coordinate complex releases across projects, in an efficient way that leverages as-code behaviors as much as possible. 
canonical_path: "/direction/release/release_orchestration/"
---

- TOC
{:toc}

## Release Orchestration

Release Orchestration is the ability to coordinate complex releases, particularly
across projects, in an efficient way that leverages as-code behaviors as much as
possible. We also recognize that there are manual steps and coordination points
involving human decisions throughout software delivery in the enterprise. In these cases, there are teams and release manager roles dedicated to groundguiding these complex enterprise releases, rather than individuals continuously deploying to production.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARelease%20Orchestration)
- [Overall Vision](/direction/ops#release)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3ARelease%20Orchestration) - [Research Insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3ARelease%20Orchestration)
- [Competitive Analysis](https://gitlab.com/groups/gitlab-org/-/epics/2622)
- [Documentation](https://docs.gitlab.com/ee/user/project/releases/index.html)

## Manual Processes vs. Automation

At GitLab we believe organizations can accomplish these enterprise grade releases elegantly and effectively with automation. We are a powerful platform that unlocks efficiency within DevOps. As such, we avoid adding manual, non-automated controls to the delivery pipeline. 

A related category in the [Configure stage](/direction/configure/) describes one approach for users to embrace an infrastructure as code approach to tasks in the catgeory of [Runbooks](/direction/configure/runbooks/). Release Management will be making contributions to this direction as we begin to support more demands for workflows within the GitLab platform. 

### Compliance and Security in the Release Pipeline

Security, compliance, control and governance of the release pipeline is handled as part of [Release Evidence](/direction/release/release_evidence). Secrets in the pipeline are part of our [Secrets Management](/direction/release/secrets_management) category.  Another category for governance and security within GitLab is [Compliance Management](/direction/manage/compliance-management/), which is inclusive of change management and approval workflows. For governance workflows pertaining to issues and requirements, check out our category of [Requirements Management](/direction/plan/requirements_management) from the [Plan](/direction/plan/) stage. 

## What's Next & Why

Our top focus is making release management easier for users across mutliple projects. The main features that will enable this are release generation from `.gitlab-ci.yml` ([gitlab&2510](https://gitlab.com/groups/gitlab-org/-/epics/2510)), enabling the specification of asset link types via ([gitlab#207257](https://gitlab.com/gitlab-org/gitlab/-/issues/207257)).

Users can now effectively track their Release Plans in context to a release via ([gitlab#9427](https://gitlab.com/gitlab-org/gitlab/issues/9427)), we are going to be investing in supporting advanced controls for deployment windows with our Deploy Freeze MVC via ([gitlab#24295](https://gitlab.com/gitlab-org/gitlab/issues/24295)), enabling users to refine when an environment can be deployed to. 

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)).

We believe that expanding editing a release capability directly from the Release page will progress the maturity to Complete. The Releases page will connect the issues assigned to specific milestone and/or release automatically into the workflow, delivering a single view for a release.

Key deliverables to achieve this are:

- [Add link to runbook in release assets](https://gitlab.com/gitlab-org/gitlab/issues/9427) (Complete)
- [Support More actions in the Release Pages UI](https://gitlab.com/groups/gitlab-org/-/epics/2312) (Complete)
- [Release generation from within `.gitlab-ci.yml`](https://gitlab.com/groups/gitlab-org/-/epics/2510) (13.2)
- [Update "Create Release" flow to re-use the "Edit Release" form](https://gitlab.com/gitlab-org/gitlab/-/issues/214244) (13.2)
- [Set a deploy freeze in the UI](https://gitlab.com/gitlab-org/gitlab/issues/24295) (13.2)
- [Associate group milestones to releases](https://gitlab.com/gitlab-org/gitlab/-/issues/121476) (13.3)
- [Support sharing of environments in a group](https://gitlab.com/gitlab-org/gitlab/-/issues/196168) (13.4)
- [Auto changelog for release](https://gitlab.com/gitlab-org/gitlab/issues/26015) 

## Competitive Landscape

Release orchestration tools tend to have great features for managing releases,
as you'd expect; they are built from the ground up as a release management
workflow tool and are very capable in those areas. Our approach to release
orchestration will be a bit different, instead of being workflow-oriented we
are going to approach release orchestration from a publishing point of view.
What this means is instead of building complicated workflows for your releases,
we will focus more on the artifact of the release itself and embedding the
checks and steps into it. 

[Gartner listed GitLab as a challenger in Application Release Orchestration](/blog/2020/01/16/2019-gartner-aro-mq/), 
celebrating the "single product" offering of GitLab as "cloud-native, with a large and rapidly growing customer base". The blurring of the market with the 
convergence of features across various tools in ARO, CI/CS, PaaS, means functionality like AutoDevops and release automation is advantageously positioned to solve 
the problems of more traditional ARO solutions. [Forrester recently listed GitLab as a Strong Performer for the Continous Delivery and Release Automation Wave](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Delivery+And+Release+Automation+Q2+2020/-/E-RES157265), highlighting the advanced controls of release ochestration for cloud native applications. GitLab's ranking as a Forrester Strong Performer is reviewed in a [published blog](https://about.gitlab.com/blog/2020/07/08/forrester-cdra2020/). We are looking forward to administration of releases at scale via [gitlab&3298](https://gitlab.com/groups/gitlab-org/-/epics/3298), where we will re-design the current [Environments Dashboard](/ee/ci/environments/environments_dashboard.html) for management of legacy style deployments.

We have conducted a detailed analysis of XebiaLabs' XL Release offering in ([gitlab#2369](https://gitlab.com/groups/gitlab-org/-/epics/2369)). XL Release's elegance of cross team views, administration of deployments and environments as well as actionable metrics for how Releases are performing are all areas of note. We will be able to compete pointedly by offering [deploy freeze capabilities](https://gitlab.com/gitlab-org/gitlab/issues/24295) and [runbook visibility](https://gitlab.com/gitlab-org/gitlab/issues/9427). Where we will strengthen our approach and confidence in head to head scenarios with XL, will in our next validation tracks. In light of this analysis we aim to improve our experience with environment features like roll backs via ([gitlab#198364](https://gitlab.com/gitlab-org/gitlab/issues/198364)), blue green deployments with ([gitlab#14763](https://gitlab.com/gitlab-org/gitlab/issues/14763)) and support a better view at the group-level in with director-level dashboard for Releases in ([gitlab#3277](https://gitlab.com/gitlab-org/gitlab/issues/3277)). 

Despite the [merger of XebiaLabs by CollabNet](https://www.businesswire.com/news/home/20200121006172/en/CollabNet-VersionOne-XebiaLabs-Combine-Create-Integrated-Agile), GitLab will continue to thrive as a result of the single platform, single data model. This is epecially true as we begin to support more non-technical personas by enhancing the UI experience of release orchestration that many enterprises are interested in. 

## Analyst Landscape

Analysts at this time are looking for more quality of life features that make
a difference in people's daily workflows - how does this make my day better?
By introducing features like ([gitlab#26015](https://gitlab.com/gitlab-org/gitlab/issues/26015))
to automatically manage changelogs and release notes as part of releases, we can demonstrate
how our solution is already capable of doing this.

Deploy freezes ([gitlab#24295](https://gitlab.com/gitlab-org/gitlab/issues/24295))
will help compliance teams enforce periods where production needs to remain stable/not change. The 2020 CDRA Wave Analysts featured criteria around scheduling for maintenance windows, which deploy freezes would satisfy.

Legacy deployment methods, calendar view for deployments and being able to see across a portfolio of releases is becoming more and more relevant as Analysts rank competing solutions like [Digital.ai](https://explore.digital.ai/news-media-coverage/digitalai-named-a-leader-in-gartner-magic-quadrant-for-8th-time-in-a-row) and [Cloudbees](https://www.previous.cloudbees.com/press/cloudbees-named-one-g2%E2%80%99s-top-50-enterprise-products-2020). Better analytics and reporting, such as the CI/CD dashboard in [gitlab#](https://gitlab.com/gitlab-org/gitlab/-/issues/199739) and supporting DORA 4 metrics via [gitlab#37139](https://gitlab.com/gitlab-org/gitlab/-/issues/37139), will move the needle for these audiences.

## Top Customer Success/Sales Issue(s)

In the same vein as ([gitlab#26015](https://gitlab.com/gitlab-org/gitlab/issues/26015)), issues that will support the ease of use of releases across multiple projects, such as associating releases to group milestones in [gitlab#121476](https://gitlab.com/gitlab-org/gitlab/-/issues/121476) are becoming extremely relevant for our customers. 

Our users and organizations leveraging GitLab at scale, are struggling with the current permission paradigm. We have received lots of traction on adding a deployer role via [gitlab#201898](https://gitlab.com/gitlab-org/gitlab/-/issues/201898). 

## Top Customer Issue(s)

Implementing ([gitlab#36133](https://gitlab.com/gitlab-org/gitlab/-/issues/36133)),
which makes creation of the release package an inline part of the
`.gitlab-ci.yml`, will make this feature feel much more mature and
production-ready (even if it is already really usable). 

A second top customer request is to auto-generate release notes from annotated tags via ([gitlab#15563](https://gitlab.com/gitlab-org/gitlab/issues/15563)).

Lastly, we have validated we need to support adding assets to a release ([gitlab#27300](https://gitlab.com/gitlab-org/gitlab/issues/27300)) and are looking to support uploading files to the Release in [gitlab#208712](https://gitlab.com/gitlab-org/gitlab/-/issues/208712) by creating a generic raw package registry. The connection of releases and assets will improve the richness of the release page, helping users see the full picture of their release in a single place. 

## Top Internal Customer Issue(s)

A lot of interest has been expressed in adding the ability to clean up stale environments in the UI ([gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724)).

## Top Vision Item(s)

The pain of navigating and triaging release tasks across projects within groups have also been researched in ([gitlab#197114](https://gitlab.com/gitlab-org/gitlab/-/issues/197114)). We are looking to make coordinating deployments in GitLab easier to be implemented by supporting environments at the Group via ([gitlab#196168](https://gitlab.com/gitlab-org/gitlab/issues/196168)) alongside association of Releases to Group Milestones via ([gitlab#121476](https://gitlab.com/gitlab-org/gitlab/-/issues/121476)).

This YouTube video on the Release Stage vision, captures much of the longer term focuses for usability, maturity, and nimbleness of orchestrating releases in GitLab:

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/pzGCishRoh4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

We are currently designing and thinking about how to better connect the [Package](/direction/ops/#package) stage with Releases. We are seeing a great use case for adding semantic versioning to Releases as outlined in [gitlab#213838](https://gitlab.com/gitlab-org/gitlab/-/issues/213838), as well improving the [Versioned Dependencies](/direction/versioned-dependencies/) category by implementing better connections between the Release's page of project and the packages created via [gitlab#215390](https://gitlab.com/gitlab-org/gitlab/-/issues/215390).
