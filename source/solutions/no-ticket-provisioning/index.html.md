---
layout: markdown_page
title: "No Ticket Provisioning"
canonical_path: "/solutions/no-ticket-provisioning/"
---
## Tickets Slow Down Delivery
Traditional enterprise approaches expect  IT Ops to manage and operate routine work for the organization.  Typically the process to get things done requires the administrative overhead of 'IT tickets' to request and perform repetitive tasks such as requesting infrastructure, configuring an application or provisioning a test environment.   The ticket mindset introduces complexity and delays in provisioning, deployment, scaling, and other software delivery and management activities.  

**In simple terms:  tickets = slow.**

In the world of DevOps, where the speed of delivery is critical, **automation** and **self-service** are keys to success.

## GitLab Enables Self Service
As a single application designed to support the entire DevOps lifecycle, GitLab enables teams to eliminate unnecessary tickets, and unleash the power of self service:

- Automatically provision and deploy [review applications](https://about.gitlab.com/product/review-apps/) for testing.  Then discard.
![review app](https://docs.gitlab.com/ee/ci/review_apps/img/review_apps_preview_in_mr.png){: .margin-top20 .margin-bottom20 }
- Provision and [deploy production applications](https://docs.gitlab.com/ee/user/project/deploy_boards.html).
![deploy board](https://docs.gitlab.com/ee/user/project/img/deploy_boards_landing_page.png){: .margin-top20 .margin-bottom20 }
- Provision and manage containers with GitLab's built in [container registry](https://docs.gitlab.com/ee/user/project/container_registry.html).
![container registry](https://docs.gitlab.com/ee/user/project/img/container_registry.png){: .margin-top20 .margin-bottom20 .image-width50pct }
- Manage [Kubernetes from GitLab](https://docs.gitlab.com/ee/user/project/clusters/#adding-and-creating-a-new-gke-cluster-via-gitlab)
![Manage Kubernetes](https://docs.gitlab.com/ee/user/project/clusters/img/k8s_cluster_monitoring.png){: .margin-top20 .margin-bottom20 }
- Instrument and enable [application performance monitoring and alerts](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html)
![Application performance and alerts](https://docs.gitlab.com/ee/user/project/integrations/img/prometheus_alert.png){: .margin-top20 .margin-bottom20 .image-width50pct }


<div class="full-width">
  <a href="https://about.gitlab.com/sales/" class="btn cta-btn orange">Connect with us and learn more how Gitlab can help</a>
</div>
