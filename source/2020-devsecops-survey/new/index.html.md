---
layout: blank
title: "Take GitLab's 2020 DevSecOps survey"
description: "And enter to win prizes!"
canonical_path: "/2020-devsecops-survey/"
suppress_header: true
twitter_image: '/images/tweets/2020-devsecops-survey.png'
---

<iframe src="https://gitlab.fra1.qualtrics.com/jfe/form/SV_3yBJ72xIvbi8YrH"></iframe>
<style type="text/css">
  iframe {
    height: 100vh;
    width: 100vw;
  }
</style>
