---
layout: markdown_page
title: "Working Groups"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's a Working Group?

Like all groups at GitLab, a [working group](https://en.wikipedia.org/wiki/Working_group) is an arrangement of people from different functions. What makes a working group unique is that it has defined roles and responsibilities, and is tasked with achieving a high-impact business goal. A working group disbands when the goal is achieved (defined by exit criteria) so that GitLab doesn't accrue bureaucracy.

## Roles and Responsibilities

| Role                  | Required                  | Responsibility                                                                                                                       |
|-----------------------|---------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Facilitator           | Yes                       | Assembles the working group, runs the meeting, and communicates results                                                              |
| Executive Stakeholder | Yes                       | An Executive or [Senior Leader](/company/team/structure/#senior-leaders) interested in the results, or responsible for the outcome  |
| Functional Lead       | Yes (for each function)   | Someone who represents their entire function to the working group                                                                    |
| Member                | No                        | Any subject matter expert                                                                                                            |

**Guidelines**

* An executive sponsor is required, in part, to prevent proliferation of working groups
* A person should not facilitate more than one concurrent working group
* Generally, a person should not be a part of more than two concurrent working groups in any role
* It is highly recommended that anyone in the working group with OKRs aligns them to the effort

## Process

* Preparation
  * Create a working group page
  * Assemble a team from required functions
  * Create an agenda doc public to the company
  * Create a Slack channel (with `#wg_` prefix) that is public to the company
  * Schedule a recurring Zoom meeting
* Define a goal and exit criteria
* Gather metrics that will tell you when the goal is met
* Organize activities that should provide incremental progress
* Ship iterations and track the metrics
* Communicate the results
* Disband the working group
  * Celebrate. Being able to close a working group is a thing to be celebrated!
  * Move the working group to the "Past Working Groups" section on this page
  * Update the working group's page with the close date and any relevant artifacts for prosperity
  * Archive the slack channel
  * Delete the recurring calendar meeting

## Participating in a Working Group

If you are interested in participating in an active working group, it is generally recommended that you first communicate with your manager and the facilitator and/or lead of the working group. After that, you can add yourself to the working group member list by creating a MR against the specific working group handbook page.

## Active Working Groups (alphabetic order)

* [Engineering Career Matrices](/company/team/structure/working-groups/engineering-career-matrices/)
* [gitlab-ui (CSS and Components)](/company/team/structure/working-groups/gitlab-ui/)
* [IACV - Delta ARR](/company/team/structure/working-groups/iacv-delta-arr/)
* [Minorities in Tech (MIT) Mentoring Program](/company/team/structure/working-groups/mit-mentoring/)
* [Multi-Large](/company/team/structure/working-groups/multi-large/)
* [Real-Time](/company/team/structure/working-groups/real-time/)
* [Recruiting SSOT](/company/team/structure/working-groups/recruiting-ssot/)
* [S-1 Data Quality Process](/company/team/structure/working-groups/s1-dqp/)
* [Self-managed Scalability](/company/team/structure/working-groups/self-managed-scalability/)
* [Simplify Groups & Projects](/company/team/structure/working-groups/simplify-groups-and-projects/)
* [SOX PMO](/company/team/structure/working-groups/sox/)
* [Webpack (Frontend build tooling)](/company/team/structure/working-groups/webpack/)

## Past Working Groups (alphabetic order)

* [CI Queue Time Stabilization](/company/team/structure/working-groups/ci-queue-stability/)
* [Development Metrics](/company/team/structure/working-groups/development-metrics/)
* [Githost Migration](/company/team/structure/working-groups/githost-migration/)
* [GitLab.com Cost](/company/team/structure/working-groups/gitlab-com-cost/)
* [GitLab.com Revenue](/company/team/structure/working-groups/gitlab-com-revenue)
* [Isolation](/company/team/structure/working-groups/isolation/)
* [Licensing and Transactions Improvements](/company/team/structure/working-groups/licensing-transactions-improvements/)
* [Log Aggregation](/company/team/structure/working-groups/log-aggregation/)
* [Logging](/company/team/structure/working-groups/logging/)
* [Performance Indicators](/company/team/structure/working-groups/performance-indicators/)
* [Secure Offline Environment Working Group](/company/team/structure/working-groups/secure-offline-environment/)
* [Self-Managed Scalability](/company/team/structure/working-groups/self-managed-scalability/)
* [Sharding](/company/team/structure/working-groups/sharding)
* [Single Codebase](/company/team/structure/working-groups/single-codebase/)
* [Telemetry](/company/team/structure/working-groups/telemetry/)
