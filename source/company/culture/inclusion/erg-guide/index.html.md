---
layout: markdown_page
title: "ERG - Employee Resource Group Guide"
---
 
## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction
On this page you will be provided an overview of what is needed to start and sustain a GitLab ERG (Employee Resource Group)

## Definition of an ERG - Employee Resource Group
ERGs are voluntary, team member-led groups focused on fostering diversity, inclusion and belonging within GitLab. These groups help team members build stronger internal and external connections; offer social, educational, and outreach activities; create development opportunities for future leaders; and increase engagement among team members.

### What is not an ERG at GitLab
There are many types of groups and not all of them meet the criteria of being a GitLab supported ERG. Here are some examples of those that would not be considered ERGs here at GitLab: 
* Groups formed for the purpose of opposing other groups
* Groups formed to promote political affiliations 
* Groups formed around strictly social activities
* Groups of passion such as those formed around a passion for coffee, exercise, book reading, photography, etc. 

**NOTE:** “GitLab supported ERG” means the group is formally recognized by the company as a GitLab ERG. 

## How to Join Current ERGs and their Slack Channels

The following groups have completed the process to be an ERG and received formal support as part of the [DIB framework](https://about.gitlab.com/company/culture/inclusion/#ergs---employee-resource-groups). Click the signup link (GitLab team members only) to join:

| **ERG** | **Slack Channel** | **Sign Up** |
| ------ | ------ | ------ |
| GitLab Pride |  #lgbtq | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/forum/#!forum/erg-pride) |
| GitLab Women+ |  #women | [Sign up for future meetings (google form)](https://docs.google.com/forms/d/e/1FAIpQLSdSiwRUYdEQmCRlLBydBRHmwd9R5-3OOMyVrxEHc2XedahbDA/viewform?usp=sf_link) |
| GitLab MIT - Minorities in Tech |  #minoritiesintech | [Sign up for future meetings (google form)](https://docs.google.com/forms/d/e/1FAIpQLSeISCkah2AO1dYKeXVGF6LkkahsxD4xIOc84QKz1SQpuXkZAQ/viewform?usp=sf_link) |
| GitLab DiversABILITY |  #diverse_ability | [Sign up for future meetings (google form)](https://docs.google.com/forms/d/e/1FAIpQLSfCGclrZJoLkOnd3tYpS8Z4K5-7MW7geTAv1dFgXHi0C_8RjQ/viewform?usp=sf_link) |
| GitLab Gender Minorities | #gender-minorities-employee-resource-group | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/forum/?hl=en#!forum/genderminoritieserg) |
| GitLab Generational Differences | #erg_generational_differences | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/g/Generational_Differences_ERG) |


## Benefits of ERGs
In general ERGs are an excellent support system and key to providing awareness, respect, and building diversity, inclusion and belonging within the workplace. These groups are a proven way to increase cultural competency, retention of team members, provide marketplace insights to the business, attract diverse talent, and more.  The endorsement of ERGs gives team members the opportunity to identify common interests, and decide how they can be shared with others.

### GitLab Benefits of ERGs
* Grow GitLab’s business and the company from DIB branding as it’s top value which will appeal to customers and attract new team members.
* Support GitLab values and business goals, including the Company’s commitment to foster an inclusive work environment.
* Support GitLab’s diversity initiatives, aspirations and goals.
* Foster communications between GitLab and its team members.
* Provide mentoring and educational and professional development opportunities for GitLab team members

### Team Member Benefits of ERGs
* ERGs introduce new and current team members to GitLab's culture and helps to build and maintain engagement.
* ERGs offer team members togetherness, camaraderie and connections to GitLab giving them a sense of belonging.  
* You will have the opportunity to develop your professional and leadership skills, build relationships across the company and regions, connect with possible mentors/mentees, raise awareness for underrepresented groups at GitLab and make a difference at work and in your communities. 
* ERGs provide team members with opportunities for development, showcasing their skills and developing their own brand within GitLab.
* Team members will also have opportunities to obtain mentoring, networking, providing positive impacts to the business, and channeling their voices to advocate for change.

## Forming an ERG
ERGs support our diversity, inclusion and belonging framework, maintain an open forum for the exchange of ideas, and serve as a source of educational and professional development opportunities for GitLab team members. It is expected that all GitLab supported ERGs will participate in initiatives that focus on the following group elements:

1. ERG Member Development: Activities that further the development of group members. Examples could include:
   * Developing and delivering developmental opportunities for members 
   * Potential career development events and activities  
   * Identifying effective mentoring opportunities  
   * Building a network of development resources that are easily accessible by members

2. Outreach/Business Development: Connecting with groups beyond GitLab
   * Establishing internal and external business partnerships  
   * Representing GitLab at industry events  
   * Working with external communities to help GitLab achieve market presence and leadership brand

3. Awareness and Education: Raising awareness and educating all team members. 
   * Events/Initiatives that bring awareness and education of the ERG to the company
   * Engagement of Allies and GitLab team members with the ERG

4. Recruiting/Retention: Promoting, growing, and developing the ERG group as a whole.
   * Establishing partnerships with universities and or STEM programs 
   * Working with Recruiting to identify sourcing and recruiting opportunities
   * Creating initiatives that attract related diversity dimension talent.


Possible events: 
* Webinars
* Panel discussions
* Effective Presentations
* Effective Communications
* Virtual Lunch n Learns
* Speed Networking with Executives
* “Meet the ERG” (informational outreach event for employees to learn more about the DIB Community)
* Volunteer activities (could be co-sponsored with other ERGs)
* Recognition events (ex.  A meeting to celebrate the ERG annual achievements, award success of individuals)


### Here’s What You Will Need to Get Started
* Contact Candace Williams DIB Manager cwilliams@gitlab.com
* Take an opportunity to fully read the guide here provided
* Be prepared to discuss the ERG diversity dimension that you would like to start.
* A detailed explanation of why GitLab should support the ERG 
* A draft of a possible mission statement for the ERG


## Let's Get Started

### Naming and branding for the ERG 

All names, because they are visible externally and could compete with other projects, products and or not be a good representation of GitLab must be approved by Legal and Brand.  You should work with the DIB Manager (Candace Williams cwilliams@gitlab.com) to gain a consensus on ideas.  Keep in mind that names chosen by the ERG may not meet GitLab’s naming and branding standards and may need to be changed.

### Defining your ERGs mission statement
A mission statement is the simplest and clearest way to explain the purpose of your group and how it will achieve its goals. Keep your mission statement short, and use simple terms that everyone understands. Finally, make sure the mission is flexible enough to allow for goals and activities to change over time. Below are some examples of mission statements used by similar groups at other companies:
* Deutsche Bank - Rainbow Group Americas: The DB-Rainbow Group Americas is an organization open to all Deutsche Bank employees to promote an inclusive and productive work environment for gay, lesbian, bisexual, and transgender (GLBT) employees to enhance their professional and personal development in alignment with business objectives.
* General Mills - Black Champions Network: To champion the growth, development and success of all African American employees while maximizing their contribution to General Mills’ goals and objectives.
* ING - Latino Network: The ING Latino Network fosters its members’ development and promotes  cultural awareness within ING’s corporate strategies and objectives.
* Salesforce - Women’s Network:  We are dedicated to building gender Equality in the workplace and beyond through empowering, supporting, and investing in our global community for women and their allies. We are the largest Ohana group with 6000+ members across 30+ hubs globally. Our programs include LeanIn Circles, volunteer opportunities, International Women’s Day events, Woman of the Month series, mentorship opportunities, children’s initiatives, and Women in Technology programs. Our focus is always on improving inclusion and Equality for all on the gender spectrum, to help make Salesforce the best place to work for all.
* Texas Instruments Incorporated—Chinese Initiative: The mission of the Chinese Initiative is to create a work culture in which all people are valued, empowered, and given opportunities to develop and contribute to their full potential, thereby gaining a competitive advantage for Texas Instruments.

### Create a project

As with all GitLab business, we want to dogfood our own product. As such, you should consider creating a GitLab project to manage discussions in issue and
update the repo with mission statement, events, and the like. You should create the repo in the [`gitlab-com`](https://gitlab.com/gitlab-com) group. To see
a project in action, you can check out the [GitLab Pride project](https://gitlab.com/gitlab-com/pride-erg).

### Create a Google group

Managing membership will be greatly simplified by using a Google group. The main benefit is that you can invite the group to any calendar events and users can
join or leave the group on their own. In order to create a Google group, you'll need to create an [access request issue](https://about.gitlab.com/handbook/business-ops/employee-enablement/it-ops-team/access-requests/#slack-google-groups-1password-vaults-or-groups-access-requests)
requesting a new group. There is a template you can use and you can [view an example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/3547)
issue if you'd like. Once you've got the Google group created, you can add users manually or allow them to sign up on their own at the group homepage. You can
look at the [pride-erg](https://groups.google.com/a/gitlab.com/forum/#!forum/erg-pride) for an example of what that might look like.

## ERG Group Members
Membership in an ERG at GitLab is open to everyone, including full-time and part-time team members, interns, and contractors.

### Members
A member is an active participant in the ongoing activities of an ERG. As a global company, the ways that members participate may vary based on location, culture, and preferences. Membership is open to both team members who identify with the diversity dimension that is the community’s focus and allies who wish to advocate and support the mission of the ERG. 

### Allies
An ally is someone who supports, empowers, or stands for another person or a group of people. Through our research, we have found it to be a best practice for all to be inclusive of ally support. When creating an ERG, planning activities, and engaging with members be sure to consider how allies will be involved. 

An ally strives to...
* be a friend
* be a listener
* be open-minded
* have their own opinions
* be willing to talk
* recognize their personal boundaries
* join others with a common purpose
* believe that all persons regardless of age, sex, race, religion, ethnicity, sexual orientation, gender identity and gender expression should be treated with dignity and respect 
* recognize when to refer an individual to additional resources
confront their own prejudices
* recognize their mistakes, but not use them as an excuse for inaction
be responsible for empowering their role in a community
* commit themselves to personal growth in spite of the discomfort it may sometimes cause

As important as it is to define what an ally is in a positive way, it is also helpful to understand the boundaries of an ally's role.

An ally is NOT...
* someone with ready-made answers
* necessarily a counselor or trained to deal with crisis situations
expected to proceed with an interaction if levels of comfort or personal safety have been violated

[Adapted from Human Rights Campaign Establishing an Allies/Safe Zone Program, Human Rights Campaign](https://www.hrc.org/resources/establishing-an-allies-safe-zone-program)

Additional resources on how to be an ally:
* [Allyship at GitLab](https://about.gitlab.com/handbook/communication/ally-resources/)
* [Live Learning Ally Training at GitLab](https://ctb.ku.edu/en/table-of-contents/culture/cultural-competence/be-an-ally/main)
* [Chapter 27. Cultural Competence in a Multicultural World | Section 5. Learning to be an Ally for People from Diverse Gro… 
Straight for Equality](https://ctb.ku.edu/en/table-of-contents/culture/cultural-competence/be-an-ally/main) 
* [Guide to Allyship](http://www.guidetoallyship.com/#the-work-of-allyship)


## ERG LEADERS

### Required of all ERG leaders: 
* Manager support to commit the time and to use this leadership role as a professional development opportunity
* A minimum one-year commitment in the role but understanding this may change to less and can be more.
* Must be a full-time GitLab team members. 

### Qualities of an ERG leader:
* Interest, passion and time to devote their time to the ERG
* Possesses facilitation, team-building, and collaboration skills
* Has a desire to build a presence for the ERG within GitLab
 

## Roles Within the Group
Group Members - At GitLab we all contribute!  Everyone has an opportunity to lead and provide feedback within the group.

**Executive Sponsor** (optional but recommended)  - An executive GitLab team member who is responsible and accountable for strategic support of the group
Share accountability for the success of the DIB group
Participate as an active member of the DIB group
Share information about the DIB group activities with other leaders
Provide insight and guidance to DIB group as needed
Partner with ERG leads on issues, concerns, and resource needs of the community
May provide additional budget

**Lead** - A GitLab team member who is responsible and accountable for strategic direction and operations of the ERG 
* Operational lead of the ERG
* Meets w/ DIB Manager as needed but otherwise quarterly.  This may be more often as a new ERG is forming to consult with DIB Manager for planning and execution.
* Responsible for submitting annual and or quarterly plans (Note: Format to be provided by DIB Manager)
* Along with Co-lead, serves as contact for any team, department, or other GitLab team member requesting partnership or education with the ERG

**Co-Lead** - A GitLab team member who supports the Lead in the strategy and operations of the ERG
* Serve as contact in the absence of the Lead
* Along with Lead, serves as contact for any team, department, or other GitLab team member requesting partnership with the ERG
 
### Optional roles
While not required, we recommend establishing other leadership positions to ensure that the responsibilities of the Lead and Co-Lead remain realistic and success is achievable for the ERG. Here are some example roles we recommend for each ERG that reflect the 4 elements of focus listed above:

Leader of Professional Member Development: Activities that further the development of group members. 

Leaders of Outreach/Business Development: Connecting with communities beyond GitLab

Leader of Awareness and Education: Raising awareness and educating all associates. 

Leader of Recruiting/Retention: Promoting, growing, and developing the ERG as a whole.


### How ERG leaders are selected
The election process should follow GitLab’s fiscal year calendar to ensure the roles are aligned to our strategy. Smaller or recently forming ERGs may choose not to have a formal election if membership can easily determine leadership. 

It is important to monitor the ERGs size to recognize when it has grown too large for an informal election process. Larger ERGs (50 members or more) should use a formal selection process with nominations of some kind, summaries of each candidate’s qualifications shared with ERG members, votes taken on a set date, and vetting process etc as a suggestion but not required. 


### ERG leader terms of service 
ERG leaders are suggested to commit at least one year to their leadership role, with the option for less if a situation arises or more if the ERG members at large would like.  This can also be set up as a rotation of 6 months as well.  The ERG can decide.

### ERG leader succession
Leadership succession is critical to sustaining ERGs and keeping leaders energized. Ideally outgoing leaders will have and overlap with incoming new leaders by acting as advisors to ensure a seamless transition. 

Research suggests developing the next generation of leaders for your ERGs by looking for members who have taken smaller roles in heading up committees or organizing events; speak with them about their interests and encourage them to take on more visible roles within the ERG.


## Tools for ERG Leaders
These resources are here to help you effectively lead and grow an ERG.

### Communications
Communication within ERGs keeps members aware of, involved with, and supportive of the group’s direction and activities. ERGs can use several inlets of communication tools outlined below to keep members informed about meeting times, structure, membership, and updates. 

Communications resources
* Slack Channels
* Google Calendars
* Google Forms
* Email templates
* Invite templates

### What to do If you’re asked to provide your opinion on behalf of GitLab
There may be times that you are asked to comment on the state of DIB at GitLab or your ERG. When or if that happens, please contact/notify PR, Talent Brand and the DIB Manager.  Here are some general best practices that we share are helpful for all GitLab team members to know.
* Don't share information that hasn't already been disclosed publicly. This includes retention and turnover rates, associate demographics, compensation trends, hiring plans or numbers, headcount, new products, corporate strategy, and more.
* If you’re asked about our Diversity, Inclusion & Belonging  stats, refer to our [GitLab identity data](https://about.gitlab.com/company/culture/inclusion/identity-data/)
* Always remember that although we work in transparency, we want to me mindful of GitLab’s reputation and brand.

### ERG Success measurement (Capturing Data)
Measuring the success of the ERG is important for the sustainability of the group and for ensuring the group’s effectiveness. 

Members of the ERGs are encouraged to identify multiple ways the success will be tracked and measured over time. Here are some suggestions for measuring success:
* Reviews and reports on metrics should be submitted quarterly to the DIB Manager and updated to the handbook. We also encourage you to share metrics with the ERG as a whole so there is a shared understanding of expectations, value added, and areas of improvement.
* Conduct an annual review with the DIB Manager, to discuss successes and opportunities for the upcoming year
* Consider reviewing and modifying, if needed, the ERGs mission statement and/or goals (every year or less to move forward iterations with changes in the ERG)
* Identify the right cadence of gathering member feedback via surveys, focus groups, or year-end review sessions. Don’t wait until there is a lack of engagement, ask early and often. 

#### Additional suggestions to measure success (may vary based on the state of the ERG):
* Event attendance
* Number of events hosted
* Feedback from events 
* Blog pages
* ERG page etc
* Tracking ERG membership numbers month to month
* Participation in the external community events
* Volunteer hours contributed by members of the ERG
* Quantitative feedback from ERG members
* Number of ally members (when appropriate to track)
* Google form list sign-ups from the handbook
* Communication methods in place (Slack, mailing list, calendar, etc)


