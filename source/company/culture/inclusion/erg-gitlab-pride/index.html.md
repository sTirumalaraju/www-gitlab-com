---
layout: markdown_page
title: "ERG - GitLab Pride"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction
You will be provided an overview of our remote ERG - Employee Resource Group GitLab Pride 


## Mission




## Leads
* [Alex Hanselka](https://about.gitlab.com/company/team/#ahanselka) 
* [Amber Lammers](https://about.gitlab.com/company/team/#amberlammers)




## Upcoming Events




## Additional Resources