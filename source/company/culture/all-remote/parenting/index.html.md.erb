---
layout: handbook-page-toc
title: "Parenting as a remote worker"
canonical_path: "/company/culture/all-remote/parenting/"
description: Tips and tricks for parents who juggle working from home
twitter_image: "/images/opengraph/Talent-Brand/hiring-day-in-the-life-darren.png"
twitter_image_alt: "GitLab Team Member working from home office, happy to see baby join the video call."
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

![GitLab parenting work from home](/images/opengraph/Talent-Brand/hiring-day-in-the-life-darren.png){: .shadow.medium.center}

On this page, we're detailing considerations for being a parent that works remotely, or works from home. 

Parents who work from home have unique demands and expectations, as well as unique challenges. Being an excellent coworker and an excellent parent is a tall order, particularly when childcare and school are impacted by a global crisis. While success may look different to each individual, it requires support from one's organization, company-wide alignment on [values](/handbook/values/) that enable parents to feel empowered, and a [transparent atmosphere](/company/culture/all-remote/building-culture/) where advice may be shared.   

## Create and reinforce supportive values

At GitLab, we have a sub-value that clarifies [family and friends come first, and work second](/handbook/values/#family-and-friends-first-work-second). Our Diversity, Inclusion & Belonging value includes a sub-value to [make family feel welcome](/handbook/values/#make-family-feel-welcome).

Parents will only get so far in juggling work and parenting without organizational support. It is vital for leaders to create values and workplace flexibility such that a parent does not feel pressured to choose work over family on an ongoing basis. 

## Create a forum for sharing

GitLab expires all Slack messages after 90 days as a remote-first forcing function. This serves multiple purposes. 

1. It forces work to begin and end in GitLab ([the product](/solutions/gitlab-for-remote/))
2. It lowers [anxiety](/company/culture/all-remote/mental-health/) often associated with chat tools by reinforcing a [bias towards asynchronous workflows](/handbook/values/#bias-towards-asynchronous-communication) 
3. It reminds employees that the tool's primary function is [informal communication](/company/culture/all-remote/informal-communication/), where parents can converse about challenges and solutions related to parents. (We're humans first, colleagues second!)

GitLab has several Slack channels where parents can ask questions, share challenges/tips/advice, and offer recommendations. This may include homeschooling programs, useful apps, or suggestions for staying sane during a global pandemic. Companies should consider creating similar. GitLab's [Juicebox Chats](/company/culture/all-remote/informal-communication/#juice-box-chats) have been a delightful tool for encouraging cultural exploration for kids while at home during COVID-19. 

1. `#intheparenthood` ([Join Slack channel](https://gitlab.slack.com/app_redirect?channel=intheparenthood) - *for GitLab team members only*)
2. `#kid-juicebox-chats` ([Join Slack channel](https://gitlab.slack.com/app_redirect?channel=kid-juicebox-chats) - *for GitLab team members only*)

It also serves as a forum to share photos, videos, and stories (depending on one's personal comfort level). As an all-remote team, these serve as windows into the worlds of coworkers and can go a long way in lifting one's spirits and building bonds amongst parents. 

## Create a dedicated work area

For parents with toddlers who may not understand why a parent is home but unable to play or engage, consider working in a space with a door that you can shut. This creates a more obvious separation between work and life. 

If you live in a space where such a division is impractical or impossible, consider working in a coworking space, external office, or shared community home through firms such as [Codi](https://www.codi.com/). GitLab recognizes that not every living space is amenable to remote work, which is why we will [reimburse for external spaces](/handbook/spending-company-money/#coworking-or-external-office--space). 

## Share your schedule

Consider sharing your work calendar with family. This can be done via a subscription to one's digital calendar, having a screen in a common living space that mirrors one's work calendar, or via handwritten notes affixed to the refrigerator. 

By knowing when a parent is in a meeting or heads-down in focus time, it relieves the burden from the parent to continually update family on their status. 

[Busy/available indicators](/company/culture/all-remote/workspace/#busyavailable-indicators) are useful for creating a visible way to show family when you're available/unavailable. 

## No-meeting days and mid-week days off

For team leaders, consider piloting a no-meeting day company-wide (or team-wide) to allow more space for time with children while working remotely. At GitLab, several parents are also shifting their schedules to take a weekday off entirely. In remote settings where more work is conducted asynchronously, it is more feasible to work an unconventional schedule to accommodate children.

## Embrace a non-linear workday

If your work environment allows for it, consider flexing your workday around your kids' schedule(s). This enables you to be present for moments during the day that would be impossible to access if commuting to an office. 

Learn more on [embracing a non-linear workday](/company/culture/all-remote/non-linear-workday/). 

## Invite kids to meetings/work

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">&quot;Some days you&#39;ll be a superhuman parent, some not. No one is grading you. Forget the guilt. Your kids will remember that all this time, you were next to them.&quot;<br><br>- <a href="https://twitter.com/BethLovesATX?ref_src=twsrc%5Etfw">@BethLovesATX</a> on <a href="https://twitter.com/hashtag/RemoteWork?src=hash&amp;ref_src=twsrc%5Etfw">#RemoteWork</a> <a href="https://t.co/aO8A9ypNj6">https://t.co/aO8A9ypNj6</a></p>&mdash; GitLab (@gitlab) <a href="https://twitter.com/gitlab/status/1280949166378758150?ref_src=twsrc%5Etfw">July 8, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

If appropriate, consider inviting your child or children into meetings, or let them work/study in a shared space while you're at work. Remember that [meetings are about the work, not the background](/company/culture/all-remote/meetings/#meetings-are-about-the-work-not-the-background), and kids barging into a meeting is indeed the best distraction in the world. 

## Use Slack/Teams statuses to manage ad hoc needs

With just one or two clicks, you can modify your status on Slack or Microsoft Teams to indicate that you're unavailable. Consider a stop sign, a family emoji, or an alternative that you're comfortable with. This gives your remote coworkers an at-a-glance indication that you're away. You need not go into detail as to what you've had to break away and manage at home. A simple status icon goes a long way in conveying availability and empathy. 

## Block out family time on your calendar

If a work meeting can be scheduled, so can anything else. That could be lunch, time for fitness/meditation, or engaging with your kids. Be deliberate and intentional about putting family functions in your calendar. That could be time for breakfast, driving to/from school, time for homework, or other activities. Two anecdotes on this approach are shared below by GitLab team members.

> What has worked for me is to integrate GitLab into my life, and not the other way around. I set very clear boundaries in my calendar of when is work time and when is family time. Per GitLab's [recommendation](/handbook/communication/#google-calendar), my calendar is viewable by anyone and clearly has time for: `kids test prep`, `visual therapy for one`, `judo pick-up` and other appointments I have outside of work. This prevents team members from scheduling over these instances.
>
> If someone *does* schedule over family time or personal time, I feel confident to decline, unless it is very urgent and I'm blocking it (I always suggest a new time first, before using family time for a meeting). I am also inclined to say that if folks keep scheduling outside of your work time, to make sure you [respond back and highlight that](/handbook/values/#transparency), and check if you can find an alternative time that works for both of you. — [*Nadia V.*](https://gitlab.com/Vatalidis)

> When I joined GitLab, it was great to see things in people's calendars like `baby time`, `homework with kids`, `date night`, etc. Part of [onboarding](/company/culture/all-remote/getting-started/) was [coffee chats](/company/culture/all-remote/informal-communication/#coffee-chats) so I saw these “meetings” on calendars as I tried to schedule chats. It reinforced our [family first sub-value](/handbook/values/#family-and-friends-first-work-second), and also normalized these activities. It helped me understand that I could set up similar boundaries and not sacrifice bed time or something similar just to have an empty hour on my calendar. — [*Seth B.*](https://gitlab.com/sethgitlab)

## Establish ground rules

Rules will look different for each family, and they will likely require [iteration](/handbook/values/#iteration) as projects change and children age. Setting rules of when and how a child (or children) may engage with a working parent is vital. While this may feel rigid or harsh, ambiguity in this area will likely lead to disappointment on both sides. These rules are empowering to the worker, as they're able to share with their manager and teams and weave this into their day-to-day work. 

Waiting for work to neatly carve out time for a parent to engage with their child or children is an unwise approach. Be intentional about these ground rules and work with your manager to ensure that they are communicated up/out and respected by your team. 

## Working/parenting shifts via synced schedules

For working parents who have another working family member in the home, consider syncing your schedules so that kids are aware. This helps balance the load, and provides clarity to children. This works best when "parenting shifts" are scheduled into each other's work calendars and communicated with respective work teams. 

## Optimize for open space in your calendar

Being booked at 100% is a risk, particularly for working parents. (See [Kingman's Formula](https://blog.acolyer.org/2015/04/29/applying-the-universal-scalability-law-to-organisations/) for the mathematics behind this.) The ebbs and flows of life cannot be predicted — after all, [it's impossible to know everything](/handbook/values/#its-impossible-to-know-everything). If your standing commitments have you at 100%, you have no room to react thoughtfully to life/children, or to plan for improvements. 

Stress is contagious, but so is [calm](/company/culture/all-remote/mental-health/). Be ruthless in declining meetings and conveying that your schedule is full once you reach a certain quantity of daily or weekly meetings. Consider using [Clockwise](/handbook/tools-and-tips/other-apps/#clockwise) to force more focus time into your schedule.

## Expect the unexpected 

Planning and preparation is wonderful, but as any parent knows, each day brings a set of challenges that are impossible to fully hedge for. Open, honest, transparent conversation with one's manager and team is vital to ensuring that hiccups are met with less stress and anxiety. Communicating one's realities is important to keeping a team on track and adjusting due dates and milestones accordingly. 

## Do not apologize for being a parent

Parents bring a great deal of wisdom and experience to the workforce and contribute to a [diverse and inclusive culture](/company/culture/inclusion/building-diversity-and-inclusion/). No one should feel pressured to apologize for being a parent or [caregiver](/company/culture/all-remote/people/#caretakers). 

## Other resources

### Adjusting to work-from-home while being a full-time parent

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TYdPXSYpBcg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) video above, GitLab's [J.D.](https://gitlab.com/jalex1) offers tips and advice for adjusting to work-from-home while being a full-time parent.*

### Roundtable discussion with parents at GitLab

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/G-_kBLZV0Z4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) video above, several GitLab team members discuss tips and advice for balancing full-time parenting while working remotely.*

### GitLab blogs from working parents

1. [What’s it like to be a working parent at GitLab?](/blog/2016/04/08/remote-working-parents/)
1. [Parental/maternity leave around the world – how does your country stack up?](Parental/maternity leave around the world – how does your country stack up?)
1. [How I balance a baby, a career at GitLab, and cultural expectations of motherhood](/blog/2019/07/25/balancing-career-and-baby/)
1. [How to make your home a space that works with kids](/blog/2019/08/01/working-remotely-with-children-at-home/)
1. [5 Things to keep in mind while working remotely with kids](/blog/2019/08/08/remote-kids-part-four/)
1. [GitLab parental leave and return tool kit](/handbook/total-rewards/benefits/parental-leave-toolkit/)

### Guides from friends and partners

1. [Doist's guide for the remote working parent](https://doist.com/blog/remote-working-parents-survival-guide/)
1. [Trello: A remote work guide for parents](https://blog.trello.com/remote-work-guide-for-parents)
1. [Thrive Global: Managing parenting and remote work](https://thriveglobal.com/stories/three-tips-for-managing-parenting-and-remote-work/)
1. [Buffer: Tips from a remote team on working at home with kids](https://buffer.com/resources/integrating-work-family-21-tips-working-home-kids/)
1. [Autonomous: How to balance working at home and parenting](https://www.autonomous.ai/ourblog/how-to-balance-working-at-home-and-parenting)

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
