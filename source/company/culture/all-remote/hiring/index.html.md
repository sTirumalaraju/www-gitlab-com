---
layout: handbook-page-toc
title: "All-Remote Hiring"
canonical_path: "/company/culture/all-remote/hiring/"
description: How to hire and support a remote team
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how to properly, efficiently, and effectively hire in an all-remote environment.

## How do you hire in an all-remote organization?

"How do you handle hiring in an all-remote company?" is a question we at GitLab hear often. Many companies hire only within one country or region, and even multinational corporations typically hire into specific offices. 

GitLab [envisions](/company/culture/all-remote/vision/) a world where talented, driven individuals can find roles and seek employment based on business needs, rather than an oftentimes arbitrary location. 

For organizations struggling to find, recruit, retain, and compensate employees in locales such as San Francisco, New York City, London, Singapore, and Sydney, imagine the influx of highly qualfied applicants if the location requirement were removed from all job descriptions. For a glimpse at what this looks like, please visit [GitLab's Jobs page](/jobs/). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/V2Z1h_2gLNU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

You can hear from our [geographically diverse](/company/culture/inclusion/#fully-distributed-and-completely-connected) team in the video above, filmed at our all-employee gathering — [Contribute](/events/gitlab-contribute/).

We want to state up front that one of the advantages of being an all-remote company is that we can [hire talent from a global pool](/handbook/hiring/). We are not restricted to the usual job centers, which gives us access to a tremendous amount of talent that many other companies will not consider for employment. It may take more effort to find talent in more diverse places, but that is an effort we are willing to make. 

You can learn more in the [Principles of Hiring section of our Handbook](/handbook/hiring/principles/). 

### Why do you hire from anywhere?

GitLab's six [values](/handbook/values/) are Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency, and together they spell **CREDIT**.

True to those values, GitLab strives to hire team members who are passionate, empathetic, kind, tenacious, and ambitious, regardless of their location. By opening the recruiting funnel to as broad a swath of the world as we can, we create a more inclusive hiring environment, lean on tight collaboration to drive progress [across time zones](/company/culture/all-remote/asynchronous/), and focus our hiring decisions on results rather than location. 

Hiring an all-remote team from across the globe allows GitLab to pay local rates, which you can learn more about [on the company blog](/blog/2019/02/28/why-we-pay-local-rates/). By hiring brilliant minds in locations with lower costs of living, GitLab is able to save money to hire even more people as we scale our business. 

It also enables GitLab to source candidates that align with our [values](/handbook/values/). We believe that an all-remote structure [enables us](/company/culture/all-remote/values/) to live our values in a more meaningful way. We source candidates who want to work in a healthier, more efficient, more transparent atmosphere in a locale where their soul is most fulfilled.

For now, this gives GitLab a tremendous competitive advantage. We are sourcing talent from places that most companies overlook, and we're creating [a more diverse team](/company/culture/inclusion/#fully-distributed-and-completely-connected) all the while. We hope that [this advantage fades](/company/culture/all-remote/vision/#diminishing-competitive-advantage), as more companies embrace all-remote and widen their recruiting funnel beyond the usual talent centers.

We've published our [hiring process](/handbook/hiring/interviewing/), including example screening questions, in our handbook. 
While this may be unique, we see it as simply staying true to our [transparency value](/handbook/values/#transparency). The process shouldn't be a mystery. 

Letting candidates know what to expect allows them to focus on whether the role and the company are right for them, while we evaluate that too. 

### Where do you *not* hire?

Each country has unique and complex rules, laws and regulations, which can affect us to conduct business, as well as the employability of the citizens of those countries.

We are growing rapidly and continuously expanding our hiring capabilities in other geographies. In countries listed in our [contract_factors.yml file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/contract_factors.yml), we have a payroll and can employ you as an employee. In all other countries, we can hire you as a contractor.

Learn more at our [Country Hiring Guidelines](/handbook/people-group/contracts-and-international-expansion/#country-hiring-guidelines) section in the [Contracts and International Expansion](/handbook/people-group/contracts-and-international-expansion/) handbook page.

## What qualities do you look for in remote hires?

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/3_bqUjz8Vd4?start=819" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab's Head of Remote speaks with [Remotefit](https://remotefit.netlify.com/) co-founder Javier on what to look for when considering a remote role, as well as what qualities to seek when hiring *for* a remote role.

> There are nuances to loving remote work, and being a great remote worker. A sub-value at GitLab that encapsulates that is [manager of one](/handbook/values/#managers-of-one). 
>
> Another element to consider is communication. GitLab is an [asynchronous](/company/culture/all-remote/asynchronous/) company. Much of what we do is [communicated through text](/company/culture/all-remote/effective-communication/). For people with long in-office careers, who are used to verbal communication as the default, they may be uncomfortable at first communicating information via text. 
>
> From the employer side, you may wonder how to screen for this. Most people have worked remotely even if they don't know it. If you come from a colocated company which had multiple headquarters — for example, someone based in Seattle with colleagues in Singapore or London — you probably worked with people outside of your office. 
>
> Even if your company didn't admit it, those offices [were remote to one another](/company/culture/all-remote/stages/). You had to deal with Zoom calls, time zone differences, etc. What was that like? Asking applicants to dive into those situations is a great way to uncover how they view remote work. 
>
> Almost everyone has accomplished work while not in an office, even if it wasn't *defined* as remote work. — *[Darren M.](https://gitlab.com/dmurph), Head of Remote at GitLab*

Put simply, we look for candidates that align with our [values](/handbook/values/) and inform us that said values resonate with them.

Having prior experience working remotely is appreciated, but by and large we look for the same attributes that a colocated company would. Qualities such as timeliness, dependability, respect, a heart of collaboration, perseverance, empathy, kindness, and ambition are valued.  

Great self-awareness and expert communication skills are necessary to thrive in an all-remote setting. 

Even if someone has not worked in a remote environment before, chances are high that they have spent time on work while outside of their office. Consider asking candidates for examples of how they managed those instances (e.g. working while traveling, advancing a university project while away from campus, etc.), looking specifically for how they embrace autonomy and [blameless problem solving](/handbook/values/#blameless-problem-solving). 

Qualities that may be considered unique to a remote workplace are listed below.

1. Appreciation for [self-learning and self-service](/company/culture/all-remote/self-service/)
1. Appreciation for [documentation](/company/culture/all-remote/handbook-first-documentation/) 
1. Proven ability to be a [manager of one](/handbook/values/#managers-of-one) 
1. Proven ability to work [asynchronously](/company/culture/all-remote/asynchronous/)

When asked during an [INSEAD](http://insead.edu/) case study [interview](https://youtu.be/EuGsen3FxXc) if anyone could learn to work well in an all-remote setting, GitLab co-founder and CEO Sid Sijbrandij provided the following reply.

> What's really important in all-remote is that you are a [manager of one](/handbook/values/#managers-of-one). 
>
> You're not going to have someone expecting you at the office. You're not going to have hour-to-hour hand-holding, so you have to be able to work independently and manage yourself. 
>
> For [asynchronous communication](/company/culture/all-remote/asynchronous/), which is essential to bridge time zones, it is important that you can write well — that you're concise and precise in your written communication. — *GitLab co-founder and CEO Sid Sijbrandij*

## How do remote companies attract top talent?

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">Now that tech companies can’t flaunt to attract and retain employees for campus amenities: ping pong, free food, gyms, cafes, car washes, massages, etc <br><br>How can they best attract top talent?</p>&mdash; Jeremiah Owyang (@jowyang) <a href="https://twitter.com/jowyang/status/1280194961565028352?ref_src=twsrc%5Etfw">July 6, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Remote work is the last great competitive advantage for employers. Any two employers can compete on salary, title, and prestige. All things being equal, a talented employee will choose the employer which **supports remote work**, allowing them maximum freedom and autonomy to integrate work into their life as opposed to the other way around. 

Companies which have resorted to traditional in-office perks — ping pong tables, free food, free-flowing drinks, fitness centers, car washes, onsite daycare, etc. — may find it jarring to realize that none of those are relevant in a remote setting. Leaders may also realize that these perks are inherently exclusive.

Further complicating matters for [hybrid-remote companies](/company/culture/all-remote/hybrid-remote/) is the challenge of placing a monetary value on those perks for onsite employees, while attempting to create set of perks with equal value for those who default to remote. 

Below is a list of suggestions for attracting top talent to remote organizations. All of this assumes that a workplace supports [permission to play](/handbook/values/#permission-to-play) behaviors. No amount of perks will make up for toxicity and indifference towards ethics.

1. Benefits for families and caregivers (daycare stipend, cleaning/laundry stipend, meal stipend, enhanced health insurance options, continuing education, adoption/fertility support, etc.)
1. [No ask, must tell vacation policy](/handbook/paid-time-off/) which is actually lived out visibly by senior leaders
1. The ability to [expense](/handbook/spending-company-money/#coworking-or-external-office--space) a "third space" for working — an external office, coworking space, or residential community space through the likes of [Codi](https://www.codi.com/)
1. Caregiver leave (maternity, paternity, eldercare, etc.)
1. [De-location stipend](https://zapier.com/blog/move-away-from-sf-get-remote-job/) (supporting relocation away from a high cost-of-market location)
1. 4-day workweek, Summer Fridays, no-meeting days, and other efforts that provide company-wide time off for employees to rest, recharge, and connect with community 
1. Supporting the purchase of a complete and ergonomic [remote workspace](/company/culture/all-remote/workspace/) which is uniquely tailored to each team member
1. Transparent policies for investing in and championing [diversity, inclusion & belonging](/handbook/values/#diversity-inclusion) (and published goals for improvement)
1. Restricted stock units (RSU) or stock options
1. Enhanced hiring bonuses and higher salaries
1. Large annual bonuses based on performance (*remote work [measures results](/handbook/values/#measure-results-not-hours), not hours spent, input, or time-in-seat*)
1. Well-trained remote managers that default to [asynchronous workflows](/company/culture/all-remote/asynchronous/) (*Confidently assuring prospective talent that they will endure fewer meetings and micromanagement is a solid way to differentiate from other employers.*)
1. Publishing supportive policies which explicitly allow employees to create better [work/life harmony](/company/culture/all-remote/people/#worklife-harmony) and put [family and friends before work](/handbook/values/#family-and-friends-first-work-second)
1. Supporting (via time and money) learning for self-development 
1. Building and explicitly publishing a culture of trust, empowerment, and autonomy for employees to do great work regardless of location (*view [GitLab's guide on building culture](/company/culture/all-remote/building-culture/) and its [Values page](/handbook/values/) for more*)
1. Engage and respect *existing* employees, empowering them to become [talent brand ambassadors](/handbook/hiring/gitlab-ambassadors/) on platforms such as Glassdoor, LinkedIn, Twitter, and Comparably
1. Permit open sourcing some of an employee's work and encourage building of their personal brands
1. Clear opportunities for [growth](/handbook/people-group/learning-and-development/career-development/) and professional development 

### Supporting remote vs. allowing remote

Supporting remote work is an inclusive perk which may be enjoyed differently by every employee. It is worth noting that [*supporting* remote](/company/culture/all-remote/phases-of-remote-adaptation/) is different than [*allowing* remote](/company/culture/all-remote/stages/#remote-allowed). The former sets remote employees up for success, giving them a single source of truth to work from. The latter casts a layer of shame over remote workers, effectively holding their lack of a commute over them as something that they must continually work to earn or deserve. 

For hiring teams, it is imperative to address this in the interview process so that expectations are transparent. 

## Make your strategy public

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/3_bqUjz8Vd4?start=412" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab's Head of Remote speaks with [Remotefit](https://remotefit.netlify.com/) co-founder Javier on building and sustaining culture while being open about processes in the recruiting and hiring process.

> In the effort of [transparency](/handbook/values/#transparency), we showcase our culture in advance. There's a section on our Jobs FAQ entitled "[What's it like to work at GitLab?](/jobs/faq/#whats-it-like-to-work-at-gitlab)" 
>
> This is the core question that anyone would have before they even apply to GitLab. It's not your typical corporation. The people who come here do so *because* it's different, and it gives them permission to drop prior organizational baggage at the door and operate differently. 
>
> The only way they'll know that before they get down the interview funnel is if you tell them that in advance. I'd encourage other organizations to do this, too.
>
> It's silly to think that a company should withhold its strategy, its culture, its way of working, until someone gets hired. These elements determine whether or not a team member will thrive. Why would you hold that back? — *[Darren M.](https://gitlab.com/dmurph), Head of Remote at GitLab*

You cannot expect to hire people who are passionate about executing your strategy if you only share the strategy *after* they are onboard. 

Make your strategy public and ensure that applicants are aware of it, along with your values, during the interview process. You can hire someone with excellent attributes, but if they are not aligned with your strategy and values, it may be difficult for that person to thrive within your organization. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EuGsen3FxXc?start=2493" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab co-founder and CEO Sid Sijbrandij discusses hiring (amongst other topics) with researchers from [INSEAD](https://www.insead.edu/). 

> It's not about having the best strategy. The strategy tends to be obvious. It's about executing the best.
>
> To execute the best, it needs to be super clear. Nothing is clearer than writing it down explicitly and having the whole world be able to amend it.
>
> People need to be aligned to it. How can you have people aligned to your strategy if you only tell it after you hire them?
>
> You may hear something akin to: 'Our strategy is very important, and you need to be aligned to it! We'll only tell you after you join the company.'
>
> I think that's bananas. That's what every company in the world does. If your strategy is really important, you want to make sure people buy in *before* they join, and people that don't buy in don't actually join the company.
>
> Execution depends on people, and this allows us to attract the best people because [we are public about what we do](/company/strategy/). — *GitLab co-founder and CEO Sid Sijbrandij*

## Using video calls to interview and engage

At GitLab, we do not rely on in-person interviews. Instead, we utilize [Zoom](/handbook/tools-and-tips/#zoom) to connect with candidates via [video calls](/handbook/communication/#video-calls). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tHThOsneFOY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Nicole Schwartz and Jeremy Watson, product managers at GitLab, walk through the Deep Dive portion of the product management interview process. For more, watch Shane Bouchard, UX Manager, and Jeremy Watson, Senior Product Manager, [discuss interviewing and finding great designers](https://www.youtube.com/watch?v=d15_q_K5Yq8).

This approach reinforces several of GitLab's [values](/handbook/values/). 
* [Efficiency](/handbook/values/#efficiency): It's highly efficient, given that neither the candidate nor the interviewer must devote time to relocating in order to engage with one another. This allows us to hire faster and provide a better candidate experience.
* [Diversity, Inclusion & Belonging ](/handbook/values/#diversity--inclusion): By allowing candidates to interview from a space where they are comfortable, we create a level playing field for those with mobility concerns, caregivers, etc.

Learn more in the [Interviewing section of GitLab's Handbook](/handbook/hiring/interviewing/), and our [guide to conducting remote interviews](/company/culture/all-remote/interviews/#how-do-you-interview-remotely) in or outside of GitLab.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/6mZqzK_40FE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Above, GitLab co-founder and CEO Sid Sijbrandi sits down for an interview with Sunil Kowlgi, founder of [Outklip](https://outklip.com/), on the topic of using video for effective collaboration. You can read more on their discussion — which covers remote hiring, management, customer support, and more — [on GitLab's blog](/blog/2019/04/18/lessons-on-building-a-distributed-company/).

It's important for candidates to become comfortable with video calls, as they are a primary medium for communication within GitLab. Because we are an all-remote organization with no offices, meetings and informal coffee chats occur using video calls. 

Learn more in the [Meetings](/company/culture/all-remote/meetings/) section of our All-Remote section on company culture.

## Handling local regulations, risks, entities, etc.

Hiring across the globe isn't without its challenges. There are local regulations and risks unique to countries and regions around the globe. 

Rather than attempting to incorporate in every country where an all-remote company has even a single employee, organizations must weigh the benefits of creating a legal entity with other options. There are four means of engagement with GitLab. These are commonly used arranagements that can be considered by other all-remote companies.

* **GitLab Entity**: Individuals can be employed directly with GitLab Inc, BV, LTD, GmbH, PTY, Federal.
* **Professional Employer Organization (PEO)**: In select countries where GitLab does not have an entity, we hire professional employment organizations to serve as the [employer of record (EOR)](/handbook/contracts/#employer-of-record-providers) in order to facilitate payments. 
* **C2C (Contractor)**: A corp to corp arrangement, whereby a corporation or LLC invoices GitLab BV for GitLab related work.
* **IND (Contractor)**: By far the most widely applicable. The individual contractor arrangement can be used in countries where GitLab is hiring, yet does not have an entity or PEO agreement in place. This allows an individual to invoice GitLab BV as an individual or via their own company with no partners.

Learn more about [employee types and hiring partners](/handbook/contracts/#employee-types-at-gitlab) across countries in the [Contracts](/handbook/contracts/) section of GitLab's Handbook.

It is not always practical to understand the nuances of local regulations in-house. Wherever GitLab has an entity or contractor, we have external counsel to advise and ensure that we are compliant. 

In countries where GitLab utilizes a professional employer organization (PEO), counsel within the PEO advises us to ensure hiring compliance. 

## Deciding between entity and PEO

Generally speaking, it becomes feasible to consider creating an entity within a country once a company reaches 20+ team members in that country. While total costs vary significantly from country to country, creating an entity within a country can cost as little as a few hundred dollars to tens of thousands of dollars. 

A company must also hire skilled team members to oversee compliance with local taxes, laws, benefits, etc. in each country where there is an entity. 

For countries where a company is not already incorporated or has a local entity, consider the below as a guide.

1. **Fewer than 5 team members**: Utilize a contractor arrangement
1. **Between 5 and 20 team members**: Leverage a Professional Employer Organization (PEO)
1. **20+ team members**: Consider establishing a local entity 

## Impact of nonuniform hiring on employee experience

Beyond the administrative challenges associated with using various vehicles (entities, PEOs, contractors, etc.) to hire team members, companies should be proactive in recognizing how this will impact culture and morale. 

While GitLab believes that there are far [more pros than cons to hiring globally](/company/culture/inclusion/#fully-distributed-and-completely-connected) — creating a geographically diverse team, finding the world's best talent, hiring in underserved areas and connecting with people from diverse socioeconomic backgrounds — it's wise to evaluate how nonuniform hiring mechanisms may conflict with a company's values. 

When leveraging a third-party to hire, it is difficult to ensure that they will treat your employees how you would ideally like to see them treated. Third-party companies are under no obligation to understand or implement your [values](/handbook/values/). 

It is imperative that the hiring team communicate this in advance, ideally during the interview phase, to manage expectations of employees who are hired by a third-party firm that may not always act in accordance to your values. 

Moreover, one should aim to leverage a single Professional Employer Organization (PEO) over the long term. Switching PEOs is disruptive to team members and creates mental and administrative burden. A growing company should pay particular attention to feedback from employees who rely on PEOs, and plan beyond current hiring needs.

## Is there a single PEO that covers the globe? 

This is a common question from remote-first and all-remote companies who are understandably interested in streamlining the process of hiring talented team members from an array of countries. The short answer, for now, is "no." 

Futhermore, some PEOs serve an array of regions, but offer varying levels of service and disperate experiences from country to country. Seek out advice where possible to gather country-level feedback. 

Remote is a new company that [intends to tackle this challenge over time](https://blog.remote.com/solving-global-employment-through-remote/).

> Remote is working on solving global employment by allowing companies to employ people *locally* through Remote. That is, full employment local to each and every employee, but a single invoice for the employer. Remote manages payroll, benefits, and compliance, according to the standards of the employer.

Be intentional about due diligence when vetting PEOs. Some firms advertise global reach, but do not offer high-touch, reliable end-to-end service in every country. Instead, they leverage third-parties themselves in various countries, creating yet another layer in the process that may negatively impact employee experience. 

## Local sourcers

Even when the whole world is your talent pool, there will be roles that require deliberate involvement and proactive search. 

For these purposes, GitLab built a Sourcing team focused on finding best passive talent and nurturing the candidate pipeline — a tactic that can be implemented by other all-remote companies. 

While GitLab is location-agnostic, we work to ensure that we have local sourcers based in every macro-region (e.g. Americas, APAC and EMEA) so they can bring their market expertise and leverage our presence in these territories. 

We also consider our sourcing effort extremely important for bringing [diverse](/company/culture/inclusion/) talent onboard. Our sourcers are aligned with recruiters and business verticals to ensure that they can apply their unique knowledge of specific areas.

Driven by our [Collaboration value](/handbook/values/#collaboration), we organize [source-a-thons](/handbook/hiring/sourcing/#source-a-thons), which become a place where everyone can share their market insights and contribute to hiring. We find these sessions extremely productive as they help sourcers partner with their hiring managers and calibrate their expectations.

Learn more on how GitLab's Sourcing team operates [here](https://about.gitlab.com/handbook/hiring/sourcing/). 

## Contribute your lessons

Streamlining the global hiring process is something that remote-first and all-remote companies are grappling with. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page. 

----

Return to the main [all-remote page](/company/culture/all-remote/).
