---
layout: handbook-page-toc
title: "All-Remote and Remote-First Jobs and Remote Work Communities"
canonical_path: "/company/culture/all-remote/jobs/"
description: All-Remote and Remote-First Jobs and Remote Work Communities
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're spotlighting all-remote and remote-first companies, along with job boards that curate high-quality remote roles. 

We're also curating a growing list of remote communities, where remote leaders and workers can network and share ideas, resources and job opportunities. 

## Organizations leading the way

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/4bO-wxLQtJQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Darren (GitLab) and Rodolphe (Remotive) discuss several remote work topics: favorite things about remote work, how to find/start/thrive in your first remote role, and the impact of the growing remote work community.* 

Working remotely is becoming more common globally, as supported by data in the [GitLab Remote Work Report](/remote-work-report/). While all-remote and remote-first organizations still represent the minority, a variety of trends — from heightened rents in major cities to a greater emphasis on sustainability to a cultural desire to work and live where one is most fulfilled — have more people than ever [interested in this way of working](/company/culture/all-remote/benefits/).

Buffer's latest [State of Remote Work](https://lp.buffer.com/state-of-remote-work-2020) shows that the overwhelming majority of those who work remotely would recommend it to others, while [new survey data from Owl Labs](https://www.owllabs.com/blog/remote-work-statistics) collected from 23 countries and 6 continents found that over half of global companies now allow some form of remote work. 

Of course, [not all remote environments are created equal](/company/culture/all-remote/part-remote/) — a point we cover in detail on the [part-remote page](/company/culture/all-remote/part-remote/). 

In the aforementioned Owl Labs survey, it found that 16% of global companies are fully remote and 40% of global firms are hybrid, offering remote options while maintaining at least one physical office. 

However, an increasing amount of startups and small-to-medium sized businesses are all-remote. In other words, those looking to work for an all-remote or remote-first organization as opposed to lobbying for a remote arrangement within a colocated company will want to cast their attention on career pages from the below.

### All-remote companies

![GitLab all-remote companies illustration](/images/all-remote/gitlab-com-all-remote-v4-dark-1280x270.png){: .shadow.medium.center}

- [InVision](https://www.invisionapp.com/), see their posts about [remote-only motivations](https://www.invisionapp.com/blog/studio-remote-design-team/), building [company culture](https://www.invisionapp.com/blog/remote-company-culture/), and what remote work [feels like](https://www.invisionapp.com/blog/remote-worker-truths/).
- [Buffer](https://buffer.com), see their posts about going [remote only](https://open.buffer.com/no-office/), [the benefits](https://open.buffer.com/distributed-team-benefits/), and how they [make it work](https://open.buffer.com/buffer-distributed-team-how-we-work/).
- [Automattic](https://automattic.com/about/), the company behind [WordPress.com](https://wordpress.com) and [The year without pants](https://www.amazon.com/Year-Without-Pants-WordPress-com-Future/dp/1118660633) fame
- [GitLab](https://about.gitlab.com), this website is hosted by GitLab, everyone is welcome to contribute to [this page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/index.html.md), and also check out [our handbook](/handbook/) for remote work practices.
- [Techmeme](https://www.techmeme.com/), has been [fully remote since 2007](https://twitter.com/gaberivera/status/1226189350276341761), utilizing a global team of editors to "curate the technology news site of record for people both within and beyond the industry."
- [10up](https://10up.com/), is a fully remote team that works to "make the internet better with consultative creative and engineering services, innovative tools, and dependable products that take the pain out of content creation and management."
- [Zapier](https://zapier.com/), wrote a [book on remote work](https://zapier.com/learn/remote-work/), is 100% remote.
- [Close.io](https://close.io), see their [jobs page](http://jobs.close.io/) and [blog post about remote culture](http://blog.close.io/remote-team-culture).
- [Hubstaff](https://hubstaff.com/), streamlines time tracking and team management, especially for remote teams. Check out the CEO's [growth blog](https://blog.hubstaff.com/startup-story-personal-pain/).
- [Groove](https://www.groovehq.com/), see their blog on being a [remote team](https://www.groovehq.com/blog/being-a-remote-team).
- [Jamm](https://jamm.app/) Fully remote team building an app to help remote teams work better together.
- [Anybox](https://anybox.fr/), see their feedback (in french) [Télétravail généralisé, notre retour d'expérience](https://anybox.fr/blog/teletravail-generalise-confiance-et-seminaires).
- [Screenly](https://www.screenly.io), this is [how they work](https://www.screenly.io/blog/2016/11/23/how-we-work-at-screenly/).
- [Soshace](https://soshace.com), see their [jobs page](https://soshace.com/careers).
- [Innolitics](http://innolitics.com), see their [handbook](http://innolitics.com/about/handbook/) which describes how they work remotely.
- [IOpipe](https://iopipe.com)
- [HiringThing](https://www.hiringthing.com/), see their post about what it is like to work [remote-only](https://www.hiringthing.com/2018/04/19/whats-it-like-to-work-at-hiringthing.html).
- [SerpApi](https://serpapi.com)
- [Scrapinghub](https://scrapinghub.com/), who turns websites into data. See their [about us page](https://scrapinghub.com/about), [open positions](https://scrapinghub.com/jobs), and [blog](https://blog.scrapinghub.com/).
- [Jitbit](https://www.jitbit.com/)
- [ProxyCrawl](https://proxycrawl.com)
- [Crossover](https://www.crossover.com/), read their tips on [creating the ultimate package for remote work](https://medium.com/the-crossover-cast/the-ultimate-package-for-remote-work-b9c68339fe57).
- [Toptal](https://www.toptal.com/), see their Benefits: ["Fully Remote, No offices, no useless meetings, no mandatory hours. You’re recognized for what you do, not your time in a chair"](https://www.toptal.com/careers), also see [FAQ: "Where do Toptal experts work?"](https://www.toptal.com/faq#where-do-toptal-experts-work).
- [Sonatype](https://www.sonatype.com/), blog article on remote work at [Sonatype](https://blog.sonatype.com/the-magic-of-a-remote-organization).
- [Podia](https://www.podia.com), a remote-only company, see [their founder's Tweetstorm](https://twitter.com/spencerfry/)
- [Knack](https://www.knack.com), a remote-only company, see [why they work remotely](https://www.knack.com/blog/why-we-chose-remote).
- [Honeybadger](https://www.honeybadger.io), see [how they work](https://www.honeybadger.io/careers/).
- [Doist](https://doist.com), makers of [Todist](https://todoist.com) and [Twist](https://twistapp.com). Here's how they [make remote work happen](https://blog.doist.com/https-blog-doist-com-managing-remote-teams-622521189e80), as well as [a blog post on not sacrificing one's career ambitions in an all-remote setting](https://doist.com/blog/remote-career-advice/).
- [Discourse](https://www.discourse.org/), see Coding Horror's ["We hire the best, just like everyone else"](https://blog.codinghorror.com/we-hire-the-best-just-like-everyone-else/)
- [Aha!](https://www.aha.io/), see their blog about [remote work](https://blog.aha.io/category/remote-work/).
- [Articulate](https://articulate.com), see their [company page](https://articulate.com/company) for how they work.
- [Infinity Interactive](https://iinteractive.com/), see their [benefits page](https://iinteractive.com/benefits) for their embrace of remote working.
- [Altcoin Fantasy](https://altcoinfantasy.com/), see their [tips on hiring and retaining performant remote workers for an early startup](https://blog.altcoinfantasy.com/2018/06/13/tips-hiring-retaining-remote-employees-early-stage-startups).
- [Nozbe](https://nozbe.com), see [what they do](https://nozbe.com/about), [how they work](https://nozbe.com/blog/why-nooffice/) and [what are Nozbe’s values](https://nozbe.com/blog/nozbe-values/).
- [Duck Duck Go](https://duckduckgo.com/), see their ["Work Whenever, Wherever" Careers page](https://duckduckgo.com/hiring/), also see [Making Remote Work: Ask DuckDuckGo About Being A Digital Nomad Q&A](http://womendigitalnomads.com/blog/duckduckgo-digital-nomad/).
- [FormAssembly](http://www.formassembly.com/), see their [jobs page](https://formassembly.workable.com/). Also see their [blog post: "[Infographic] Benefits of Working Remotely"](https://www.formassembly.com/blog/remote-work-benefits/).
- [Gruntwork](https://gruntwork.io/): see [how they built a distributed, self-funded, family-friendly, profitable startup](https://blog.gruntwork.io/how-we-built-a-distributed-self-funded-family-friendly-profitable-startup-93635feb5ace).
- [Idea To Startup](https://ideatostartup.org/), see their [join page: _"2. Work From Anyplace. It doesn't matter whether you live"_ .... _"You can work from anyplace"_](https://ideatostartup.org/join/).
- [SoftwareMill](https://softwaremill.com/), see their [pros&cons of remote work post](https://softwaremill.com/remote-work-pros-and-cons/) & others
- [Pangian](https://pangian.com/) _"Top talent working remotely. Worldwide"_, see their [about page](https://mailchi.mp/pangian.com/remote-network) _"The fastest-growing remote talent network. Worldwide"_.
- [Winding Tree](https://windingtree.com/), Decentralized Travel Ecosystem built by decentralized team.
- [reinteractive](https://reinteractive.com/)
- [easyDNS](https://easydns.com/), see why they decided to [go all-remote.](https://easydns.com/blog/2019/05/23/the-end-of-an-era-for-easydns/)
- [TaxJar](https://www.taxjar.com), read their [blog about building a remote-only culture](https://life.taxjar.com/), their [benefits](https://www.taxjar.com/jobs/), [core values](https://www.taxjar.com/about/), and [hear what employees say about working there](https://www.comparably.com/companies/taxjar).

### Remote-first companies

- [Basecamp](https://basecamp.com/), authors of [Remote](https://37signals.com/remote)
- [Harvest](https://www.getharvest.com/), see their collection of stories on [Working without borders](https://www.getharvest.com/working-without-borders)
- [Nota](https://notainc.com), builders of a [modern idea hub](https://scrapbox.io) & [instant screenshot sharing app](https://gyazo.com) perfect for remote work.
- [Niteo](https://niteo.co/), a decade-old Python boutique with a public [handbook](https://github.com/niteoweb/handbook)
- [ElevenYellow](https://elevenyellow.com), type `job openings` on their [console](https://www.elevenyellow.com/) to join this team of digital nomads.
- [wemake.services](https://wemake.services), software development company using [`RSDP`](https://wemake.services/meta/)
- [Igalia](https://igalia.com), [employee-owned cooperative](https://www.igalia.com/about-us/great-place-to-work/) providing software development consulting services
- [Ad Hoc](https://adhocteam.us/), see their post ["The truth about remote work"](https://adhocteam.us/2017/12/05/truth-remote-work/) and their [jobs page](https://adhocteam.us/join/).
- [Help Scout](https://www.helpscout.com/), see their posts "[What We’ve Learned Building a Remote Culture](https://www.helpscout.com/blog/remote-culture/)," "[Employee Onboarding Best Practices](https://www.helpscout.com/blog/employee-onboarding/)," and "[6 Tips to Keeping Your Remote Team Connected](https://www.helpscout.com/blog/remote-team-connectivity/)."

### Newly remote companies due to COVID-19

COVID-19 served as a catalyst for many companies to become remote-friendly. In turn, an increasing number of organizations are allowing existing employees to work remotely indefinitely/permanently. Below are trackers which publicize which firms have shifted policies to support remote.

Beyond those listed, COVID-19 has democratized the conversation around workplace flexibility. It is now expected for prospective job seekers to inquire about a firm's approach to remote work and flexibility early in the interview process. 

- [LifeShack: Remote Work Tracker](https://lifeshack.io/remote/)
- [Are They Remote Yet?](https://www.aretheyremoteyet.com/)
- [ProsperCicle: Remote-First Companies](https://www.prospercircle.org/signals/remote-companies)
- [OfficeStatus](https://officestatus.fyi/)

## All-remote and remote-first job boards

Job seekers are wise to point their efforts towards companies that are [built to support 100% remote](/company/culture/all-remote/learning-and-development/). You're able to bypass hours of lobbying for a remote arrangement [during the interview process](/company/culture/all-remote/hiring/), and you're assured that the tools you need to operate effectively from anywhere will be included from the get-go. 

While there are myriad job boards that curate part-time and freelance gigs which can be completed from anywhere, this section is focused more on surfacing all-remote and remote-first *careers*. 

Here's a [list](https://www.ryrob.com/remote-jobs-websites/) of 60 remote jobs sites in 2020, including:

- [Grow Remote](https://growremote.ie/)
- [Torre](https://www.torre.co/)
- [RemoteHub](https://remotehub.io/)
- [We Work Remotely](https://weworkremotely.com)
- [Remote OK](https://remoteok.io)
- [Jobspresso](https://jobspresso.co/)
- [Working Nomads](http://www.workingnomads.co/jobs)
- [Wrk Crush](https://wrkcrush.com/)
- [PowerToFly](https://powertofly.com)
- [Remote jobs on Angellist](https://angel.co/job-collections/remote)
- [Remotive](https://remotive.io/) - in particular, [The List of Awesome](https://docs.google.com/spreadsheets/d/1TLJSlNxCbwRNxy14Toe1PYwbCTY7h0CNHeer9J0VRzE/htmlview?pli=1#gid=0)
- [Who Is Hiring?](https://whoishiring.io/)
- [JustRemote](https://justremote.co)
- [Remote For Me](https://remote4me.com/)
- [FlexJobs](https://www.flexjobs.com/)
- [Remote.Co](https://remote.co/)
- [Remotey](https://remotey.com/search/vacancy)
- [Glassdoor](https://www.glassdoor.com/) (toggle the "WFH or Remote" option to filter search results)

> "Getting promoted while working remotely really depends on how remote your company is. GitLab is a fully remote organization with over 700 folks around the world, so getting promoted wasn’t difficult at all. In previous roles, I was the only person who worked remotely. In those cases, I felt very forgotten by my employer." - *Emilie L Schario, a Data Engineer at GitLab*

For a better understanding of the nuances, take a look at "[6 People Who Prove You Don’t Have to Sacrifice Your Career to Work Remotely](https://doist.com/blog/remote-career-advice/)" on the Doist blog. Experts from various all-remote and remote-first companies, including [GitLab](/jobs/), [Doist](https://doist.com/), [Help Scout](https://www.helpscout.com/), [DataTrue](https://datatrue.com/), and [Quuu](https://quuu.co/), share their experiences.

## Remote work communities and networking

Staying [connected](/company/culture/all-remote/events/) with remote workers globally is in many ways much easier than staying connected with peers through in-person networking events. Working remotely forces you to be [intentional about everything](/company/culture/all-remote/management/), and the same is true for forming and fostering relationships and becoming part of one or more remote work communities. 

As remote work [becomes more common globally](/remote-work-report/), there are more opportunities than ever to get plugged in, share resources and tips, and exchange job opportunities. Below are several vetted remote communities with high engagement.

* [Remote-how virtual coworking space](https://remote-how.com/community)
* [We Work Remotely](https://weworkremotely.com/) 
   * [WWR Slack Community](http://bit.ly/wwr_slack)
   * [WWR Homepage](https://weworkremotely.com/)
   * [WWR: AMA with Darren Murph, Head of Remote at GitLab](https://weworkremotely.com/ama-with-darren-murph-head-of-remote-at-gitlab#)
* [Remote Workers in LinkedIn](https://www.linkedin.com/groups/13657237/) (*private group; all may request admission*)
* [NoHQ](https://nohq.co/join/)
* [RemotelyOne](https://www.remotelyone.com/)
* [CreativeTribes](https://creativetribes.co/)
* [Women in Technology](https://witchat.github.io/)
* [Grow Remote](https://growremote.ie/)

## Informal all-remote and remote-first job searching

Keep in mind that many great all-remote and remote-first companies aren't big enough to justify an appearance on conventional job sites. Particularly for startups, hiring is often accomplished via networking and word-of-mouth. 

Thus, it's worth a concerted effort to scour [LinkedIn](https://www.linkedin.com/) (including the [Remote Workers on LI Group](https://www.linkedin.com/groups/13657237/)), [Instagram](https://www.instagram.com/), and [Twitter](http://twitter.com). 

Oftentimes, founders will put out [informal](/company/culture/all-remote/informal-communication/) calls for help through a single tweet, Instagram story, or LinkedIn post. These calls do not make it to formal job boards, yet they represent opportunities to join a fledgling company during a formative time where there is potential to secure a significant stake via equity or [stock options](/handbook/stock-options/). 

Each of these platforms support search via hashtag. Be aware that many results will be spam, but diligent searchers can find diamonds in the proverbial rough. Here's a [list](https://www.yonder.io/post/best-remote-work-hashtags-social-media) of popular remote work hashtags, including the below.  Where you can, combine searches to include the below along with hashtags such as `#hiring` or `#nowhiring`.

1. `#remote`
1. `#remotework`
1. `#futureofwork`
1. `#goremote`
1. `#digitalnomad`
1. `#remoteworking`
1. `#workanywhere`
1. `#virtualteam`
1. `#telecommute`
1. `#locationindependent`

Working in an all-remote environment is unique. Be sure to read over the distinct [benefits](/company/culture/all-remote/benefits/) and [drawbacks](/company/culture/all-remote/drawbacks/) when considering if such a setting is ideal for you. 

----

Return to the main [all-remote page](/company/culture/all-remote/).
