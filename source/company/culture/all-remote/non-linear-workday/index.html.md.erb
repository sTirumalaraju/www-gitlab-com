---
layout: handbook-page-toc
title: "The non-linear workday: reimagining routine in an all-remote environment"
canonical_path: "/company/culture/all-remote/non-linear-workday/"
description: The non-linear workday: reimagining routine in an all-remote environment
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

How diverse, invigorating, gratifying, and productive could your day be if you threw away the notion that you had to stick to a daily routine? 

On this page, we're detailing what life can look and feel like when embracing a non-linear workday, paired with suggestions on catalyzing your imagination to consider possibilities that simply are not possible in a colocated, synchronous workplace.

## Breaking preconceived notions about routine

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ZlUpDkoq_v0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Darren M. (GitLab's Head of Remote) and Elisa R. (Founder of The Cowork Experience) discuss the impact and purpose of routines when looking at satisfaction and productivity.* 

A common recommendation for those new to remote work is to find a routine early on, and stick to it. While this may be sound advice for *some*, it ignores the reality that remote enables a complete deconstruction of the perceived need for routine. 

Routine is a common suggestion not necessarily because it is *good*, but because it is *tradition*. Routine is mandated in a colocated environment, where team members are required to commute and work between fixed hours. We have been conditioned to believe that routine keeps us disciplined, when in reality, routine simply makes it easier for colocated companies to keep workers in line. 

Remote decouples routine from responsibility. Indeed, [managers of one](/handbook/values/#managers-of-one) thrive in a remote setting, and exhibit *more* discipline to work well when no one is looking. 

Many people adhere to routines simply because they know no other way. Remote allows another option, thanks to the tremendous benefits of [asyncronous workflows](/company/culture/all-remote/asynchronous/), [handbook-first documentation](/company/culture/all-remote/handbook-first-documentation/), and companywide [transparency](/handbook/values/#transparency). 

## What is a non-linear workday?

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/SeTTehyZsFc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://youtu.be/SeTTehyZsFc) above, GitLab's Head of Remote talks with Megan Dilley (Director, Remote Work Association) about remote work's democritizating power, the importance of community, and projections for what life will look like after the great remote work migration of 2020. Discover more in GitLab's [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc).*

Perhaps the most useful approach to describing a non-linear workday is to share an example. In transparency, this is an actual example from a GitLab team member. 

* Darren wakes up at 6:00 AM on a Tuesday in Montana. After a shockingly brief 30 second commute from one room to another, he's ready to start his workday, coffee in hand.

* Darren begins work at 6:30 AM, and works until 8:30 AM. He changes his Slack status to a skiier emoji, noting to colleagues that he'll be out through 4:30 PM local time. (This distinction is important, as his company works asynchronously with colleagues across an ever-changing array of time zones. Said another way, time is relative.)

* He then cooks breakfast, eats with his family, and packs his ski gear into the car. By 10:00 AM, the family is skiing. There is almost no one on the mountain, as the vast majority of the world is following a typical routine that pulls them into a colocated office by 10:00 AM on any given Tuesday. Lift lines are nonexistant. Lift tickets were heavily discounted — it's Tuesday, after all — and there was no traffic to contend with. All of these are [spoils of an off-peak life](/company/culture/all-remote/people/#the-spoils-of-an-off-peak-life).

* By 3:00 PM, Darren and his family ski back to the car, refreshed after an exhilarating day. Given that they're departing the mountain before rush hour has any impact on traffic, they're back to their rental apartment by 3:30 PM. 

* After a shower and an early dinner, Darren logs back on to work at 4:30 PM, enthused to tackle ongoing projects and help move issues forward. But first, he shares a few photos he grabbed while skiing — something that is encouraged when you operate in a [non-judgemental culture](/company/culture/all-remote/mental-health/#creating-a-non-judgemental-culture) and measure people on [results, not hours](/company/culture/all-remote/values/#results). Though working remotely with hundreds of colleagues across six continents, this deliberate approach to [informal communication](/company/culture/all-remote/informal-communication/) creates personal bonds that are, in many ways, deeper than those formed in-office. 

* Because it's winter in Montana, it's fairly dark outside by 5:00 PM. Darren has maximized his daylight hours, and has time-shifted his working day to primarily occur during darkness. Given that he would likely be indoors during this time anyway, it's more conducive to work. There is no pull to leave and explore the outdoors when it is dark. Instead, it is an ideal time to work, despite the fact that resuming your work day while most others are ending theirs is incongruent with the conventional definition of routine. 

![GitLab all-remote team](/images/all-remote/all-remote-skiing-montana.jpg){: .shadow.medium.center}

We should pause at this point and recognize that time is still relative. When Darren resumes his workday at 4:30 PM, he has six more hours to contribute if working a standard eight-hour day. It is important to not get caught up in local times. 4:30 PM may sound like an absurd time to resume working, but that's morning, afternoon, and night for various other members on his team. 

The non-linear workday decouples time from work and acts as a forcing function to embrace [asynchronous workflows](/company/culture/all-remote/asynchronous/). Local times are only as important as your company relies on synchronicity to get things done. 

The more this bothers you, the further you need to distance your organization from synchronous defaults. 

* Darren resumes his work from 4:30 PM to 10:30 PM, relying on [low-context documentation](/company/culture/all-remote/effective-communication/#understanding-low-context-communication) created by his colleagues while he was skiing to understand the current state of work that he is involved in. He contributes asynchronously, as to minimize required [meetings](/company/culture/all-remote/meetings/) and be considerate of colleagues who may also be working in non-linear fashion.

## Fostering your imagination 

![GitLab all-remote team](/images/all-remote/remote_work_02.jpg){: .shadow.medium.center}

* What could your life be if no two days were ever the same? 
* What do you long to do that you think a work routine is holding you back from? 
* How would you integrate work into your life if you were given permission to scrap everything you thought you knew about the importance of routines? 

Answering the above will allow you to truly evaluate what elements of routine are beneficial to you, and which are holding you back. 

The above skiing example is a maximally [efficient](/handbook/values/#efficiency) day. It was a full working day, and a full day of exploring and spending meaningful time with family. The above team member could've opted to take PTO ([paid time off](/handbook/paid-time-off/)), or opted for a shorter ski session. He could've taken a half-day, thereby extending the ski session or simply providing more buffer time between work and play. 

You could swap anything in for skiing and envision how it could apply to you. From participating in midday school activities with your children, to helping with a midday community service event, to being available to serve as support during an important medical appointment for a loved one — the examples are endless.

The point is, a non-linear mindset gives you options to [break free from routine](/company/culture/all-remote/people/) and structure each day differently. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/csS3iTjIHNo?start=1193" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In a [GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) conversation, Dani from Ceridian asked GitLab's Head of Remote the following question: "What’s a question that people don’t ask, but you feel they should, about remote work?"*

*His answer is verbalized in the video above, and partially transcribed below.*

> What could life look like if I never had to commute again? If I did not have to be in this particular city for work? 
>
> What you're seeing en masse (due to COVID-19) is people being thrust into remote, and they're trying to replicate virtually the in-office experience. 
>
> I want people to give themselves permission to realize that remote is an entirely new universe. You don't have the commute. You don't have the stigma and office expectations. You can potentially shift your day to work during your peak productivity hours, which opens up hours that have never been accessible to you before.  
>
> It's the ultimate blank slate, the ultimate life cheat code. There tends to be remoter's guilt, where you feel like you need to over-produce as a remote employee to compensate for the removal of a commute. The commute was never the employer's to begin with; that was always your time. 
>
> You can do something with that. You can be innovative with it. — *Darren M., Head of Remote, GitLab*

## What is required to enable non-linear workdays?

The example detailed here would not have been possible without a few realities already in place.

* The company must work [handbook-first](/handbook/handbook-usage/#why-handbook-first), such that all meaningful takeaways from conversations are documented in their proper place, around the clock. 
* The company must embrace [asynchronous workflows](/company/culture/all-remote/asynchronous/) (including [tools like GitLab](/stages-devops-lifecycle/) as well as processes) in a deliberate, intentional, and thorough way. 
* The company must support a [non-judgemental culture](/company/culture/all-remote/mental-health/#creating-a-non-judgemental-culture), which measures team members on [results rather than hours](/company/culture/all-remote/values/#results). This enables people to enter and exit work as they so choose, with no fear of retribution for doing something as unorthodox as skiing while everyone else works, and working while everyone else wishes they would've gone skiing. 
* A personal dedication to being a [manager of one](/handbook/values/#managers-of-one), able to focus on the right tasks while working from a foreign environment. 
* An understanding that not every single day will look like this. Even masters of non-linear workdays recognize that some days are less amenable to midday excursions than others. Rather than being sour about that, embrace the thrill of it being possible at all, and put effort into structuring your upcoming schedule in a way that allows for such days. As a manager of one, you have to take control over (and be accountable for) your schedule. Otherwise, other forces of the world and work will control it for you. 

### What about meetings?

 The obvious question when discussing such examples is this: "How do you leave work during a time when [meetings](/company/culture/all-remote/meetings/) are most likely to be scheduled?"

The not-so-obvious answer is: Create a workplace culture where meetings are a last resort, and ensure that unavoidable meetings can be [contributed to asynchronously](/company/culture/all-remote/meetings/#have-an-agenda). 

It bears repeating that not every single day will present itself as a natural, meeting-free day. However, the more intentional your company is about ruthlessly minimizing meetings, [separating decision gathering from decision making](/company/culture/all-remote/management/#separating-decision-gathering-from-decision-making), and insisting that all work begin where it eventually needs to end up (e.g. in a [GitLab issue](https://docs.gitlab.com/ee/user/project/issues/) or [merge request](/blog/2019/12/19/future-merge-requests-realtime-collab/)), the more feasible it will be. You'll also realize benefits on the [mental health](/company/culture/all-remote/mental-health/) front.

GitLab's [approach to meetings](/company/culture/all-remote/meetings/), as with all of our processes, is public in our [handbook](/handbook/). We encourage leaders to study, implement, and make suggestions for improvement. 

## GitLab Knowledge Assessment: Non-Linear Workday 

Anyone can test their knowledge on the non-linear workday of working in an all-remote enviornment by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSdCfWYrjKBxpd0wqVa_9rzPOCGjhs1JD9fCrJIihLyIj--sZg/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been completed, you will receive an email acknowledging the completion from GitLab with a summary of your answers. If you complete all knowledge assessments in the [Remote Work Foundation](/company/culture/all-remote/remote-certification/), you will receive an unaccredited [certification](/handbook/people-group/learning-and-development/certifications/). If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
