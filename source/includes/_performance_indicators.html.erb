<% if key_performance_indicators.any? %>
## Executive Summary

<table>
  <thead>
    <tr>
      <th>KPI</th>
      <th>Health</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <% key_performance_indicators.each do |kpi| %>
      <tr>
        <td><a href="#<%= kpi.name.parameterize %>"><%= kpi.name %></a></td>
        <td><%= color_code_health kpi.health.level %></td>
        <td><ul><% kpi.health.reasons.each do |reason| %>
          <li><%= reason %></li>
        <% end %></ul></td>
      </tr>
    <% end %>
  </tbody>
</table>
<% end %>

<% if key_performance_indicators.any? %>
## Key Performance Indicators

<% key_performance_indicators.each do |kpi| %>
  <%= partial 'includes/performance_indicator', locals: { performance_indicator: kpi } %>
<% end %>
<% end %>

<% if regular_performance_indicators.any? %>
## Regular Performance Indicators

<% regular_performance_indicators.each do |rpi| %>
  <%= partial 'includes/performance_indicator', locals: { performance_indicator: rpi } %>
<% end %>
<% end %>

## Other PI Pages

<%= partial 'includes/performance_indicator_links' %>

## Legends

### Health

<%= partial 'includes/performance_indicator_health' %>

### Instrumentation

<%= partial 'includes/performance_indicator_instrumentation' %>

## How to work with pages like this

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/uZYM_MqXJ3g" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Pages

Pages, such as the [Engineering Function Performance Indicators page](/handbook/engineering/performance-indicators/) are rendered by an [ERB](https://en.wikipedia.org/wiki/ERuby) template that contains HTML code.

*  Any changes to the [Performance Indicator (PI) ERB template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/includes/_performance_indicators.html.erb) will cascade to all links under the `Other PI Pages` section
*  To update the options for the `maturity` table, make changes to [the Performance Indicators Maturities ERB file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/includes/performance_indicator_maturities.erb)
*  To update the way each individual (K)PI displays:
    -  make changes to [the Performance Indicators List ERB file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/includes/performance_indicator_list.erb) for how the (K)PI is displayed on the KPI Index page 
    -  make changes to [the Performance Indicators ERB file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/includes/performance_indicator.erb) for any other properties and its format 
*  When adding a new PI page, 
    -  create the new PI page in the business division's handbook section (like the [Engineering Division page](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/handbook/engineering/performance-indicators/index.html.md.erb)) 
    -  include the relative link to the PI page in [the Performance Indicators List ERB file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/includes/_performance_indicators_links.erb)
    -  add all the (K)PI information into the [Performance Indicators data file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml)

#### Helper Functions 

These ERB templates calls [custom helper functions](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/helpers/custom_helpers.rb) that extract and transform data from the [Performance Indicators data file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml).

*  The `kpi_list_by_org(org)` helper function takes a required string argument named `org` (deparment or division level) that returns all the KPIs (pi.is_key == true) for a specific organization grouping (pi.org == org) from the [Performance Indicators data file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml). 
*  The `pi_maturity_level(performance_indicator)` helper function automatically assigns a maturity level based on the availability of certain data properties for a particular PI. 
*  The `pi_maturity_reasons(performance_indicator)` helper function returns a `reason` for a PI maturity based on other data properties. 
*  The `performance_indicator_target(name)` helper function returns the target value from the `target` property for a specific PI from the [Performance Indicators data file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml).
*  The `performance_indicators(org)` takes a required string argument named `org` (deparment or division level) that returns two lists - a list of all KPIs and a list of all PIs for a specific organization grouping (department/division). 
*  The `signed_periscope_url(data)` takes in the sisense_data property information from the [Performance Indicators data file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml) and returns a signed chart URL for embedding a Sisense chart into the handbook. 


### Data

The heart of pages like this is the [Performance Indicators data file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml) which is in [YAML](https://yaml.org/) file. 
Each - denotes a dictionary of values for a new (K)PI. The current elements (or data properties) are: 

| Property    | Type     | Description |
|-------------|----------|-------------|
| `name`      | Required | String value of the name of the (K)PI |
| `base_path` | Required | Relative path to the performance indicator page that this (K)PI should live on |
| `definition`| Required | refer to [Parts of a KPI](/handbook/ceo/kpis/#parts-of-a-kpi) |
| `parent`    | Optional | should be used when a (K)PI is a subset of another PI. For example, we might care about Hiring vs Plan at the company level. The child would be the division and department levels, which would have the parent flag. |
| `target`    | Required | The target or cap for the (K)PI. Please use `Unknown until we reach maturity level 2` if this is not yet defined.  |
| `org`       | Required | the organizational grouping (Ex: Engineering Function or Development Department)  |
| `is_key`    | Required | boolean value (true/false) that indicates if it is a (key) performance indicator |
| `health`    | Required | has two additional elements/property - level (inclusive value between 0-3) and reasons. This should be updated monthly before Key Meetings by the DRI. |
| `urls`      | Optional | list of urls associated with the (K)PI  |
| `sisense_data` | Optional | contains elements/properties related to Sisense, including chart (numeric Sisense widget ID), dashboard (numeric Sisense dashboard ID),  shared_dashboard (Sisense shared dashboard ID), embed (v2) |
| `public` | Optional | boolean flag that can be set to `false` where a (K)PI does not meet the public guidelines. |
| `instrumentation` | Optional | has two additional elements/property - level (inclusive value between 0-3) and reasons. This should be updated monthly before Key Meetings by the DRI. |


### Guidelines

*  Each KPI chart should be a timeseries chart.
    - Use Purple bars to denote values.
    - Use a single Green bar for the current month (in progress).
    - Use a Red stepped-line for timeseries target.
    - Optional: Use a Black dashed-line for rolling average.
*  Each KPI should have a standalone dashboard with a single chart representing the KPI and a text box with a link back to the handbook definition. 
    - In Sisense, [create a shared dashboard link](https://dtdocs.sisense.com/article/share-dashboards) to get the shared dashboard ID. 
    - In Sisense, [use the Share Link action of the chart](https://dtdocs.sisense.com/article/chart-options#ShareLink) to get the chart (widget_id) and the dashboard ID. 
    - Add the `shared_dashboard`, `chart` , and the `dashboard` key-value pairs to the [Performance Indicators data file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators.yml) under the `sisense_data` property
*  Avoid `:` in strings as it's an important character in YAML and will confuse the data parsing process. Put the string in "quotes" if you really need to use a `:`
*  `urls:` should be an array (indented lines starting with dashes) even if you only have one url
*  Both `maturity.level` and `health.level` display a value between 0 and 3 (inclusive). The health value is a manual input while the maturity is an automated value based on other discrete (K)PI data properties. 

