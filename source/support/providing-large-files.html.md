---
layout: markdown_page
title: Provide large files to GitLab support
description: "How to share large files with the GitLab support team."
---

# Provide large files to GitLab support

Zendesk has a [maximum attachment size](https://support.zendesk.com/hc/en-us/articles/235860287-What-is-the-maximum-attachment-size-I-can-include-in-ticket-comments-) of 20MB _per file_. Zendesk will not allow us to increase this limit any further.

You have a handful of options for sharing a file that is larger than 20MB:

## Compression

If you're sending a text file or an archive with mostly text files, then please compress it. Use either bz2 (preferred) or gzip (faster) compression and it should compress to a small percentage of its original size. Zip compression is fine if you're on Windows.

If this brings your file under the 20MB limit, then simply attach it to the ticket and be done. If not, then see below for more options.

## File sharing services

Please feel free to use your own choice of file sharing service. Be aware that submitting potentially sensitive data to 3rd parties
does carry a risk, so be sure to check with your security team for a properly vetted choice.

## GitLab private project

This is a fairly straight-forward option. [Sign up for a gitlab.com account](https://gitlab.com/users/sign_in#register-pane) if you don't already have one. Then create a private project and invite the Support Engineer(s) assisting you with **Reporter** access or higher.
You can find the GitLab handle of the support engineer(s) you need to add by searching for the respective engineer(s) on our [team](../company/team/) page.

## Use GNU split

Since the attachment is applied _per file_, we can split that one file into many and attach all of them to a ticket.

The `split` command is bundled in GNU coreutils, which should be installed on all Unix-like operating systems by default. Please avoid using alternatives like winzip, winrar, 7zip, etc. We've included an example below:

```sh
# split -b <size> <source file> <prefix for new files>
split -b 19M source-file.tar.bz2 "target-file.tar.bz2."
```

This will create many files in your current directory such as `target-file.tar.bz2.aa` and `target-file.tar.bz2.ab`. These files can be later joined with the `cat` command.

```sh
cat target-file.tar.bz2.* > joined-file.tar.bz2
```
