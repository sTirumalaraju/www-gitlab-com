---
layout: job_family_page
title: "Financial Planning and Analysis"
---

## Levels

## Analyst

You will be a key member of the FP&A team helping the team provide data driven decision support using strong business and financial acumen.  
You will maintain financial models, execute financial reporting, summarize data and own parts of our strategic planning, forecasting and variance processes. Your goal is to help make GitLab predictable.

Ultimately, you will ensure GitLab's resource allocation is aligned with business objectives.

### Job Grade

The FP&A Analyst is a [grade 6](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Business Partnership: Support Senior Financial Analysts and Managers to engage with leadership regarding business strategy, go to market, functional strategy, spending initiatives, ad hoc financial analysis and monthly, quarterly and annual planning. 
- Financial Modeling: Maintain and improve financial models to plan, measure and forecast the business. 
- Planning and Financial Analysis: Participate in annual planning, long-term planning, rolling forecast and variance process.
- Data Analysis: Produce KPI reporting and root cause trends in KPIs. Prepare insights for variance and key meetings.
- Financial process improvement: Execute improvements to processes within your own workflow. Document in company handbook.
- Project Management: Run small sized projects that improve our ability to make better data driven insights or make the company more efficient.
- Communication: Prepare and review visualizations of financial data to promote internal and external understanding of the company’s financial results to FP&A leadership, functional managers and directors. Clearly articulate insights.
- Live GitLab Values every day

### Requirements

- Financial Acumen: Have mastery understanding of financial statements.
- Financial Modeling: Be able to understand and update financial models that follow industry best practice. Expertise in Google sheets (we do not use excel for modeling purposes)
- Business Acumen: Be able to understand the business at a level to understand company and fp&a team strategy.
- Data Analysis: A passion for understanding business questions and making data driven insights. Excellent analytical skills. SQL experience preferred.
- Systems: Hands-on experience with financial and visualization software. Netsuite, Sisense and Adaptive Planning a plus
- Business Partnership: Consistent track record of using quantitative analysis to impact key business decisions.
- Strong communication: Ability to present financial data using detailed reports and charts
- Experience recommendation: 3-5 years of experience in a finance role ideally with enterprise SaaS software model
- BS degree in Finance, Accounting or Economics or relevant degree.

## Performance Indicators

- [Budget, Forecast Creation Cycle Time](/handbook/finance/financial-planning-and-analysis/#budget-forecast-creation-cycle-time)
- [Plan vs Actual](/handbook/finance/financial-planning-and-analysis/#plan-vs-actual)
- [Team Morale Score](/handbook/finance/financial-planning-and-analysis/#team-morale-score)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with the Manager, FP&A
- Next, candidates will be invited to schedule a 45 minute interview with one or two FP&A team members
- Next, candidates will be invited to schedule a 45 minute interview with the EVP of the function they support or CAO
- Next, candidates will be invited to schedule a 45 minute interview with our VP Finance
- Successful candidates will subsequently be made an offer via email


## Senior Analyst

You will partner with executive team leaders across the company to provide data driven decision support using strong business and financial acumen.  
You will build and create financial models, execute financial reporting, summarize data and own parts of our strategic planning, forecasting and variance processes. Your goal is to help make GitLab predictable.

Ultimately, you will ensure GitLab's resource allocation is aligned with business objectives.

### Job Grade

The Senior FP&A Analyst is a [grade 7](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Business Partnership: Engage with leadership regarding business strategy, go to market, functional strategy, spending initiatives, ad hoc financial analysis and monthly, quarterly and annual planning. Collaborate with accounting/data/IT teams on process improvement projects. 
- Financial Modeling: Build bottoms up financial models to plan, measure and forecast the business. 
- Planning and Financial Analysis: Participate in annual planning, long-term planning, rolling forecast and variance process, make recommendations to improve the process. Explore investment options and present risk and opportunities.
- Data Analysis: Summarize key data driven insights to executive leadership that drive better outcomes, an increase in revenue or decrease in cost. Participate in monthly key reviews for your functional area.
- Financial process improvement: Execute improvements to processes within your own workflow and of your peers. Document in company handbook.
- Project Management: Run medium sized projects that improve our ability to make better data driven insights or make the company more efficient.
- Communication: Prepare and review visualizations of financial data to promote internal and external understanding of the company’s financial results to CFO, CFO staff and a functional EVP. Clearly articulate insights.
- Live GitLab Values every day

### Requirements

- Financial Acumen: Have mastery understanding of financial statements.
- Financial Modeling: Be able to create financial models that follow industry best practice. Expertise in Google sheets (we do not use excel for modeling purposes)
- Business Acumen: Be able to understand the business at a level to understand EVP priorities.
- Data Analysis: A passion for understanding business questions and making data driven insights. Excellent analytical skills. SQL experience preferred.
- Systems: Hands-on experience with financial and visualization software. Netsuite, Sisense and Adaptive Planning a plus
- Business Partnership: Consistent track record of using quantitative analysis to impact key business decisions.
- Strong communication: Ability to present financial data using detailed reports and charts
- Experience owning a business function as a finance business partner
- Experience recommendation: 5-7 years of experience in a finance role ideally with enterprise SaaS software model
- BS degree in Finance, Accounting or Economics or relevant degree.  MBA and relevant certification (e.g. CFA/CPA) is a plus

## Performance Indicators

- [Budget, Forecast Creation Cycle Time](/handbook/finance/financial-planning-and-analysis/#budget-forecast-creation-cycle-time)
- [Plan vs Actual](/handbook/finance/financial-planning-and-analysis/#plan-vs-actual)
- [Team Morale Score](/handbook/finance/financial-planning-and-analysis/#team-morale-score)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with the Manager, FP&A
- Next, candidates will be invited to schedule a 45 minute interview with one or two FP&A team members
- Next, candidates will be invited to schedule a 45 minute interview with the EVP of the function they support or CAO
- Finally, candidates will be invited to schedule a 60 minute interview with our VP Finance which will include some case questions


## Manager, FP&A 

You will partner with executive team leaders across the company to provide data driven decision support using strong business and financial acumen.  You will own parts of our strategic planning, forecasting and variance processes. Your goal is to help make GitLab predictable.

As a Manager, you will hire, manage and coach a team of individuals who are highly skilled at financial analysis to facilitate the corporate planning and reporting function. You will be an expert in your functional area.

Ultimately, you will ensure GitLab's resource allocation is  aligned with business objectives.

### Job Grade

The Manager, FP&A is a [grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Business Partnership: Engage with leadership regarding business strategy, go to market, functional strategy, spending initiatives, ad hoc financial analysis and monthly, quarterly and annual planning. Collaborate with accounting/data/IT teams on process improvement projects. 
- Financial Modeling: Build bottoms up financial models to plan, measure and forecast the business. 
- Planning and Financial Analysis: Participate in annual planning, long-term planning, rolling forecast and variance process, make recommendations to improve the process. Explore investment options and present risk and opportunities.
- Data Analysis: Summarize key data driven insights to executive leadership that drive better outcomes, an increase in revenue or decrease in cost. Participate in monthly key reviews for your functional area.
- Review: Review work and analyses produced by the team you manage or from other members of the team.
- Financial process improvement: Recommend and execute improvements to processes and policies within the FP&A team. Document in company handbook.
- Project Management: Run large complex projects that improve our ability to make better data driven insights or makes the company more efficient.
- Communication: Prepare and review visualizations of financial data to promote internal and external understanding of the company’s financial results. Basic influence to CFO, CFO staff and functional EVPs. Clearly articulate insights.
- Management: Hire, build, coach and manage a highly productive team day to day. Learning the craft of management as first level management role.
- Live GitLab Values every day

### Requirements

- Financial Acumen: Have mastery understanding of financial statements.
- Financial Modeling: Be able to create financial models that follow industry best practice. Expertise in Google sheets (we do not use excel for modeling purposes)
- Business Acumen: Be able to understand the business at a level to influence EVP priorities.
- Data Analysis: A passion for understanding business questions and making data driven insights. Excellent analytical skills. SQL experience preferred.
- Systems: Hands-on experience with financial and visualization software. Netsuite, Sisense and Adaptive Planning a plus
- Business Partnership: Consistent track record of using quantitative analysis to impact key business decisions.
- Strong communication: Ability to present financial data concisely through written and oral communication. Demonstrated an ability to influence business stakeholders.
- Experience as an expert owning a  business function as a finance business partner
- Experience recommendation: 7-10 years of experience in a finance role ideally with enterprise SaaS software model
- BS degree in Finance, Accounting or Economics or relevant degree.  MBA or relevant certification (e.g. CFA/CPA) is a plus

## Performance Indicators

- [Budget, Forecast Creation Cycle Time](/handbook/finance/financial-planning-and-analysis/#budget-forecast-creation-cycle-time)
- [Plan vs Actual](/handbook/finance/financial-planning-and-analysis/#plan-vs-actual)
- [Team Morale Score](/handbook/finance/financial-planning-and-analysis/#team-morale-score)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our VP Finance
- Next, candidates will be invited to schedule a 45 minute interview with one or two FP&A team members
- Next, candidates will be invited to schedule a 45 minute interview with our CAO 
- Next, candidates will be invited to schedule a 45 minute interview with the EVP of the function to support
- Next, candidates will be invited to schedule a 45 minute interview with our CFO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our hiring page.

## Sr. Manager, FP&A 

You will partner with executive team leaders across the company to provide data driven decision support using strong business and financial acumen.  You will own parts of our strategic planning, forecasting and variance processes. Your goal is to help make GitLab predictable.

As a Sr. Manager, you will hire, manage and coach a team of individuals who are highly skilled at financial analysis to facilitate the corporate planning and reporting function. You will be an expert in your functional area.

Ultimately, you will ensure GitLab's resource allocation is  aligned with business objectives.

### Job Grade

The Senior Manager, FP&A is a [grade 9](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Business Partnership: Engage with leadership regarding business strategy, go to market, functional strategy, spending initiatives, ad hoc financial analysis and monthly, quarterly and annual planning. Collaborate with accounting/data/IT teams on process improvement projects. 
- Financial Modeling: Build bottoms up financial models to plan, measure and forecast the business. 
- Planning and Financial Analysis: Participate in annual planning, long-term planning, rolling forecast and variance process, make recommendations to improve the process. Explore investment options and present risk and opportunities.
- Data Analysis: Summarize key data driven insights to executive leadership that drive better outcomes, an increase in revenue or decrease in cost. Participate in monthly key reviews for your functional area.
- Review: Review work and analyses produced by the team you manage or from other members of the team.
- Financial process improvement: Recommend and execute improvements to processes and policies within the FP&A team and identify/influence changes in other teams (accounting, business function). Document in company handbook.
- Project Management: Run large cross-functional projects / working groups that improve our ability to make better data driven insights or makes the company more efficient.
- Communication: Prepare and review visualizations of financial data to promote internal and external understanding of the company’s financial results. Strong influence to CFO, CFO staff and functional EVPs. Clearly articulate insights.
- Management: Hire, build, coach and manage a highly productive team day to day. Strong management skills and techniques especially around prioritization.
- Live GitLab Values every day
### Requirements

- Financial Acumen: Have mastery understanding of financial statements.
- Financial Modeling: Be able to create financial models that follow industry best practice. Expertise in Google sheets (we do not use excel for modeling purposes)
- Business Acumen: Be able to understand the business at a level to influence EVP priorities.
- Data Analysis: A passion for understanding business questions and making data driven insights. Excellent analytical skills. SQL experience preferred.
- Systems: Hands-on experience with financial and visualization software. Netsuite, Sisense and Adaptive Planning a plus
- Business Partnership: Consistent track record of using quantitative analysis to impact key business decisions.
- Strong communication: Ability to present financial data concisely through written and oral communication. Demonstrated an ability to influence business stakeholders.
- Management: 3-5 years of management experience in a financial role
- Experience as an expert owning a large business function or multiple functions as a finance business partner
- Experience recommendation: 10-12 years of experience in a finance role ideally with enterprise SaaS software model
- BS degree in Finance, Accounting or Economics or relevant degree.  MBA or relevant certification (e.g. CFA/CPA) is a plus

## Performance Indicators

- [Budget, Forecast Creation Cycle Time](/handbook/finance/financial-planning-and-analysis/#budget-forecast-creation-cycle-time)
- [Plan vs Actual](/handbook/finance/financial-planning-and-analysis/#plan-vs-actual)
- [Team Morale Score](/handbook/finance/financial-planning-and-analysis/#team-morale-score)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our VP Finance
- Next, candidates will be invited to schedule a 45 minute interview with one or two FP&A team members
- Next, candidates will be invited to schedule a 45 minute interview with our CAO 
- Next, candidates will be invited to schedule a 45 minute interview with the EVP of the function to support
- Next, candidates will be invited to schedule a 45 minute interview with our CFO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our hiring page.

## Director, FP&A 

You will be a critical leader on the FP&A team. You will partner with executive team leaders across the company to provide data driven decision support using strong business and financial acumen.  You will own parts of our strategic planning, forecasting and variance processes. Your goal is to help make GitLab predictable.

As a Director, you will set vision, hire, manage and coach a team of individuals who are highly skilled at financial analysis to facilitate the corporate planning and analysis function. You will be an expert in a very complex functional area or own multiple functional areas.

Ultimately, you will ensure GitLab's resource allocation is  aligned with business objectives.

### Job Grade

The Director, FP&A is a [grade 10](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Business Partnership: Engage with leadership regarding business strategy, go to market, functional strategy, spending initiatives, ad hoc financial analysis and monthly, quarterly and annual planning. Collaborate with accounting/data/IT teams on process improvement projects. 
- Financial Modeling: Build bottoms up financial models to plan, measure and forecast the business. 
- Planning and Financial Analysis: Participate in annual planning, long-term planning, rolling forecast and variance process, make recommendations to improve the process. Explore investment options and present risk and opportunities.
- Data Analysis: Summarize key data driven insights to executive leadership that drive better outcomes, an increase in revenue or decrease in cost. Participate in monthly key reviews for your functional area.
- Review: Review work and analyses produced by the team you manage or from other members of the team. Expert at providing feedback.
- Financial process improvement: Identify and drive improvements to processes and policies within the FP&A team and in business partner groups (accounting, business function). Document in company handbook.
- Project Management: Run large cross-functional projects / working groups that improve our ability to make better data driven insights or makes the company more efficient. Drive system improvement initiatives.
- Communication: Prepare and review visualizations of financial data to promote internal and external understanding of the company’s financial results. Expert influence to CFO, CFO staff and functional EVPs. Clearly articulate insights.
- Management: Set the vision, hire, build, coach and manage a highly productive team day to day. Expert management skills, especially at developing talent. Master at setting goals and priorities for the team. May manage managers.
- Be a culture definer and evolver of GitLab Values

### Requirements

- Financial Acumen: Have mastery understanding of financial statements.
- Financial Modeling: Be able to create financial models that follow industry best practice. Expertise in Google sheets (we do not use excel for modeling purposes)
- Business Acumen: Be able to understand the business at a level to influence EVP priorities and company strategy.
- Data Analysis: A passion for understanding business questions and making data driven insights. Excellent analytical skills. SQL experience preferred.
- Systems: Hands-on experience with financial and visualization software. Netsuite, Sisense and Adaptive Planning a plus
- Business Partnership: Consistent track record of using quantitative analysis to impact key business decisions.
- Strong communication: Ability to present financial data concisely through written and oral communication. Expert at influencing business stakeholders.
- Management: 5-7 years of management experience in a financial role
- Experience as an expert owning a large business function or multiple functions as a finance business partner
- Experience recommendation: 12-15 years of experience in a finance role ideally with enterprise SaaS software model
- BS degree in Finance, Accounting or Economics or relevant degree.  MBA or relevant certification (e.g. CFA/CPA) is preferred

## Performance Indicators

- [Budget, Forecast Creation Cycle Time](/handbook/finance/financial-planning-and-analysis/#budget-forecast-creation-cycle-time)
- [Plan vs Actual](/handbook/finance/financial-planning-and-analysis/#plan-vs-actual)
- [Team Morale Score](/handbook/finance/financial-planning-and-analysis/#team-morale-score)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our VP Finance
- Next, candidates will be invited to schedule a 45 minute interview with one or two FP&A team members
- Next, candidates may be invited to schedule a 45 minute interview with a People Ops team member
- Next, candidates will be invited to schedule a 45 minute interview with our CAO 
- Next, candidates will be invited to schedule a 45 minute interview with the EVP of the function to support
- Next, candidates will be invited to schedule a 45 minute interview with our CFO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our hiring page.

## Career Ladder

The next step in the Financial Planning and Analysis job family is to move to the [VP, FP&A](/job-families/finance/vp-finance/) job family. 


