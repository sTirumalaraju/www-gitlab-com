---
layout: job_family_page
title: "All-Remote Marketing"
---
# All-Remote Marketing

The All-Remote Marketing team holds a unique position on the corporate marketing team to develop and tell the story of GitLab’s remote culture. As a pioneer of remote work, this team will tell the story of GitLab’s global remote employees, remote work processes, transparent culture and the movement to remote work that GitLab has created.

## Responsibilities

- Own the story around GitLab’s remote culture and transparent work philosophy that is changing the future of how people work.
- Create and execute a content strategy and thought leadership platform to tell GitLab’s story about remote work and transparent culture.
- Use such tactics as online publishing, social, events, public relations, and partnerships to demonstrate GitLab’s leadership in the future of work movement.
- Maintain and evolve a standalone digital publication focused on GitLab’s leadership in remote work culture in the context of the broader movement. Manage content production from start to finish.
- Work with employees around the globe to highlight remote culture stories.
- Collaborate directly with the CEO to document GitLab’s remote and transparent culture philosophies.
- Follow the movement to remote work with a journalist’s eye, and cover as appropriate.
- Plug in to bigger opportunities to drive awareness around GitLab’s remote work culture.
- Employ an ethnographic storytelling approach to document and share authentic, credible stories from the movement offering insights that can be applied to solve problems throughout the organization and also adopted by others outside of GitLab.
- Work cross-functionally to develop and share key insights that can be applied across teams and programs: HR (Culture & Recruiting), Product Management and Design (Ethnographic research), Community, Internal Communications, etc.

## Requirements

- Proven track record developing content marketing strategies and awareness/thought leadership campaigns.
- A passion for remote work, and a proven ability to execute remotely.
- Ability to travel frequently, internationally.
- A natural storyteller with excellent narration and writing skills.
- Ability to gracefully handle day-to-day tasks, showcasing compassion and empathy. 
- Ability to coordinate across many teams and perform in fast-moving startup environment.
- Proven ability to be [self-directed](/company/culture/all-remote/self-service/) and work with minimal supervision.
- Outstanding written and verbal communications skills.
- You embrace our [values](/handbook/values/), and work in accordance with those values.
- Ability to use GitLab

## Levels

### Coordinator

- Maintain and update list of relevant remote conferences, events, experts, panelists, and outreach targets.
- Manage outreach and submissions for events, sponsorships, and speaking opportunities.
- Serve as writer and ghostwriter for all-remote content.
- Coordinate teams, people, and schedules as required across GitLab's all-remote initiatives.

#### Requirements

- 2+ years experience in content marketing, journalism or communications.
- Organized and efficient, able to develop, iterate, and execute against a plan.
- Able to clearly communicate new ideas and campaigns to both internal and external stakeholders.
- Team player with ability to work independently.
- Flexibility to adjust to the dynamic nature of a startup.

### All-Remote Content Manager

- Execute on the all-remote content strategy set forth by the Head of Remote.
- Work across teams to develop and execute programs in all-remote training, case studies and surveys, handbook development, and social marketing.
- Work closely with executives, spokespeople and the greater organization to develop e-books, how-to guides, blog posts, contributed articles, and panel submissions. 
- Compile successes and lessons learned as the team iterates on the campaign.
- Manage projects from start to finish.

#### Requirements

- 3-5 years experience in journalism, marketing, communications, or a related field.
- An understanding of GitLab's [remote work culture](/company/culture/all-remote/) and [philosophy](/company/culture/#life-at-gitlab).
- Experience in publishing, journalism, content marketing, social, and events.

### All-Remote Integrated Campaign Manager

- Execute on the all-remote content strategy set forth by the Head of Remote.
- Work across teams to help implement thought leadership activities.
- Compile successes and lessons learned as the team iterates on the campaign.
- Manage projects from start to finish.

#### Requirements

- 3-5 years experience in journalism, content marketing, integrated/experiential marketing, lead generation or brand activations.
- An understanding of GitLab's [remote work culture](/company/culture/all-remote/) and [philosophy](/company/culture/#life-at-gitlab).
- Experience executing thought leadership campaigns, working in content marketing, social and events.

### Head of Remote

- Develop overall strategy and vision for GitLab's remote work thought leadership campaign.
- Collaborate with GitLab leaders to curate GitLab's culture via content, social, events and public relations activities.
- Work with PeopleOps and Corporate Marketing to measure the program effectively.
- Collaboratively work across corporate marketing, PeopleOps and leadership to tell GitLab's remote work story.
- Responsible for ideation of all campaign activities, initiatives, OKRs, and reporting on results.

#### Requirements

- 5+ years experience in content marketing, journalism or communications.
- Proven experience managing and leading successful thought leadership campaigns and working in a remote work environment.
- Experince leading campaigns that include public relations, events, social, publishing and content.

## Performance Indicators

- Merged MRs within All-Remote handbook section
- Published articles, podcasts, and other content by media and external sources
- Total remote work-related videos uploaded to GitLab Unfiltered

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
Additional details about our process can be found on our [hiring page](/handbook/hiring).

Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Head of Remote
* A 30 minute interview with one of our Corporate Marketing Leads
* A 30 minute interview with our Senior Talent Branding Lead
* A 30 minute interview with our Senior Director of Corporate Marketing
* Finally, our CMO may choose to conduct a final interview
* Successful candidates will subsequently be made an offer via email
