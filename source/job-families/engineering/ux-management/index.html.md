---
layout: job_family_page
title: "UX Management"
---

# UX Management Roles at GitLab

Managers in the UX department at GitLab see the team as their product. While they are credible as designers and know the details of what Product Designers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

## Product Design Manager

The Product Design Manager reports to the Director of Product Design, and Product Designers report to the Product Design Manager.

### Responsibilities

* Identify improvements for UX design (e.g. look and feel, color, spacing, etc.).
* Hire a world class team of Product Designers.
* Help Product Designers grow their skills and expertise.
* Hold regular 1:1s with all members of their team.
* Create a sense of psychological safety on their team.
* Recommend UX technical and process improvements.
* Give clear, timely, and actionable feedback.
* Improve scheduling process to balance necessary UX improvements.
* Work with all parts of the organization (e.g. Backend, Frontend, Build, etc.) to improve overall UX.
* Strong sense of ownership, urgency, and drive.
* Excellent written and verbal communication skills, especially experience with executive-level communications.
* Ability to make concrete progress in the face of ambiguity and imperfect knowledge.

### Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

#### FE/UX Foundations

The Foundations team works on building a cohesive and consistent user experience, both visually and functionally. You'll be responsible for leading the direction of the experience design, visual style, and technical tooling of the GitLab product. You'll act as a centralized resource, helping to triage large-scale experience problems as the need arises.

You'll spend your time collaborating with a [cross-functional team](https://about.gitlab.com/handbook/product/product-categories/#ecosystem-group), helping to implement our [Design System](https://design.gitlab.com/), building comprehensive accessibility standards into our workflows, and defining guidelines and best practices that will inform how teams design and build products. A breakdown of the vision you’ll help to deliver within the FE/UX Foundation category can be found on our [product direction page](https://about.gitlab.com/direction/create/ecosystem/frontend-ux-foundations/).

**What you can expect in a Product Design Manager, FE/UX Foundations role at GitLab:**

* Advocate for good product design practices and bring a deep level of subject matter expertise to the team.
* Act as the experience owner of our [Pajamas Design System](https://design.gitlab.com/).
* Play a key role in both defining the direction of the category and regularly refining/scheduling issues during a given milestone.
* Work with your direct reports to build out a UX strategy for your team.
* Proactively learn other product areas in order to help your Product Designers propose design solutions that work for multiple use cases and scenarios across the product.
* Proactively identify large, strategic UX opportunities within the Foundations team and the product as a whole. Work with other Product Design Managers to drive cross-product initiatives.
* Review UX deliverables (research, designs, and so on) that your team creates, and provide feedback to ensure high-quality output.
* Identify strategic user research initiatives that span multiple product areas, and work with other Product Design/Research Managers to organize research efforts.
* Evangelize the value of UX to cross-functional GitLabbers, and influence the Product Managers you support to prioritize UX initiatives.
* Track coverage of Pajamas components in the GitLab product.
* Set up and manage collaborative processes within your team to ensure Product Designers and Researchers are actively working together. Make sure everyone has exposure to the work that is happening within the broader team.
* Foster a safe space for your team, where they’re comfortable sharing feedback and advocating for change they see as necessary. 
* Champion the importance of participation in critiques, content creation, and speaking engagements, while also participating alongside them.
* Hire a world-class team of Product Designers.
* Hold regular 1:1s with every member of your team, and create Individual Growth Plans with monthly check-ins.
* Collaborate with our [UX Director to define OKRs](https://about.gitlab.com/company/okrs/fy21-q2/) for our design practice. These OKRs will shape your team’s process, define your responsibilities as a manager, and feed into the responsibilities of your direct reports.
* Play a part in the evolution of our design culture as we take the next steps in expansion.

### Requirements

* A minimum of 3 years experience managing a group of designers.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Proficiency with pre-visualization software (e.g. Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group).
* Ability to use GitLab.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Product Designer. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too. 
* Interview with Product Design Manager. In this interview, we want you to share a case study presentation that provides insight to a problem you solved as part of a project you led. We'll look to understand the size and structure of your team, the goals of the project, the low-fidelity design work, the high-fidelity design output, how you/the team approached research, how you synthesized research data to inform design decisions, what design standards and guidelines you worked within, and how you collaborated with the wider team. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led, and your management style. The interviewer will also want to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with Product Director.
* Interview with VP of Engineering.

## Senior Product Design Manager

The Senior Product Design Manager reports to the Director of Product Design, and the Product Design Manager reports to the Senior Product Design Manager.

### Responsibilities

* Define and manage performance indicators for the Product Design team by actively contributing to the product design KPIs on the [UX KPIs](/handbook/engineering/ux/performance-indicators/) page in the handbook.
* Actively advocate for Product Design throughout the organization by ensuring Product Design responsibilities are reflected in the Product Development Flow.
* Help drive cross-product workflows by having an awareness of what's happening across all sections through active participation in design reviews, UX Showcases, and Group Conversations.
* Faciliatate the creation and execution of product design [OKRs](https://about.gitlab.com/company/okrs/) in collabration with the Product Design team and UX Leadership.
* Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* Coach Product Design Managers on how to conduct 1:1s, growth, and feedback conversations with their direct reports.
* Conduct quarterly skip levels with your reports' direct reports.
* Participate in Opportunity Canvas reviews by asking questions and providing feedback that focus on a great user experience.
* Hire a world-class team of Product Designers and Product Design Managers.

### Requirements

* A minimum of 3 years experience managing Product Design Managers.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Experience defining the high-level strategy (the why) and helping your team tie design and research back to that strategy.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group).
* Ability to use GitLab.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Product Design Manager. In this interview, the interviewer will focus on understanding your experience with driving design strategy, managing managers, and influencing the wider organization in which you worked. Examples of large, complex projects that had a significant impact to product experience will be helpful. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led, and your management style. The interviewer will also want to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with Product Director.
* Interview with VP of Engineering.

## Director of Product Design

The Director of Product Design reports to the UX Director, and Product Design Managers and Senior Product Design Managers report to the Director of Product Design.

The Director of Product Design role extends the Senior Product Design Manager role.

### Responsibilities

* Define and manage performance indicators for the Product Design team by independently managing product design KPIs on the [UX KPIs](/handbook/engineering/ux/performance-indicators/) page in the handbook.
* Actively advocate for Product Design throughout the organization by ensuring Product Design responsibilities are reflected in the Product Development Flow.
* Help drive cross-product workflows by having an awareness of what's happening across all sections through active participation in design reviews, UX Showcases, and Group Conversations.
* Independently manage the creation and execution of product design [OKRs](https://about.gitlab.com/company/okrs/) with feedback from the Product Design team and UX Leadership.
* Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* Communicate significant product design strategy decisions to leadership and the wider company.
* Coach Product Design Managers on how to conduct 1:1s, growth, and feedback conversations with their direct reports.
* Conduct quarterly skip levels with your reports' direct reports.
* Participate in Opportunity Canvas reviews by asking questions and providing feedback that focus on a great user experience.
* Hire a world-class team of Product Designers and Product Design Managers.

### Requirements

* A minimum of 10 years experience managing designers, and leading design for a product company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Proficiency with pre-visualization software (e.g. Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Product Designer. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with UX Manager (peer). In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with UX Director. In this interview, we will be looking for you to give some real insight into a problem you were solving as part of project you've led the work on. We'll look to understand the size and structure of the team you were a part of, the goals of the project, the low-fidelity design work, the high-fidelity design output, how you/the team approached research, how you synthesised research data to inform design decisions, what design standards and guidelines you worked within, and how you collaborated with the wider team. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with Product Director.
* Interview with VP of Engineering.


## Vice President of UX

The Vice President of User Experience will report to the Executive Vice President of Engineering, and Senior Managers and Directors of Product Design, UX Research, and Technical Writing report to them.

### Responsibilities

* Set a UX vision that aligns Product Design, UX Research, and Technical Writing with overall company objectives.
* Communicate broadly about important UX initiatives, setting an ambitious UX vision for the department, product, and company.
* Manage the UX budget, including compensation planning, non-headcount budget allocation, and tradeoff decisions.
* Interface regularly with executives on important decisions.
* Identify ways to elevate the GitLab product experience, manage initiatives to address those concerns, and track and communicate about progress.
* Help UX leaders grow their skills and leadership experience.
* Foster an open and collaborative culture based on trust in the UX department, where everyone feels empowered to do their best work.
* Ensure that UX is well-integrated into the [Product Development Flow](/handbook/product-development-flow/), and advocate for process changes that help product management, engineering, and UX work together to build a great experience.
* Establish and promote design guidelines, best practices, and standards, and help to drive GitLab's [design system](https://design.gitlab.com/) forward at a strategic level.
* Work with product leadership to prioritize research efforts, so that we validate whether we're solving the right problems in the right ways.
* Ensure that Development is included in the UX process by offering the opportunity to participate in and understand the outcomes of user research, give early feedback on upcoming designs, and participate in design system strategy.
* Define value-driven quarterly UX OKRs, and manage their execution.
* Represent the company at conferences, in media, in blog articles, in YouTube videos, and in other public venues.
* Hold regular skip-level 1:1s with all members of their team.
* Hire a world-class team of Product Designers, UX Researchers, Technical Writers, and their managers.

### Requirements

* A minimum of 10 years experience managing designers, researchers, and writers and leading design for a product company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with UX Manager. In this interview, the interviewer will spend a lot of time trying to understand the experience you have leading managers, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with UX Director. In this interview, we will be looking for you to give some real insight into a problem you were solving as part of a large initiative you led the work on. We'll look to understand the size and structure of the team, the goals of the project, how you/the team approached research, how you used research to inform decisions, and how you collaborated with the wider team. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with VP of Product.
* Interview with VP of Engineering.

As always, interviews and the screening call will be conducted via video.

See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)
