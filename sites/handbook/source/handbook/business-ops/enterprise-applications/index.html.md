---
layout: handbook-page-toc
title: "Enterprise Applications Team"
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# <i class="fas fa-bullhorn" id="biz-tech-icons"></i> What can you reach out to us about? 

* [Tech Stack](https://about.gitlab.com/handbook/business-ops/tech-stack-applications/)
* [Change Management](https://about.gitlab.com/handbook/business-ops/business-technology-change-management/)
* [Customer Portal](/handbook/business-ops/enterprise-applications/portal/)

# <i class="fas fa-users" id="biz-tech-icons"></i> The team

<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://about.gitlab.com/job-families/finance/business-system-analyst/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Business Systems Analyst</a>
  <a href="https://about.gitlab.com/job-families/finance/it-compliance/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">IT Compliance</a>
  <a href="https://about.gitlab.com/job-families/finance/finance-systems-administrator" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Finance Operations</a>
  <a href="https://about.gitlab.com/job-families/finance/integrations-engineer/" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Integrations Engineering</a>
</div>

## <i class="far fa-building" id="biz-tech-icons"></i> Business Systems Analysts

### What can we help you with? 

##### 1. Retrospectives

We can host your project retrospective.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

##### 2. Application Evaluations

We provide [templates](/handbook/business-ops/#templates) for vendor evaluations, can help write and review your user stories, and are happy to participate in tool evaluations that integrate with other applications or intersect with multiple departments.

Please involve us in all tool evaluations that integrate into the enterprise application ecosystem before beginning demos with vendors.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

##### 3. Solving Business Problems/Architectural Troubleshooting

We have high level views of the enterprise application ecosystem and can help troubleshoot where a business process has broken down or a system flow is not working as expected.
[Open an issue.](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new)

### What are the BSAs working on?

##### Finance

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1580145?assignee_username=broncato&&label_name[]=BTG-Project" class="btn btn-purple">Work Management Board</a>

* Work with the Finance and Accounting teams to improve workflows, processes, and application ecosystem
* Project work to implement new modules to tools
* Work with stakeholders from other teams like Field Ops and Fulfillment team that integration with the financial ecosystem.
* Work closely with Finance Systems Administrator

##### Portal integrations and operations

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1586460?assignee_username=j.carey&" class="btn btn-purple">Work Management Board</a>

*  Portal Analysis/Research, Documentation, and Architecture Recommendations
*  Partner to Go to Market Operations (Marketing and Sales Ops), Sales Systems, Customer Success Operations, and Channel Operations.

##### IT Operations and People Operations

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1596495?assignee_username=lisvinueza&" class="btn btn-purple">Work Management Board</a>

*  Partner to Employee Enablement and People Operations
*  Business Technology Operations and Workflows

## <i class="fas fa-user-shield" id="biz-tech-icons"></i> IT Compliance Manager

*  Partner to Legal and Security teams to ensure business compliance
   *  Offboarding
   *  GDPR/CCPA
   *  Business Preparedness Plans
   *  SOX Compliance

## <i class="fas fa-hand-holding-usd" id="biz-tech-icons"></i> Finance Operations

* Technical and Operational owner of the finance application ecosystem partnering with Finance and Accounting
* Maintains and optimizes the integrations of the ecosystem 
* Partner to Sales Ops, Sales Systems, Growth Teams and other departments where the integrations intersect and the data passes from one system into another
* Slack Channel `#financesystems_help`

##### What we are working on?

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1722830?assignee_username=awestbrook&" class="btn btn-purple">Work Management Board</a>
* Backlog and In progress issues related to all [finance systems](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/edit/master/README.md#systems-covered) 
    - Labels ~"BTG PS:: Backlog", ~"BTG PS::To Do", ~"BTG PS::In Progress", ~"BTG PS::Done" 
    - Gitlab.com > Assignee= @awestbrook

##### Types of Support

1. Access Request or change in access: [Queue](https://gitlab.com/groups/gitlab-com/-/boards/1765444?&label_name[]=FinSys%20-%20Access%20Request).
Submit [issue](https://about.gitlab.com/handbook/business-ops/employee-enablement/it-ops-team/access-requests/).
1. Breaks, bugs and incidents related to a system.
Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).
1. Enhancement Request for a system.
Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).
1. Other and questions.
Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).

##### Finance Systems Covered

1. Zuora: [Board](https://gitlab.com/groups/gitlab-com/-/boards/1723367?label_name[]=FinSys%20-%20Zuora) with everything slated to be done.
1. Netsuite
1. Tipalti 
1. Expensify
1. Stripe
1. TripActions
1. Avalara
1. CaptivateIQ
1. Workiva
1. FloQast

##### *Coming Soon*

1. AdaptiveInsights
1. RevPro

#### What's the status of my request?

- Every issue will have a tag of either
-   ~"BTG PS:: Backlog" > Unless a due date is indicated or urgency specified, non-access related issues will go into the backlog and prioritized bi-weekly
-   ~"BTG PS::To Do" > Team will look at the issue within a week of submitting
-   ~"BTG PS::In Progress" > Team is currently actively workiing on scoping out and gathering requirements
-   ~"BTG PS::Done"

##### Change Process

1. [Issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new) submitted with request
1. Request is approved by technical owner and business owner (as necessary).
([Approvals Queue](https://gitlab.com/groups/gitlab-com/-/boards/1774935))
1. Change pushed to sandbox/dev environment (as necessary)
1. Change validated
1. Change depoloyed to production environment