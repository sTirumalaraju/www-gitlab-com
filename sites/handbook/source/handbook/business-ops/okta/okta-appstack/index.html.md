---
layout: handbook-page-toc
title: "Okta Application Stack"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Below is the list of Applications currently configured in Okta. If you are an application owner for any of these applications that have incomplete or inaccurate information, please create a MR with the updated details and assign to @gitlab-rmitchell.

If you are an Application Owner of an Application not listed here please submit an [new application setup issue](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/new?issuable_template=okta_add_application) for your application. We will work with you to verify details and provide setup instructions.

## Okta Applications

### Baseline Entitlements for all Team Members

Below is a list of all baseline entitlements currently configured within Okta. For more information about baseline entitlements and to see a full list of these entitlements inside and outside of Okta, please refer to [Baseline Role-Based Entitlements Access Runbooks & Issue Templates](/handbook/engineering/security/#baseline-role-based-entitlements-access-runbooks--issue-templates).

| Application | Business Purpose | Configured Access | Okta Application Admin | SAML/SWA/Other | Deployment Status |
| --- | --- | --- | --- | --- | --- |
| [BambooHR](/handbook/business-ops/tech-stack/#bamboohr) | HR Portal  | Everyone | Brittany Rohde | SAML | Complete  |
| [Calendly](/handbook/business-ops/tech-stack/#calendly) |  Scheduling  | Everyone | TBA  | SWA | Complete |
| [Carta](/handbook/business-ops/tech-stack/#carta) |  Share Options | Everyone | Paul Machle  | SWA | Complete |
| [CultureAmp](/handbook/business-ops/tech-stack/#cultureamp) |  360 Review | Everyone | Jessica Mitchell | SWA | Complete |
| [Expensify](/handbook/business-ops/tech-stack/#expensify) |  Expenses | Everyone | Wilson Lau | SWA/SAML | Complete |
| [G-Suite](/handbook/business-ops/tech-stack/#g-suite) |  Email/Calendar/Drive | Everyone | Robert Mitchell | SWA | Complete for SWA. Needs SAML Conversion |
| [GitLab](/handbook/business-ops/tech-stack/#gitlab) | GitLab.com for Team | Everyone | Robert Mitchell  | SAML | Complete |
| [Greenhouse](/handbook/business-ops/tech-stack/#greenhouse) | Recruitment  | Everyone| Erich Wegscheider | SAML | Complete |
| [Moo](/handbook/business-ops/tech-stack/#moo) | Business Cards  | Everyone | Cassiana Gudgenov  | SWA | Complete |
| [Sisense](/handbook/business-ops/tech-stack/#periscope) | Data Analytics  | Everyone | Emilie Schario, Taylor Murphy | SAML  | Complete |
| [Slack](/handbook/business-ops/tech-stack/#slack) | Chat/Collaboration  | Everyone | Robert Mitchell | SWA/SAML| Complete for SWA/WebApp. Needs SAML Conversion |
| [TripActions](/handbook/business-ops/tech-stack/#tripactions) | Travel Booking | Everyone | Amber Lammers | OIDC | Complete |
| [Will Learning](/handbook/business-ops/tech-stack/#will-learning) | HR Education  | Everyone  | Robert Mitchell  | SWA | Complete  |
| [Zoom](/handbook/business-ops/tech-stack/#zoom) |  VideoConferencing | Everyone | Cassiana Gudgenov | SAML | Complete |


### Other Applications

| Application | Business Purpose | Configured Access | Okta Application Admin | SAML/SWA/Other | Deployment Status |
| --- | --- | --- | --- | --- | --- |
| [ADP](/handbook/business-ops/tech-stack/#adp) |  Payroll (US) | Finance Team | Wilson Lau  | SAML | Incomplete. Needs configuration to enable access and understand use. |
| Amazon Web Services | Cloud platform | Infrastructure, SRE, Solution Architechs | David Smith | Multiple options | Incomplete. Needs owner identification and configuration. |
| [Avalara](/handbook/business-ops/tech-stack/#avalara) | Taxation | Finance Department | Wilson Lau  | SWA | Complete |
| [Balsamiq Cloud](/handbook/business-ops/tech-stack/#balsamiq-cloud) | UX-Wireframing  | UX Department | Christie Lenneville | SWA | Complete |
| Betterment | Financial Advice | US-Based Team Members | Brittany Rohde | SWA | Complete |
| [Blackline](/handbook/business-ops/tech-stack/#blackline) | Accounting Automation | Finance Department Members | Kim Stithem  | SWA/SAML | SWA Complete. Needs SAML Conversion |
| BrowserStack | Browser Testing Application | PeopleOps Shared Account | Brittany Rohde | SWA (Shared Account) | Complete  |
| Cigna | PeopleOps | PeopleOps Shared Account | Brittany Rohde | SWA (Shared Account) | Complete  |
| [Chorus](/handbook/business-ops/tech-stack/#chorus) | Call Management | Everyone (Available by Request) | Amber Stahn | SWA  | Complete |
| [Clari](/handbook/business-ops/tech-stack/#clari) | Forecasting | Commercial and Enterprise Sales | Alex Tkach | SWA/SAML | SWA Complete. Needs SAML Conversion |
| [ContractWorks](/handbook/business-ops/tech-stack/#contractworks) | Contract Management  | TBA | TBA  | SAML  | Incomplete - Needs Engagement with Owner |
| [Cookiebot](/handbook/business-ops/tech-stack/#cookiebot) | Marketing | Marketing Dept | Sarah Daily | SWA  | Complete |
| Crayon | Sales Intelligence | Enterprice Sales, Commercial Sales, Strategic Marketing, Product Management | Clinton Sprauve | SAML | Complete |
| [Crowdin.com](/handbook/business-ops/tech-stack/#crowdincom) | Localisation  | Available by Request  | TBA | SWA  | Complete pending ownerID and team requirements |
| dev.gitlab.com | Dev Platform | Available by Request | Amber Lammers | SWA | Complete |
| [DiscoverOrg](/handbook/business-ops/tech-stack/#discoverorg) |  Lead Generation | Marketing, Business Development, Commercial Sales Depts | Beth Peterson | SWA  | Complete |
| [Disqus](/handbook/business-ops/tech-stack/#disqus) | Social Media  | Available by request | Sarah Daily  | SWA  | Complete |
| DocuSign | Document Signing | PeopleOps Shared Account | Brittany Rohde | SWA (Shared Account) | Complete  |
| Dribbble | UX Prototyping | UX Department | TBA | SWA | Complete |
| [Drift](/handbook/business-ops/tech-stack/#drift) | Conversations  | Marketing | Beth Peterson  | SAML  | Incomplete, pending SAML Config |
| [Eventbrite](/handbook/business-ops/tech-stack/#eventbrite) | Event Management  | Field Marketing, Marketing Dept | TBA | SWA  | Complete |
| [HackerOne](/handbook/business-ops/tech-stack/#hackerone) | Bug Bounty  | Security Dept | Robert Mitchell  | SAML  | Complete |
| IIPay | Payroll Portal | Available by Request | Robert Mitchell | SWA | Complete |
| JetBrains | Development Toolkit | PeopleOps Shared Account | Brittany Rohde | SWA (Shared Account) | Complete  |
| [LinkedIn Sales Navigator](/handbook/business-ops/tech-stack/#linkedin-sales-navigator) |   | Business Development Dept | Nichole LaRue  | SWA  | Complete |
| Litmus | Email Marketing | PeopleOps Shared Account | Brittany Rohde | SWA (Shared Account) | Complete pending shared account setup |
| log.gprd.gitlab.net | Monitoring of GitLab.com | TBA | TBA | SAML | Incomplete, Owner needed |
| Lumity | US Health Benefits | US-based Team Members | Brittany Rohde | SWA | Complete |
| [MailGun](/handbook/business-ops/tech-stack/#mailgun) | Outbound Mail | TBA - Available to Request | Robert Mitchell | SWA  | Incomplete, need owner and group assignments |
| [Marketo](/handbook/business-ops/tech-stack/#marketo) |  Marketing Automation | Marketing OPS  Dept| Dara Warde  | SWA/SAML  | Complete for SWA. Needs SAML Conversion |
| Mercer | TBA | PeopleOps Shared Account | Brittany Rohde | SWA (Shared Account) | Complete  |
| Meetup | UserGroup meetings | Marketing Shared Account | Dara Warde | SWA (Shared Account) | Complete |
| MicroSoft Azure | Cloud Platform | Infrastructure, SRE | David Smith | SWA/SAML | In Progress |
| Modern Health | US Health Benefits | US-Based team members | Brittany Rohde | SWA | Complete |
| [Moz Pro](/handbook/business-ops/tech-stack/#moz-pro) |  SEO | Marketing Ops Dept | Sarah Daily  | SWA  | Complete  |
| [Netsuite](/handbook/business-ops/tech-stack/#netsuite) | Finances | TBA | Kim Stathiem | SAML  | Incomplete - needs Setup  |
| Ops.gitlab.net | GitLab Operations Instance | TBA | TBA | SAML | Incomplete, owner needed  |
| [Outreach.io](/handbook/business-ops/tech-stack/#outreachio) | Sales Engagement  | Commercial Sales, Enterprise Sales, Marketing | Nichole LaRue | SWA  | Complete |
| [OwnBackup](/handbook/business-ops/tech-stack/#ownbackup) | SFDC Backup | Business Operations | Jack Brennan  | SWA | Complete, but need to confirm configuration |
| [PagerDuty](/handbook/business-ops/tech-stack/#pagerduty) | Incident Response | InfraStructure, Customer Support, Security Departments  |  TBA | SAML | Incomplete, Need to identify Owner  |
| Reddit | Community | Security Shared Account | Security | SWA (Shared Account) | Complete |
| [Salesforce](/handbook/business-ops/tech-stack/#salesforce) | Sales CRM | Enterprise Sales, Commercial Sales, Marketing Dept | Jack Brennan | SWA/SAML/Custom  | SAML Complete. Provisioning setup in progress |
| [Sertifi](/handbook/business-ops/tech-stack/#sertifi) | eSign Application  | Everyone (Available by Request) | Alex Tkach?  | SWA | Complete  |
| [Snowflake](/handbook/business-ops/tech-stack/#snowflake) | Data WareHouse  | Data Team (Available by Request) | Taylor Murphy | SAML | Complete  |
| [Sprout Social](/handbook/business-ops/tech-stack/#sprout-social) | Social  | Marketing OPS | Sarah Daily  | SWA/SAML | Complete for SWA. Needs SAML Conversion |
| [staging.gitlab.com](/handbook/business-ops/tech-stack/#staginggitlabcom) | GitLab Staging Site  | Infrastructure, Development Dept (Everyone can Request)  | Robert Mitchell  | SAML  | Complete  |
| Status.io | Status Monitoring | Infrastructure Dept | TBA | SWA | Complete. Need to confirm owner and groups |
| [Survey Monkey ](/handbook/business-ops/tech-stack/#survey-monkey) |  Surveys | Marketing and UX Dept | Dara Warde | SWA  | Complete |
| TheHive | Security Operations | Security Ops team (Available on request) | Jayson Salazar | SWA | Complete |
| Tenable.io | Vulnerability Management | Security Dept | Anthony Carella | SAML | Incomplete, needs reconfiguration |
| [Tweetdeck](/handbook/business-ops/tech-stack/#tweetdeck) |  Twitter Management | Available on Request | Sarah Daily | SWA | Complete. Are there Shared Logins that we need to register? |
| [UsabilityHub](/handbook/business-ops/tech-stack/#usabilityhub) | UX Testing  | UX Department  | Sarah O’Donnell | SWA  | Complete  |
| [Xactly](/handbook/business-ops/tech-stack/#xactly) |  Sales Performance Managemetn | Finance Team | Matt Benzaquen  | SWA/SAML  | Complete for SWA. SAML Conversion and Entitlements need verification  |
| [YouTube](/handbook/business-ops/tech-stack/#youtube) |  Videos | Everyone can Request | Sarah Daily  | SWA | Complete. Any shared logins?  |
| [Zapier](/handbook/business-ops/tech-stack/#zapier) | Automations  | Everyone can Request  | TBA | SWA   | Complete. Need owner details.  |
| [Zendesk](/handbook/business-ops/tech-stack/#zendesk) | HelpDesk  | Customer Support and Security Dept. Everyone can request Light Agent | Nabeel Bilgrami  | SAML  | Complete. |
| [Zendesk-Federal ](/handbook/business-ops/tech-stack/#zendesk) | HelpDesk for Federal Customers  | US-Based Customer Support and Security Dept. | Jason Colyer  | SAML  | Complete. |
| [Zuora](/handbook/business-ops/tech-stack/#zuora) | Sales order processing | Sales Operations? | Wilson Lau  | SAML  | Incomplete, needs SAML Configuration  |
