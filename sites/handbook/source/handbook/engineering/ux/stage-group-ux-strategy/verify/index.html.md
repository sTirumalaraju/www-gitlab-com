---
layout: handbook-page-toc
title: "Verify UX Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The [Verify stage](/stages-devops-lifecycle/verify/) includes all features that user automated testing and reporting to help keep strict quality standards for production code. It includes the foundation that allows delivery teams to fully embrace continuous development that automates builds, integration, and verification of their code while accelerating their team's velocity. 
Testing ranges from unit testing, load testing, all the way to accessibility and usability testing.

The Verify UX team's goal is to streamline, expand, and enable increasingly complex configurations by providing the best management experience when setting up, configuring, or troubleshooting pipelines. Our design mission is to bring to the forefront simple ways to make GitLab the tool of choice for continuous development users.

Our biggest partners are the stages under the CI/CD section ([Release](/direction/ops/#release) & [Package](/direction/ops/#package)), Dev ([Create](/direction/create)), and Ops ([Configure](/direction/configure)).

### UX DRIs

- Continuous Integration (CI) UX [DRI](/handbook/people-group/directly-responsible-individuals/): [Dimitrie Hoekstra](https://gitlab.com/dimitrieh)
- Templates UX [DRI](/handbook/people-group/directly-responsible-individuals/): [Dimitrie Hoekstra](https://gitlab.com/dimitrieh)
- Testing UX [DRI](/handbook/people-group/directly-responsible-individuals/): [Juan J. Ramirez](https://gitlab.com/jj-ramirez)
- Runner UX [DRI](/handbook/people-group/directly-responsible-individuals/): [Juan J. Ramirez](https://gitlab.com/jj-ramirez)

### Shared UX

We divided the Verify stage into dedicated experience groups to align with a similar [split](/handbook/product/product-categories/#verify-stage) undertaken by our engineering and PM counterparts. To deliver seamless user experiences, we share responsibility in the overall vision, goals, and research initiatives related to overlapping features. The UX teams work closely together and have shared coverage in the following areas:

- `gitlab-ci.yml`
- Pipelines
- Environments
- Merge requests
- Project settings
- User settings
- Runner
- Audit logs

## Our users

We have different user types we consider in our experience design effort. Even when a user has the same title, their responsibilities may vary by organization size, department, organization structure, and role. Here are some of the people we are serving:

- [Software Developer](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
- [Development Tech Lead](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead) 
- [DevOps Engineer](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
- QA

## Our customers

### Customer: Continuous Integration (CI)

Coming soon.

### Customer: Testing

Coming soon.

### Customer: Runner

Coming soon.

### Related pages

- [Product marketing CI usecase](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/)

## Our UX strategy

We are committed to staying aligned on shared UX with the engineering groups as much as possible, and being the conversation drivers with product managers and other counterparts.

The Verify UX team is working together to uncover customers' core needs, what our users’ workflows look like, and defining how we can make tasks easier. Our strategy involves the following actions:

| Strategy | Cadence |
| ------ | ------ | 
| Jobs to be done framework | Quarterly |
| [UX Scorecards and recommendations](/handbook/engineering/ux/ux-scorecards/) | Quarterly |
| [Opportunity canvas](https://about.gitlab.com/handbook/product-development-flow/#opportunity-canvas) | Ad hoc | 
| Stakeholder interviews | Ad hoc | 
| User and customer interviews | Ad hoc |

Visit [CI/CD UX](/handbook/engineering/ux/ci-cd/) to read about the department strategy. 

### Other initiatives we value

- **Think Big sessions**: Every two weeks we brainstorm as a technical, product, and design team about our vision, roadmap, and all other components involved in creating a great experience for our GitLab customers. We aim to align on medium and long term goals.
- **UX Vision**: Clarify and establish a shared understanding of our user experience foundations together with Product Managers.
- **Perform heuristic evaluation on competitors.**
- **Improve the overall user experience**: Partner up with other teams/individuals responsible for improving our product's UI, and bringing back knowledge to Pajamas and gitlab-ui.
- **Share and learn**: Stay up to date with other design teams to learn from their experience.

### Our UX Scorecards 

#### Primary Jobs to be done ([JTBD](https://about.gitlab.com/handbook/engineering/ux/ux-resources/#jobs-to-be-done))

| JTBD | Description | Walkthrough | Recent recommendation | Rescoring |
| ------ | ------ | ------ | ------ | ------ |
| **Setting up GitLab CI** | When I have created my project, I want to set up automated testing inside GitLab, so I can assure every commit before merging.  | [View issue](https://gitlab.com/gitlab-org/gitlab-design/issues/480) | [View issue](https://gitlab.com/gitlab-org/gitlab-design/issues/518) & [view research insights epic](https://gitlab.com/groups/gitlab-org/-/epics/2227) | [View issue](https://gitlab.com/gitlab-org/ux-research/issues/330) |
| **Runner Concept Discovery / Understanding** | I need to quickly understand the role of runner in their projects CI/CD, how it works, how to configure it and its underlying features. I should be able to setup builds and other Runner specific setups with ease. | [View issue](https://gitlab.com/gitlab-org/gitlab-design/issues/600) | [View issue](https://gitlab.com/gitlab-org/gitlab-design/issues/664) |  |

## Verify UX Team

- [Dimitrie Hoekstra](https://gitlab.com/dimitrieh) - Product Designer
- [Juan J. Ramirez](https://gitlab.com/jj-ramirez) - Senior Product Designer

### Stable counterparts

The following members of other functional teams are our stable counterparts:

- [Nadia Udalova](https://gitlab.com/nudalova) - UX Manager 
- [Lorie Whitaker](https://gitlab.com/loriewhitaker) - Senior UX Researcher
- [Marcel Amirault](https://gitlab.com/marcel.amirault) - [Technical Writer](https://about.gitlab.com/job-families/engineering/technical-writer/), Verify Stage

### Our team meetings

Coming soon

### Follow our work

Our youtube channels for [Verify (CI)](https://www.youtube.com/watch?v=uf1C_95DbN4&list=PL05JrBw4t0KpsVi6PG4PvDaVM8lKmB6lV) and the [Verify Group](https://www.youtube.com/watch?v=yycDyDs0q2I&list=PL05JrBw4t0KrogQIIIezigwB8aUJzsrPh) include UX scorecard walkthroughs, UX reviews, group feedback sessions, team meetings, and more.
