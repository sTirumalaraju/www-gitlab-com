---
layout: handbook-page-toc
title: "Emilie Schario's README"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

# Emilie Schario
{:.no_toc}

**Internal Strategy Consultant, Data**

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, 
and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Guiding principles
* [I will create opportunity for myself and others](https://ventureforamerica.org/become-a-fellow/is-vfa-for-me/). 
* Life is easier when we play like we're on the same team. 
* If you walk by a straw wrapper on the floor, pick it up. 

You don't have to solve all the problems of the universe. 
Just the one sitting right in front of you. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/T0NawB8j2q0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## My time at GitLab

I joined GitLab in June 2018.
I was hired as GitLab's first Data Analyst into the Data Team by my then-manager [Taylor Murphy](/company/team/#tayloramurphy). 
When I joined GitLab I wanted three things in a role: 1. to be a first-class team member in an all-remote organization, 2. to be part of a team, and 3. the opportunity to grow with GitLab. 
I had experienced hybrid-remote teams and knew how difficult it was to be the exception-to-the-in-office-rule.
I've spent a lot of time as the solo-data person, and my knowledge was a bit cobbled together. 
I wanted to be part of team to have people to learn from and develop ideas with. 
And, I wasn't looking for role for the next year to eighteen months. 
I was looking for where I was going to be for a long-term growth opportunity.
And, wow, has GitLab checked all my boxes!

I spent 10 months as a Data Analyst (what some other people would call an [Analytics Engineer](https://blog.getdbt.com/what-is-an-analytics-engineer/)) with over six months as the *sole* data analyst, as the company grew from under 300 to over 600.
In May 2019, I moved into a Data Engineering role, ready for my next challenge as the team and my technical chops grew. 

<embed width="100%" height="400px" src="<%= signed_periscope_url(chart: 8346850, dashboard: 551051, embed: 'v2') %>">

In September 2019, I moved into the Chief of Staff Team as an Internal Strategy Consultant, Data. 
If you had asked me in June if I would ever have moved into a Strategy role, or really anywhere outside of the data team, I probably would have laughed. 
I mean, I spent my own money buying `gitlabdata.com`. 
I have had big visions for the GitLab Data Team since before I accepted my offer that spring day in 2018. 
But right now, it's an incredible challenge, and I'm really enjoying what I do. 
And, there's still no better feeling than having a question and being able to answer it myself! 

## Working with me

### Default to asynchronous  
{:.no_toc}

If you ask me for a meeting, I will push you to see if it can be done asynchronously. 

### Never urgent
{:.no_toc}

I will never act on something as if it's urgent, unless you explicitly say it is. 

## Bookmarks Bar

Optimizing the bookmarks bar in my Browser has really made my life much easier. I have never used the Bookmarks Bar before (and still don't on my personal laptop). From left to right, here's what lives on my Bookmarks Bar:

* Group Conversation Agenda
* My 1:1 with my Manager
* CoST issue board
* Primary project issue board
* Create New Issue in Data Project
* Data page in the handbook
* MRs assigned to me
* MRs authored by me but not assigned to me, so I can follow up with the assignees
* Create New Issue in the CoS Project
* KPI page in the handbook
* CoS Team page in the handbook
* Time tracking doc
* (Changes based on what I'm working on)

## Technical Workflow

I wrote [the data team's onboarding script](https://gitlab.com/gitlab-data/analytics/blob/master/admin/onboarding_script.sh) based mostly on my own workflow preferences. 

* I use iTerm2 as a terminal client with tmux 
* I use atom as my text editor, but I'm working to transition to vim
* Since I don't use my work email for email, I use it as my GitLab notification manager. I only clear out my GitLab todos once per week.

## Work and Communication style

Even though they're descriptive not prescriptive, these sort of communication assessments make it easier to communicate with other folks about how I generally approach situations or interactions.
These assessments do not give people excuses to be jerks.

#### DISC Assessment
{:.no_toc}

Here is my DISC Assessment from July 2019:
[DISC Profile for July 2019](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/disc_profile_2019_july.pdf)

Here is my DISC Assessment from June 2015 (to see the very little change):
[DISC Profile for June 2015](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/disc_profile_2015_june.pdf) 

#### Strengths Finder
{:.no_toc}

Here is my Strengths Finder Report from December 2016:
[Strengths Finder Report from December 2016](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/Gallup_Strengths_Finder.pdf) 
Additional details can be found in my [Insight Report](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/Gallup Insight Report.pdf) and [Insight and Action Plan](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/Gallup Action Plan.pdf).

#### SOCIAL STYLES
{:.no_toc}

We have resources at GitLab on [SOCIAL STYLES](/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/).
I am a Driver. 
Here are my results. 

![Emilie's SOCIAL STYLES](/handbook/ceo/chief-of-staff-team/readmes/emilie/emilie_SOCIAL_STYLE.png)

#### Gretchen Rubin's Four Tendencies
{:.no_toc}

I am an Obliger. Gretchen Rubin outlines [The Four Tendencies](https://gretchenrubin.com/2013/10/what-kind-of-person-are-you-the-four-rubin-tendencies/).
She also speaks about [Obligers](https://youtu.be/CUU99WhRu5Q) on YouTube.

#### Enneagram
{:.no_toc}

I am a Type 3, Wing 4.
This type is often called The Performer.
Read more about [Type 3](https://www.enneagraminstitute.com/type-3/) from the Enneagram Institute.

#### Myers-Briggs
{:.no_toc}

I am an ENTJ. 
This type is often called The Commander.
Read more about [ENTJs](https://www.16personalities.com/entj-personality).

## Areas of Focus
While the list of *areas to improve* is long- infinite, even- I thought I'd take a second to highlight three weaknesses that have the most negative impact on my work.
I've tried to internalize the GitLab Iteration value, especially this line from [Focusing on Improvement](https://about.gitlab.com/handbook/values/#focus-on-improvement), "We believe great companies sound negative because they focus on what they can improve, not on what is working."
I believe the same can be true at the individual level.

Here are some feedback pieces I've received while at GitLab:
   * [Feedback Doc from January 2019](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/2019 Recommendations for Emilie - Google Docs.pdf): This came from my manager when I had been at GitLab for about 6 months.
   * [360 Feedback from January 2019](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/culture_amp_360_jan_2019.pdf): This came through CultureAmp when GitLab had an organization-wide 360.
   * [Feedback Doc from May 2019](https://gitlab.com/emilie/emilie.gitlab.io/-/blob/master/static/pdfs/Emilie Schario Handoff Doc - Google Docs.pdf): This came in the context of a manager handoff. I had been at GitLab for 11 months.
   * [My own reflection on 1 Year at GitLab in June 2019](http://shedoesdatathings.com/post/1-year-at-gitlab/)

## Related pages
* [My WFH Set up](http://blog.emilieschario.com/post/another-article-on-working-from-home-in-the-era-of-covid-19/)
* [How to Run Remote Data Teams](https://builtin.com/remote-work/remote-data-teams)

### Blog Posts I've Written For GitLab
Or talks
* [Deploying your first dbt project with GitLab CI](https://www.youtube.com/watch?v=-XBIIY2pFpc&feature=youtu.be&t=1305)
* [DataOps in a Cloud Native World](https://www.youtube.com/watch?v=PLe9sovhtGA&list=PLFGfElNsQthaaqEAb6ceZvYnZgzSM50Kg&index=9&t=0s)
* [The Three Levels of Data Analysis- A Framework for Assessing Data Organization Maturity](/blog/2019/11/04/three-levels-data-analysis/)
* [How to Implement DataOps using GitLab](https://www.youtube.com/watch?v=GSEwkL5ZRNs)
* [What I Learned about the CEO's Job from Participating in GitLab's CEO Shadow Program](/blog/2019/10/07/what-i-learned-about-our-ceo-s-job-from-participating-in-the-ceo-shadow-program/)
* [How to manage your Snowflake spend with Periscope and dbt](/blog/2019/08/26/managing-your-snowflake-spend-with-periscope-and-dbt/)
* [Snowflake Spend dbt Package Release 0.1.1](/blog/2020/04/08/snowflake-spend-dbt-package-release/)
* [How to run an all-remote board meeting](/blog/2020/04/15/remote-board-meeting/)

#### For other groups
{:.no_toc}

* [Webinar: How to Align and Inspire a Remote Team](https://www.youtube.com/watch?v=N-KkTIPA_4E)

## Bio for speaking events 

Emilie Schario has significant experience scaling Data & Analytics teams without scaling their headcount, while being responsive to the hypergrowth of the business. 
As GitLab’s first Data Analyst, then Data Engineer, Analytics, she oversaw 4x growth in the Data function. 
Today, she leverages her data skills in a Strategy role supporting the entire business. 
She is a contributor to many open source projects including dbt, Meltano, and GitLab. 
When not at her day job, Emilie can be found in her local CrossFit box or volunteering with Operation Code, codebar, TechSAV, or MilSpouse Coders. 
An Army wife, Emilie lives in Savannah, GA with her husband Casey. 
Emilie is a Princeton University and Venture for America Alumna.

