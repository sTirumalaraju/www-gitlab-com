---
layout: handbook-page-toc
title: "Commercial Sales Enablement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Commercial Sales Enablement Handbook Page

### Process for Capturing Customer Requests You've Never Heard Before

With all the uncertainty around Covid-19, we have developed a process to capture never-before-heard customer requests. Please use this [document](https://docs.google.com/document/d/1pWNKq-cyH5uUIGvueyAQea-n4zk6A21ghoAJvOV5uZQ/edit?usp=sharing) to help you understand the process.

### Opp Management - Managers check this

**Fields that need to be updated at all times**
1. Close date
2. $ value (by a t-shirt size). “T-shirt sizing” an opportunity is a way of estimating how much a prospect should be able to spend on GitLab for that transaction (new, add-on, or renewal). It’s a way         of shorthanding the size or complexity of the opportunity and is meant as a gut-check based on both data and your experience working with similar opportunities. 

      - Each opportunity created should have a dollar amount in the Incremental ACV field greater than $0 to ensure visibility into pipeline and for forecasting purposes. 

      - Similar to account ranking, the advantage of sizing each opportunity is that it helps you prioritize and allocate the right resources to optimize your iACV/hour. For      example, if an opportunity is t-shirt sized larger, you might reach out on a daily basis to schedule the first meeting whereas your communication cadence might be every         other day for a smaller sized opportunity. T-shirt sizing also gives you a sense of what resources (technical, leadership, etc.) might be needed to move the            opportunity through the pipeline and increase your win rates. 

      - After reading SDR notes, the two key places to look at firmographic information are LinkedIn and Crunchbase. Data points to consider include where the company is located, employee size, as well  who the contact is and at what level of the org they live. As an example, you may asses an opportunity in which there are more employees as a larger t shirt size.  Remember, this is only an estimate - opportunity amounts can be refined after deeper discovery. 

3. Opportunity name (needs to have tier and user count)
4. Account tier and tier notes
5. Primary contact
6. Next steps
7. Next Steps Date
8. Stage
9. Any activity that’s logged while working the opportunity needs to be attached to the **contact** and the **opportunity**
   * Best Practice is to log an activity in ‘activity history’
   * For Call activity types, associate the record with the correct contact(s) list and as needed, include **(ii)** in the record subject to call attention to information inside
10. Command Plan

**This is what you’re going to learn in this course**
1. Here are all the reasons why ppl say they can’t fill in the Command Plan
   * I haven’t talked to them yet
   * They didn’t tell me
   * I don’t know anything about the account/company
   * I’m going to learn that on the next call
2. The above are **not** acceptable reasons not to fill in the required fields listed above.

**How to get each of those required fields**
1. Research online
2. Write the discussion questions you’re going to ask to obtain the info
3. Qualification Indicators (101)
   * How to get these - Qual 201
4. Other SFDC field requirements
5. Recap on the objections
   * Review the list and reiterate that they’re not acceptable reasons to not fill out those fields


### Tiering Accounts

* [Commercial Sales: Tiering Accounts Training Clippet](https://youtu.be/M-5OhlYxmFI)

For more details visit the [Account Tiering](/handbook/sales/commercial/#account-tiering) section on the Commercial Sales Handbook page.


### Getting into Accounts That Say They Don't Have a Problem

**In Concept**
1. Each of us sales professionals makes a regular decision: where we apply our time. We are trying to optimize the ratio of output to time. Everything we do or work on should be judged by this ratio.
2. Each of us has a market to go after: note that most of our volumes of contacts to connect with is roughly similar - you either have:
   * Lots of accounts with less contacts
   * Or few accounts with many, many more contacts
3. We in MM and SMB use the Account Tiering fields to effectively show ourselves where we think there is a higher or lower level of this ratio.

**Each person writes out a process:**
1. First: come up with a t-shirt size for each account overall, and how far on the journey they are
2. Find all easily attainable contacts and put in SFDC
3. Prioritize in order of most likely to speak/reply
   * Bucket into 3 groups: know nothing, know good info, and not sure value of what i know
4. Plan the method and type of content for reach out for each group/bucket
5. ID your ideal client: personally I want some form of intro…
   * A slow valuable intro is better than most fast, cold connects
6. Get a 10-15 min connect in whatever way they prefer:
   * Nice to meet you - how’s it going?
   * Give your credibility statement.
   * Ask them if they think there is anyway that GitLab could help.
   * When they say no, be sad. Then ask what challenges their team or they have.
   * When they say yes, listen up and begin discovery, then bounce back to qualification to understand if its worth it for them to work on it.
7. Document all notes and immediately build the most compelling customized deck on why they should try GitLab
   * See where the biggest gaps are in the deck, and if you aren’t sure, role play with a colleague on it to see what is missing
8. ID EXACTLY the info missing to make it somewhat compelling
9. Go back to your list of contacts and find out who could best get you this info

**The tougher, non-intro’d scenario:**
1. Dig in on the persona of the contact info you have so you can reach out in a way that you have 100% confidence that this connect will result in 2-way communication
   * This can be showing up at an event
   * It can be somewhat person based
   * It should never be creepy
   * Think about how to give first
2. When coming up with a strategy, think about the specific reason that a person inside that company has NOT offered their contact into to GitLab, and figure out how to get over that single hump

**Make sure that you document all:**
1. Notes
2. Ideas
3. Internal strategy

Be sure to put in the status of this account in Account Tiering and Tiering Notes so that you can be efficient every time you or another person works on it

**When is it Game Over?**
1. It is game over when working on an account reveals information that moves it below another account you are working that surpasses it in terms of value.
2. Value = Worth (what you will get)/Cost (what you need to put in) x Risk (the chances you predict that your understanding of value and worth are accurate.
3. Value (in this specific effort) = how much the account is worth this FY/the time you need to get them to spend that amount


### Sales Note Taking
GitLab is an organization that is built on contribution and collaboration. These values are not only critical for building a great product, they are critical for building a world class sales team. Each member of the Commercial team is expected to create insider notes in SFDC on accounts and conversations they participate in or currently own. With GitLab being a remote company, it is even more important to create and maintain a culture of great note taking.

#### Why Do We Take Notes on Accounts?
1. Capture insight and learning
   * Notes draw out key insights that help you better understand GitLab’s potential and existing customers. As you plan, organize and grow your territory, the discipline of note taking empowers you to scale your efforts, clarify needs, next steps and signals in ways your memory cannot.
   * Note taking shares knowledge and guidance that the whole sales organization (from sales reps to leadership) can benefit from. Why are customers buying GitLab? What are their common challenges? Are we seeing interest from new groups of buyers? Notes on Salesforce are the corporate memory of the commercial team, if there are no notes, it increases the probability of wasted meetings.

2. Become better sales leaders
   * Through note taking you position yourself to be great at your job, which is great for our potential and existing customers. Notes create a framework for how you can best help your customer. What is your next step following your call? What did the customer really want from GitLab Premium? Just check your notes.

3. Capture A Story 
   * Every conversation, opportunity or interaction with a potential or existing customer forms part of a story. Stories have chapters, a beginning, a middle and an end. Within the commercial team, stories provide resources and context for training and iteration, a key component on our journey to world class achievement.

#### Different Sections of Notes to Include

**Raw Notes Section**

   * Sales call notes could be word-for-word transcripts or much less structured. They must include customer specific language and should always detail technologies and challenges in the customer's exact words (in the case of Kubernetes, spelling matters for future search). This portion is called raw notes. For example, who did you perceive to be the decision makers (DM’s) and influencers (INF) in the meeting and why? What were the key points raised or asked? How did they feel? Were you unclear on certain things? What questions did you not know to ask at the time and what would you ask if you could do it again? These type of notes are primarily for you to recall noteworthy moments from the meeting that you can build upon. 

**Questions Section**

*   This section of notes reflects questions you have following your meeting. Do you need to find more decision makers? Need to think of a plan to win the renewal or new business? Jot down a few questions - that should be sufficient. Questions play the role of helping you recall the gaps you find from the meeting.

**Summary Section** 

*   These type of notes are not just for you. Rather, they benefit the entire sales team. Summaries focus on important, actionable and insightful points - things that will help you in you in your next meeting. Try and convey timelines where relevant, include dates, times of day, etc. Did you say you would follow up in the morning with a response before sending a quote? That’s a great note to include. Lastly, when it comes to summaries, bullet points are our friends. 

**Next Steps Section** 

*   Just as it sounds, "Next Steps" outline strategic actionable items that need to be acted upon by yourself, someone else in the team or a customer. Where possible, tag internal colleagues on tasks that relate to them and follow up with customers via email on tasks that relate to them. Next steps are great for handovers and business continuity, remember the story you’re compiling? think of next steps as bookmarks - people can pick up the story where you leave them. 

#### Notes Subject Line

*   YOUR NAME/YOUR MANAGER NAME Internal (ii) WHAT YOUR NOTES ARE FROM
*   Anthony/Ryan internal (ii) listened to chorus call OR TITLE

#### Where to Make Notes

Login to [Salesforce](https://gitlab.my.salesforce.com/00T4M00002KWYXx?srPos=24&srKp=00T) to see where you should add notes. If no opportunity exists, create a note on the Contact level, if an Opportunity exists, create a note on the Opportunity level. 

#### Characteristics of Next Steps

1. **A colleague with a similar skill set should be able to pick up your notes, get an understanding of what’s happened and take action quickly (within 3-5 minutes).**

2. They should be compelling for the future version of you who will read them. If, when you read the next steps, you aren’t motivated to take action at the exact time and day that you prescribe, you should iterate on them. Compelling next steps are specific, concise, actionable and efficient. Please think about our [values](https://about.gitlab.com/handbook/values/#hierarchy) of transparency, collaboration, efficiency and results.

3. Next Steps and actions, once completed, will add value to the customer’s GitLab experience and journey. They are impactful.

**Example:** Vanilla Notes vs Great Notes

*   A Vanilla Note: “Follow up with Anthony next week.” 

*   Great Note: “Send an email to Anthony (our #1 champion at XYZ Corp) next Tuesday morning at 10 his time to confirm he got sign off from Ryan on Premium: ‘Anthony, did you have a chance to present the ROI slide we created? I’d love to hear feedback on those numbers…’” [RO note: the statement in single quotes should actually be written in your next steps so that you could cut and paste it into and email verbatim.]

**This Sales Team takes great notes!**

