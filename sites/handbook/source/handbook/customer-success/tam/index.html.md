---
layout: handbook-page-toc
title: "Technical Account Management Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Handbook directory

### TAM responsibilities

- [Using Gainsight](/handbook/customer-success/tam/gainsight/)
- [TAM Responsibilities and Services](/handbook/customer-success/tam/services/)
- [TAM and Product Interaction](/handbook/customer-success/tam/product/)
- [TAM and Support Interaction](/handbook/customer-success/tam/support/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [Account Handoff](/handbook/customer-success/tam/account-handoff/)
- [Capturing Customer Interest in GitLab Issues](/handbook/customer-success/tam/customer-issue-interest/)
- [Usage Ping Playbook](/handbook/customer-success/tam/usage-ping-faq/)
- [TAM READMEs](/handbook/customer-success/tam/readmes/) (Optional)

### Customer journey

- [Account Engagement](/handbook/customer-success/tam/engagement/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Digital Journey](/handbook/customer-success/tam/digital-journey/)
- [Cadence Calls](/handbook/customer-success/tam/cadence-calls/)
- [Success Plans](/handbook/customer-success/tam/success-plans/)
- [Playbooks](/handbook/customer-success/playbooks/)
- [Stage Adoption](/handbook/customer-success/tam/stage-adoption/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/tam/ebr/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/)
- [Customer Health Assessment and Triage](/handbook/customer-success/tam/health-score-triage/)

### TAM managers

- [TAM Manager Processes](/handbook/customer-success/tam/tam-manager/)

---

## What is a Technical Account Manager (TAM)?

GitLab's Technical Account Managers serve as trusted advisors to GitLab customers. They offer guidance, planning and oversight during the technical deployment and implementation process. They fill a unique space in the overall service lifecycle and customer journey and actively bind together sales, solution architects, customer stakeholders, product management, Professional Services Engineers and support.

See the [Technical Account Manager role description](/job-families/sales/technical-account-manager/) for further information.

### Advocacy

A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product. In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base.

### Values

#### Management

Technical Account Managers maintain the relationships between the customers and GitLab. Making sure that everyone is working towards pre-defined goals and objectives.

#### Growth

Technical Account Managers help to bring GitLab to all aspects of your company, not just software development. They can do this by showing other business unit's how to use GitLab for their day-to-day tasks and to advocate for new features and functionality that are in demand by other groups.

#### Success

Technical Account Managers make sure that the adoption of GitLab is successful at your company through planning, implementation, adoption, training and regular healthchecks.

### Responsibilities and Services

[Customers who meet certain criteria are aligned with a Technical Account Manager](/handbook/customer-success/tam/services/#responsibilities-and-services), which differs between Commercial and Enterprise.

There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilisation of GitLab's products and services. These services include, but are not limited to:

#### Book of Business
Technical Account Managers are responsible for managing a portfolio of customer accounts based on the sum of the annual contract value of the accounts under management. The current targets per TAM based on segment are:
- Large / Enterprise: $3.25M/TAM
- Commercial: $4M/TAM
- SMB: N/A as it's digitally-led

The team is working on efficiency initiatives to increase the contracts under management per TAM, targeting $5M for FY21.

#### Relationship Management

- Regular [cadence calls](/handbook/customer-success/tam/cadence-calls)
- Regular open issue reviews and issue escalations
- Account [health checks](/handbook/customer-success/tam/health-score-triage/)
- [Executive business reviews](/handbook/customer-success/tam/ebr)
- Success strategy roadmaps - beginning with a 30/60/90 day success plan
- To act as a key point of contact for guidance, advice and as a liaison between the customer and other GitLab teams
- Own, manage, and deliver the customer onboarding experience
- Help GitLab's customers realize the value of their investment in GitLab
- GitLab Days

#### Training

- Identification of pain points and training required
- Coordination of demo sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
- "Brown Bag" sessions
- Regular communication and updates on GitLab features
- Product and feature guidance - new feature presentations

#### Support

- Upgrade planning
- User adoption strategy
- Migration strategy and planning
- Launch support
- Monitors support tickets and ensures that the customer receives the appropriate support levels
- Support ticket escalations

## Related pages

- [Dogfooding in Customer Success](/handbook/customer-success/#dogfooding/)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation/)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions/)
- [Commercial Sales Customer Success](/handbook/customer-success/comm-sales/)
- [Customer Success' FAQ](/handbook/customer-success/faq/)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [GitLab Positioning](/handbook/positioning-faq/)
- [Product Stages and the POCs for each](/handbook/product/product-categories/#devops-stages/)
- [How to Provide Feedback to Product](/handbook/product/how-to-engage/#feedback-template/)
- [Sales handbook](/handbook/sales/)
- [Support handbook](/handbook/support/)
