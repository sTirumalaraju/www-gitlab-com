---
layout: handbook-page-toc
title: Manager Challenge
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Manager Challenge page. We are rolling out a 4 week manager challenge in September 2020, "The GitLab Great Managers Program." All details relating to the challenge can be found here on this page. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vST_3shd7g0Y6E46JaCdtpXKHfj6D8TAjF-fgZ4IiZ_1NETN2f8ROjBE6NtOpCSs0YXwWgYq-oHryO9/embed?start=true&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Week 1

Add info for week 1 here 

[embed week 1 slide deck here] 

## Week 2

Add info for week 2 here 

[embed week 2 slide deck here] 

## Week 3

Add info for week 3 here 

[embed week 3 slide deck here] 

## Week 4

Add info for week 4 here 

[embed week 4 slide deck here] 
