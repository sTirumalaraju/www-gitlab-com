---
layout: handbook-page-toc
title: "360 Feedback"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### 360 Feedback

360 Feedback is an opportunity where managers, direct reports, and cross functional team members will give feedback to each other. There will not be ratings associated with the feedback. This is about investing in each other to help our team members succeed and grow, while also receiving valuable feedback for our own development.

**Next 360 Feedback Cycle**

On 2020-05-18 we will launch our next 360 feedback cycle. We understand and recognise the current change in circumstances and the additional pressure Covid-19 is putting on our team members. Therefore we have spread the process over 7 weeks, simplified the reviews and made participation optional but highly encouraged. From the People Group we encourage team members to gather feedback from peers, their manager and team members. 

**Timeline**
- 2020-05-13 - Q&A on the process before kick off
- 2020-05-18 - Survey Launch
- 2020-05-18 to 2020-05-25 - Team member nominates peers, manager, and any direct reports they have. Managers can adjust the peer nominations at this time and whenever the nominations are set the manager or a Culture Amp admin can launch the individual review.
- 2020-05-25 to 2020-06-22* - Team member, managers, and peers complete reviews for themselves and each other. Managers will have visibility into all self and peer reviews for their direct reports as they are completed.
- 2020-06-22 to 2020-07-06 - Manager to review completed 360 and send results to team member. **Please prioritize sending feedback results to your respective direct reports as soon as possible. It is important that they have ample time to review their feedback before it is discussed live (minimum 3 business days).**
- 2020-07-06 to 2020-07-20 - Manager and team member 360 feedback/discussions.
- 2020-07-20 - Closing the cycle
- 2020-07-22 - Feedback Retrospective info [here](https://gitlab.com/gitlab-com/people-group/General/-/issues/617)

*Extended by 2 weeks - other due dates have also been pushed out to align to this extension. 

*Please note: for the purpose of this 360 feedback cycle we have included team members with a hire date of before May 1st 2020 in the automatic email communications. If you are a team member with a hire date of post May 1st 2020, please reach out to the People Specialist team (#PeopleOps) to be manually added.*

**360 via Culture Amp**

We utilize the tool [Culture Amp](https://gitlab.cultureamp.com/) to administer the 360.

Team members who started at GitLab in the last 90 days are welcome to but not expected to participate in the 360 feedback process.  However, you can still use the 360 process to provide feedback around recruiting, onboarding or any other aspect at GitLab. All team member input is valuable.

Managers will be assigned as the coach for their direct reports.  It is important that all managers review and send out the 360 feedback within 48 hours once the survey closes and you are sent the results. 

- If a team member is in the process of migrating to a new role, the current manager and new manager should arrange a successful handover of the feedback, whether sync or async. 
- If your manager changes throughout the 360 process, please contact peopleops@domain.com to make this change on Culture Amp.

This section outlines the process for the 360 cycle using the tool Culture Amp. Culture Amp is a tool that makes it easy to collect, understand and act on team member feedback.  It helps improve engagement, the experience and effectiveness of every team member.  We have recorded a training overview of the 360 process via Culture Amp for your review and created a slide-deck to provide [guidance on 360-Feedback](https://docs.google.com/presentation/d/1YxnAWDO0GPSWX5hDepgwMOohT0ZsGzqmUHxvBpaHf7E/edit#slide=id.g77644d9eff_0_0), kindly review before you give feedback.  

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Dix7lZSadvI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

**Why do we have 360 Reviews?**

Feedback received in a 360 review can be used for organizational growth, team development and your own personal development.  Here are 5 ways that team members can benefit from a 360 review.
- Increased Self Awareness - Gives a team member insight into their behavior and how they are perceived by others.  They have a deeper understanding when you compare your self review with the review of your team members.
- Balanced View - Since feedback is received from team members across GitLab and not just your manager it provides a fair and more accurate picture to the team member.
- Leverages Strengths - A 360 helps you identify your strengths in which you can then build your development plan for continued growth and success.
- Uncovers Blind Spots - This enables a team member to understand behaviors they are exhibiting, but never noticed themselves.  Highlighting these blind spots allows a team member to focus on those overlooked behaviors.
- Development of Skills - A 360 gives a team member a starting point for creating a development plan.  It encourages individual accountability and gives team members control over their own career path.  

A 360 review also ties into our core values of Collaboration, Efficiency and Transparency.  

During this 360 review we will be using the Individual Effectiveness 360 survey.  This survey consists of 13 questions in 4 different focus areas. These focus areas are the following:
 - Strength areas
 - Improvement areas
 - General
 - Manager feedback*

 *Manager feeback will only show up when as a team member you are giving your manager feedback. This section will not be included in feedback to co-workers or direct reports. 

Team members have 7 days to nominate reviewers before the survey launches on 2020-05-25.  Once the survey has launched, team members will be asked to complete their self review and provide feedback on peers within 14 days. Once the feedback section is closed managers will have 14 days to send the results to the team member.  Managers and team members should complete all 360 review conversations within 14 days after the feedback section closes.

**Reviewer Nomination Process:**
In Culture Amp, a reviewer is anyone who is requested to provide feedback for a team member.  Reviewers can be managers, direct reports or co-workers.  Here are some best practices for selecting reviewers:
- Nominate around 7/10 team members including your manager, any direct reports and a selection of peers.
- Search for team members using their email or last name, as CultureAmp does not support listing Preferred Name, only Legal Name and Email.
- Reviewers need to be in a position to provide meaningful feedback, supported by examples, that will help you find a focus.
- Choose people who you have worked closely with for at least 3 months.  An exception is where you may have worked very closely with someone, say on a project, but for a shorter amount of time.
- Select reviewers who will provide you with honest and perhaps even "difficult to hear" feedback.

Remember you want to hear honest feedback so select reviewers you know will provide you meaningful data.  In general, comments should be to the point and include specific examples. Helping your teammates understand the impact of what they are, or are not doing, is an important part of making that feedback actionable. Feedback will be anonymous, however each team member is given a unique email link to provide feedback so please do not share.  In Culture Amp reviewers are listed in 3 categories:
- Manager
- Direct Reports
- Co-worker

Feedback will not be tied back to a specific reviewer, however you only have 1 manager so you know that any feedback in that section came from directly from your manager. If you nominated 10 team members to provide feedback all 10 will be listed however any feedback is not tied directly back to a specific team member.  

**Giving Feedback:**
If you feel overwhelmed by the number of teammates that have requested feedback from you, keep in mind that you are providing your teammates with a gift: the ability to learn and grow from the feedback they receive. However, you may not have feedback related to each of the questions asked. That is ok. If you don't have anything meaningful to provide, you can put not-applicable. Focus on the teammates and the questions for which you have meaningful and helpful feedback.   Here are 5 Tips for Providing Effective Individual Feedback in a 360 Process.
- Prepare - Think about the individual beforehand.  What do you value in them as a co-worker? Where do you think are their biggest opportunities to improve?
- Speak from your own experience - avoid "I've heard..." statements.
- Be specific - provide examples wherever possible and avoid general statements like "really good" or "difficult to work with"
- Keep it actionable- always describe behaviors, not traits. Focus on what the person can actually do something about going forward (i.e. more of, less of, keep doing - "it would be good to see more of X as it leads to Y".)
- Be respectful AND honest - development feedback can at times be challenging to give.  Keep in mind the purpose of the survey is for development and not to judge or evaluate performance.

If you would like to learn more, we held a [Delivering Feedback Live Learning course](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/#delivering-feedback) on 2020-06-08. 

**General Tips:**
* It is ok to skip a question by typing N/A if you don't have meaningful feedback in that area. Don't create a "story" where there is none.
* Try to explain the why? Thinking through the "why" of the feedback will help you provide better and more meaningful feedback.
* Try to avoid comparing different team members and rather look at a person's progress, comparing them against themselves only. 
    * Ex: Person X's performance in January compared to their performance in May, as opposed to person X's performance compared to person Z's performance. 
* Team members have very different backgrounds, personalities, strengths and other elements that dictate their performance. The most important thing is that they are making progress and this should be encouraged.
* Try to complete feedback for 1 or 2 people a day, versus waiting until the last minute. Spending 10-20 minutes a day on this can make thinking through and writing the feedback less overwhelming.
* If you have submitted your requests for feedback in Culture Amp and need to make a change (add or remove a reviewer(s)), please email peopleops@gitlab.com with the requested changes and we will update your profile if still within the deadline where changes can occur.

**Bias**

- At this moment in time we do not have any integration tools in Culture Amp to help us combat bias. As an alternative and to help with this we have 2 Culture Amp training courses. One for [team members](https://www.cultureamptraining.com/participate-in-a-performance-cycle-for-employees/481210) and the other for [managers](https://www.cultureamptraining.com/performance-for-managers/437360). These training courses will combat bias. 

**Managers:**
It is very important to remember that your teammates may be receiving incomplete feedback. GitLab team-members are not being asked to provide comprehensive feedback. We completed the compensation review prior to this 360 feedback cycle to limit concerns around providing feedback being punitive or rewarding. Feedback is valuable for feedback's sake. To learn and grow, or understand yourself a bit better.  The feedback that your team member receives may reinforce excellent or under performance you have already observed, but shouldn't be the only data point you use in evaluating performance.  In cases where you’ve identified your top performer, we should learn from what makes that person successful to share with others. In cases where unsatisfactory performance is identified, you should also address that timely and honestly.  The feedback included through the 360 process may help in talking through examples of strengths or improvement areas, but you should not wait for the 360 Feedback process to address performance matters.

**360 Feedback Result:**
Once the survey closes managers we advise to send out the completed review to their direct reports within 48 hours.  Managers and the team member should schedule time within the next 2 weeks to go over the results and create a development plan.  Giving and receiving feedback can be hard for both the manager and the team members, remember to be open minded and calm.  Be open minded to the fact that others may see something that you do not.  If you disagree with the feedback, others may be seeing something that you are not aware of- we called these blind spots earlier.  Allow for the fact that other may be right, and use that possibility to look within yourself.  Managers, feedback should never be a surprise!  It is meant to guide, mentor, support, enhance and to help the team member grow.  Try and maintain the model that feedback is a gift- it is data.  More data is always better because it provides us with choices we wouldn't otherwise have.  

### 360 Feedback Meeting

This section outlines the conversation for a manager to have with their direct report after the cycle has closed. Peer feedback is not meant to evaluate performance specifically, but instead assist in giving feedback to help the team member grow and develop within their role.

We recorded a training on this subject:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/VK8cA8nYcoY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

* No surprises. Team members should not hear about positive feedback or performance in need of improvement for the first time at the 360 feedback meeting. Team members should have regular [1:1s](/handbook/leadership/1-1/) where this is discussed. However, if new information is uncovered during the 360 Feedback process, you should discuss that new data.
* The overall aim is providing meaningful feedback. Don't allow the feedback meeting (document and conversation) to (d)evolve into a "todo" list.
* Managers should send the results within 48 hours of the survey closing so they can prepare and come to the meeting with questions and discussion points.
* Make sure you (Manager) are also prepared for the discussion, write down some notes and key points you want to make. What are the major themes coming out of the feedback?
* Make time to talk about the future - [career development](/handbook/leadership/1-1/#career-development-discussion-at-the-1-1) by working through the [Compa Ratios](/handbook/total-rewards/compensation/compensation-calculator/#compa-ratio) to progress against those factors.
* Make sure to discuss and document on the team page any [expertises](/company/team/structure/#expert) the team member has obtained.
* If there are areas that need improvement, consider if the use of a [PIP](/handbook/underperformance/) would be helpful.
* This should be a conversation, try to avoid doing all the talking and get feedback from the team member. As a manager, you can help your teammate process and understand the feedback, helping to avoid over/under reactions or defensiveness.  Ask questions such as:
   1. Is there feedback that you received that is surprising or upsetting to you?
   1. After reading your feedback, what are the areas you would like to focus on and how can I help?
   1. How can I be a better manager for you?
   1. What are you hoping to achieve at GitLab this coming year?
* Avoid the [horns and halo effects of recent events](https://www.thebalance.com/effective-performance-review-tips-1918842), and instead make sure to take a step back to review the entire review period.
* Make sure you discuss _positive_ aspects of performance, but avoid using the ["feedback sandwich"](https://www.officevibe.com/blog/employee-feedback-examples) to mask an honest conversation about areas that need improvement
* Follow Up. Did you discuss pathways to career progress, or specific points of attention for improving performance? Make sure you add them to the top of the 1:1 doc so as to remind yourselves to follow up every so often.
* Managers should also share the themes of the feedback they received with their teams.  Making yourself open and vulnerable can help the rest of the team understand that it is ok to get hard feedback and we can grow from it.  It also enlists the team is helping you grow.
* Consider asking each teammate to share the top 2-3 Themes from their feedback, what they plan to do now, and how the team can help.

### Receiving Feedback Well

Be open to engaging in the conversation.  Your peers have taken the time to provide you with their feedback.  And the purpose of this feedback is to help you develop and reach your full potential.  The perception they have of you is important information for you to have and to build into an action plan.

Before going into the conversation and reviewing feedback, check out the page on [receiving feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/#receiving-feedback).

Be accepting of positive feedback.  Instead of deflecting compliments, hear and internalise them.  They are strong indicators of where you have successfully developed your skills.  Remember to maintain a focus on them so you can continue to develop the skill.

During the meeting:
* Breathe!
* Assume positive intent
* Avoid your first response as chances are it may be defensive
* Ask lots of questions to fully understand the feedback
* Ask for time to process the feedback and come back with any follow up questions & / or action
* Say thank you for their time  

If you would like to learn more, we held a [Receiving Feedback Live Learning course](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/#receiving-feedback) on 2020-02-25. 
