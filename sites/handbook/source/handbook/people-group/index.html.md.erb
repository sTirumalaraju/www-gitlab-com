---
layout: handbook-page-toc
title: "People Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Role of the People Group
{: #role-peopleops}

In general, the People Group team and processes are here as a service to the rest of the team; helping make your life easier so that you can focus on your work and contributions to GitLab. On that note, please don't hesitate to [reach out](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group) with questions! In the case of a conflict between the company and a team member, the People Group works "on behalf of" the company.

## Need Help?
{: #reach-peopleops}

### In Case of Emergency

Please email: `peopleops@gitlab.com` with "Urgent" in the subject line (we aim to reply within 24 hours, however usually sooner). The [Senior Manager, People Operations](https://about.gitlab.com/company/team/#Vatalidis) and [Compensation & Benefits Manager](https://about.gitlab.com/company/team/#brittanyr) are also both contactable 24/7 via their mobile numbers that appears in slack. In the event a team member is in an unsafe situation due to natural disaster, please see the [Disaster Recovery Plan](https://about.gitlab.com/handbook/people-group/#disaster-recovery-plan) section on this page.

## Welcome

Welcome to the People Group handbook! You should be able to find answers to most of your questions here. You can also check out [pages related to People Group](/handbook/people-group/#other-pages-related-to-people-operations) in the next section below. If you can't find what you're looking for please do the following:

- [**The People Group**](https://gitlab.com/gitlab-com/people-ops) holds several subprojects to organize the people team; please create an issue in the appropriate subproject or `general` if you're not sure. Please use confidential issues for topics that should only be visible to GitLab team-members. Similarly, if your question can be shared please use a public issue. Tag `@gl-peopleops` or `@gl-hiring` so the appropriate team members can follow up.
  - Please note that not all People Group projects can be shared in an issue due to confidentiality. When we cannot be completely transparent, we will share what we can in the issue description, and explain why.
  - [**Employment Issue Tracker**](https://gitlab.com/gitlab-com/people-group/employment-templates): Only Onboarding, Offboarding and Transition Issue Templates are held in this subproject, and they are created by the People Exp Team only. Interview Training Issues, are held in the [Training project](https://gitlab.com/gitlab-com/people-group/Training) and created by the Recruiting team. Please see the [interviewing page](/handbook/hiring/interviewing/#typical-hiring-timeline) for more info.
- [**Chat channel**](https://gitlab.slack.com/archives/peopleops); Please use the `#peopleops` Slack chat channel for questions that don't seem appropriate for the issue tracker. For access requests regarding Google or Slack groups, please create an issue here: https://gitlab.com/gitlab-com/team-member-epics/access-requests. For questions that relate to Payroll and contractor invoices please direct your question to the `#payroll`, `#expense-reporting-inquiries` and `#finance` channel for Carta. Regarding questions for our recruiting team, including questions relating to access, or anything to do with Greenhouse, referrals, interviewing, or interview training please use the `#recruiting` channel. For more urgent general People group questions, please mention `@peoplegeneral` to get our attention faster.
- If you need to discuss something that is confidential/private, you can send an email to the People Group (see the "Email, Slack, and GitLab Groups and Aliases" Google doc for the alias).
- If you only feel comfortable speaking with one team member, you can ping an individual member of the People Group team, as listed on our [Team page](/company/team/).
- If you wonder who's available and/or in what time zone specific team members of the People Group are, you can easily check it via [this tool](https://timezone.io/team/peopleops). If you want to use this tool for your own team or want to sign up as part of the People Group make sure to sign up [here](https://timezone.io/). When signing up please add your title to the name field, so everyone can see which role you are in.
- If you need help with any technical items, for example, 2FA, please ask in `#it_help`. The channel topic explains how to create an issue. For urgent matters you can mention `@it-ops-team`.

## How to reach the right member of the People Group

This table lists the aliases to use, when you are looking to reach a specific group in the People Group. It will also ensure you get the right attention, from the right team member, faster.

| Subgroup                          | GitLab handle   | Email            |  Slack Group handle | Greenhouse |
|-----------------------------------|-----------------|------------------|-----------------|-----------------|
| [People Business Partners](https://gitlab.com/gitlab-com/people-group/General) | @gl-peoplepartners | peoplepartners@domain | @peoplepartners | n/a |
| [Total Rewards](https://gitlab.com/gitlab-com/people-group/total-rewards) | @gl-total-rewards | total-rewards@domain | n/a | n/a |
| [People Operations Specialists](https://gitlab.com/gitlab-com/people-group/General) | @gl-peopleops  | peopleops@domain | @peopleops_spec | n/a |
| [People Experience Associates](https://gitlab.com/gitlab-com/people-group/employment-templates) | @gl-people-exp  | people-exp@domain | @people_exp | n/a |
| [Diversity, Inclusion and Belonging](https://gitlab.com/gitlab-com/diversity-and-inclusion) | No alias yet, @mention the [Diversity, Inclusion and Belonging Partner](/job-families/people-ops/diversity-inclusion-partner/) | diversityinclusion@domain | n/a | n/a |
| [Learning and Development](https://gitlab.com/gitlab-com/people-group/Training) | No alias yet, @mention the [Learning and Development Generalist](/job-families/people-group/learning-development-specialist/) | learning@domain | n/a | n/a |
| Recruiting Leadership | @gl-recruitingleads | n/a | @recruitingleads | n/a/ |
| [Recruiting](/company/team/?department=recruiting) | @gl-recruiting | recruiting@domain | @recruitingteam | n/a |
| [Employer Branding](/job-families/people-group/employment-branding-specialist/) | No alias yet, @ mention the [Senior Talent Brand Manager](/company/team/#bchurch) | employmentbranding@domain | n/a | n/a |
| [Candidate Experience Specialist](/job-families/people-group/candidate-experience/) | @gl-ces | ces@domain |@ces | @ces* |
| [Recruiting Operations](/job-families/people-group/recruiting-operations/)| @gl-recruitingops | recruitingops@domain | @recruitingops | @recruitingops |

## Team Calendars

Here's an overview of all the People group's team events.

<%= partial "includes/people-operations/calendar_overview" %>

## People Business Partner Alignment to Division

Please reach out to your aligned people business partner to engage in the following areas: 
- Career development/[Promotions](https://about.gitlab.com/handbook/people-group/promotions-transfers/)
- [Succession planning](https://about.gitlab.com/handbook/people-group/performance-assessments-and-succession-planning/#succession-planning)
- New organizational structures or re-alignment of team members 
- Employee relations issues (i.e. performance or behavioral issues)
- Performance issues (before starting a [PDPs](https://about.gitlab.com/handbook/underperformance/#performance-development-plan-pdp)/[PIPs](https://about.gitlab.com/handbook/underperformance/#performance-improvement-plan-pip))
- [Coaching](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/coaching/) guidance
- Employee engagement
- Hiring challenges (candidate pipeline, compensation, all other hiring concerns)
- Team member absence or sick leave
- Team member [relocations](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#relocation)
- An unbiased third party to discuss issues or problems and possible ideas for next steps

<%= partial "includes/people-operations/people_business_partners" %>

## Support provided by Director of Legal, Employment, to the People Group.

The Director of Legal, Employment, collaborates with and provides support to the People Business Partner team in many functional areas.

Email approval from the Director of Legal, Employment is required prior to engagement with external counsel to allow for accurate tracking of costs.

Invoices will be sent to Director of Legal, Employment, for approval with the relevant People Business Partner copied for visibility. 

## Support provided by the People Operations Specialist team to the People Business Partner team

All tasks will be assigned via slack in the #pbp-peopleops slack channel. This is a private channel. The People Operations Specialist team, will self-assign a task, within 24 hours and comment in slack on the request to confirm.

## People Experience vs. People Operations Core Responsibilities & Response Timeline

Please note that the source of truth for role responsibilites are the job families for [People Operations Specialist](https://about.gitlab.com/job-families/people-ops/people-operations/) and [People Experience Associate](https://about.gitlab.com/job-families/people-ops/people-experience-associate/). The table below is meant to provide a quick overview of some of the core responsibilities for each team.

### People Experience Team

| Responsibility                         | Response Timeline  |
|-----------------------------------|-----------------|
| [Onboarding](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/) | 4 days prior to start date |
| [Offboarding](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/) | Immediate action for involuntary & 24 hours for voluntary |
| [Internal Transitions](https://about.gitlab.com/handbook/people-group/promotions-transfers/) | Within 24 hours |
| [Letters of Employment](https://about.gitlab.com/handbook/people-group/frequent-requests/#letter-of-employment) | Within 48 hours |
| [Employment Verification](https://about.gitlab.com/handbook/people-group/frequent-requests/#employmentverification) | Within 48 hours |
| [Anniversary Emails](https://about.gitlab.com/handbook/people-group/celebrations/#anniversary-gifts) | Last day of the month |
| Slack and Email Queries | Within 24 - 48 hours |
| [Probation Period Notifications](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#probation-period) | Daily (when applicable) |
| [Group Conversations scheduling](https://about.gitlab.com/handbook/people-group/group-conversations/) | As requested |
| [Hosting of company calls](https://about.gitlab.com/handbook/people-group/group-conversations/) | Daily on a weekly rotation |
| [Evaluate onboarding surveys](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) | As response are submitted and on a monthly basis |
| [Flowers and gift ordering](https://about.gitlab.com/handbook/people-group/#gifts) | Within 24 hours |
| [Determining quarterly winners of the onboarding buddy program](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) | Last week of each quarter  |
| [People Experience Shadow Program](https://about.gitlab.com/handbook/people-group/people-experience-shadow-program/) | As requested by other team members  |

Other listed processes for the People Experience Team can be found [here](https://about.gitlab.com/handbook/people-group/people-experience-team)

### People Specialist Team

| Responsibility                         | Response Timeline  |
|-----------------------------------|-----------------|
| [Risk Matrix Data Update](hhttps://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#international-expansion) | Monthly |
| [Relocation Conversions](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#relocation-conversions) | As requested, usually completed 30 days or more prior to team member's relocation date |
| [Exit Interviews](https://about.gitlab.com/handbook/people-group/offboarding/#exit-survey) | During voluntary offboarding team member's last week |
| [Hosting of Company Calls](https://about.gitlab.com/handbook/people-group/group-conversations/) | Daily on a weekly rotation |
| [Administration of the signing of our Code of Conduct](https://about.gitlab.com/handbook/people-group/code-of-conduct/#code-of-business-conduct--ethics-acknowledgment-form) | Annually in Feb/March |

## Celebrations
{: #celebrations}

How the GitLab team celebrates work anniversaries and further information about anniversary gifts and birthdays can be found on this [page](/handbook/people-group/celebrations)

## Frequently Requested
{: #frequently-requested}

Please review the [frequently requested section](/handbook/people-group/frequent-requests) of the People Handbook before reaching out to the team. The page includes information on accessing a team directory, requesting a letter of employment, mortgage forms, the companies reference request policy, ordering business cards, and changing your name in GitLab systems.

## Other pages related to the People Group

- [Benefits](/handbook/total-rewards/benefits/)
- [Code of Conduct](/handbook/people-group/code-of-conduct/)
- [Promotions and Transfers](/handbook/people-group/promotions-transfers/)
- [Global Compensation](/handbook/total-rewards/compensation/)
- [Incentives](/handbook/incentives)
- [Hiring process](/handbook/hiring/)
- [Group Conversations](/handbook/people-group/group-conversations)
- [Leadership](/handbook/leadership/)
- [Learning & Development](/handbook/people-group/learning-and-development/index.html)
- [Onboarding](/handbook/people-group/general-onboarding/)
- [Offboarding](/handbook/people-group/offboarding/)
- [OKRs](/company/okrs/)
- [People Group Vision](/handbook/people-group/people-group-vision)
- [360 Feedback](/handbook/people-group/360-feedback/)
- [Guidance on Feedback](/handbook/people-group/guidance-on-feedback)
- [Collaboration & Effective Listening](/handbook/people-group/collaboration-and-effective-listening/)
- [Gender and Sexual-orientation Identity Definitions and FAQ](/handbook/people-group/gender-pronouns/)
- [Travel](/handbook/travel/)
- [Underperformance](/handbook/underperformance)
- [Visas](/handbook/people-group/visas)
- [People Group READMEs](/handbook/people-group/readmes/)
- [Women in Sales Mentorship Pilot Program](/handbook/people-group/women-in-sales-mentorship-pilot-program)
- [People Key Performance Indicators](/handbook/people-group/people-success-performance-indicators/#key-performance-indicators)
- [People Performance Indicators](/handbook/people-group/people-success-performance-indicators/#regular-performance-indicators)

## Leadership Toolkit

Our [leadership toolkit](/handbook/people-group/leadership-toolkit) has tools and information for managers here at GitLab.

## Boardroom addresses
{: #addresses}

- For the SF boardroom, see our [visiting](/company/visiting/) page.
- For the NL office, we use a postbox address listed in the "GitLab BV address" note in the Shared vault on 1Password. We use [addpost](https://www.addpost.nl) to scan our mail and send it to a physical address upon request. The scans are sent via email to the email alias listed in the "Email, Slack, and GitLab Groups and Aliases" Google doc.
- For the UK office, there is a Ltd registered address located in the "GitLab Ltd (UK) Address" note in the Shared vault on 1Password
- For the Germany office, there is a GmbH address located in the "GitLab GmbH Address" note in the Shared vault on 1Password

## Guidelines for Vendor meetings

These guidelines are for all Vendor meetings (e.g. People Group/Recruiting IT system providers, 3rd party suppliers, benefits providers) to enable us to get the best out of them and make the most of the time allotted:

Guidelines:

1. We request external vendor meetings to use our video conferencing tool so we can quickly join the call and record the session if we need to. Confirm with vendor that they agree we can record the call. The DRI for the vendor meeting will generate the zoom link and share with the vendor.

1. Decide ahead of the meeting who should be invited, i.e. those likely to get the most out of it.

1. Ahead of the meeting, we should agree internal agenda items/requirements/priorities and provide to the external provider.

1. In order to make the best use of time, we wish to avoid team introductions on the call, particularly where there are a number of us attending. We can include a list of attendees with the agenda and give it to the vendor before or after the meeting.

1. When a question or issue is raised on the agenda, if the person who raised it is present they will verbalize it live on the call; or if they are not present, then someone will raise it for them. This is a common GitLab practice.

1. Where possible, we request that the vendor provides their slides / presentation materials and any other related information after the meeting.

1. Do not demo your tool live, create a pre-recorded walk-through of the tool and make it available to GitLab before the meeting so we can ask questions if needed.

1. Be cognizant of using inclusive language.

1. We respectfully request that everyone is mindful of the time available, to help manage the call objectives effectively.

## Gifts

**Due to COVID-19** There may be delays on flower/gift delivery. Please keep this in mind when requesting an order.

Managers: Should you wish to send flowers to a team member in the event of a birth, death, or other significant event. Please complete the [GitLab Gift Request form](https://docs.google.com/forms/d/e/1FAIpQLScxwCUNF-9IV-y-XNswQwkzwA-a6ahuPd8HFGEuxw3EMjukrA/viewform) to provide information about the flower request. This significant event must apply to the team member or to an immediate relationship to the GitLab team member. The People Experience Associates will then send the flowers as per the submitted request and message the requester via Slack or email to confirm that the order has been completed. 

Other gifts can be arranged to a max value of 75 - 125 USD, please specify details on the [GitLab Gift Request form](https://docs.google.com/forms/d/e/1FAIpQLScxwCUNF-9IV-y-XNswQwkzwA-a6ahuPd8HFGEuxw3EMjukrA/viewform) or if you see fit you can allocate the cash value amount to the team member to spend and expense on something of their own choice eg. a dinner, a family activity, etc.

The budget for sending these gifts will come from the department budget of the gift-receiving team member. To make sure the reports in Expensify are routed correctly, People Experience Associates will be sure to fill in the correct division and department. Team members' division and departmental information can be found in their BambooHR profile under the Job tab, in the `Job Information` section.

For procedures on how to handle [gifts](/handbook/people-group/frequent-request-procedures)

## Using BambooHR

We use [BambooHR](https://gitlab.bamboohr.com) to keep all team member information
in one place. All team members (all contract types) are in BambooHR.
We don't have one contact person but can email BambooHR if we want any changes made in the platform. The contact info lives in the Secretarial Vault in 1Password.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action, prepare a communication to the team that is approved by the Chief People Officer.

Team Members have access to their profile in BambooHR and should update any data that is outdated or incorrect. If there is a field that cannot be updated, please reach out to the People Ops Analyst with the change.

The mobile app has less functionality than the full website, and the current version has security issues, so use of the BambooHR mobile app is discouraged. If access to BambooHR is required from a mobile device (such as requests that need to be approved), this should be done through the desktop version of the website or through the mobile device's web browser.

## Using Google Drive

**We are a handbook first organziation and google drive should only be utilized when necessary.**

When using google drive, always save team documents to the appropriate [Shared_Drive].

## Using RingCentral

Our company and office phone lines are handled via RingCentral. The login credentials
are in the Secretarial vault on 1Password. To add a number to the call handling & forwarding
rules:

- From the Admin Portal, click on the Users button (menu on left), select the user for which you
  want to make changes (for door handling, pick extension 101).
- A menu appears to the right of the selected user; pick "Call Handling & Forwarding" and review
  the current settings which show all the people and numbers that are alerted when the listed User's
  number is dialed.
- Add the new forwarding number, along with a name for the number, and click Save.

## WBSO (R&D tax credit) in the Netherlands

For roles directly relating to Research and Development in the Netherlands, GitLab may be eligible for the [WBSO (R&D Tax Credit)](http://english.rvo.nl/subsidies-programmes/wbso).

### Organizing WBSO

**Applications**

As of 2019 GitLab must submit three applications each year and the deadlines for those are as follows:

1. **31 March 2019**, for the May - August 2019 period (Product Manager for Create Features)
1. **31 August 2019**, for the September - December 2019 period (Product Manager for Gitaly)
1. **30 November 2019**, for the January - April 2020 period (Product Manager for Geo Features)

There is a [translated English version of the application template](https://docs.google.com/document/d/15B1VDL-N-FyLe84mPAMeJnSKjNouaTcNqXeKxfskskg/edit) located in the WBSO folder on the Google Drive. The applications should be completed by a Product Manager, responsible for features or a service within GitLab, who can detail the technical issues that a particular feature will solve. Assistance on completing the application can also be sought from the WBSO consultant (based in the Netherlands). The contact details for the consultant can be found in a secure note in the People Ops 1Password vault called WBSO Consultant. The People Operations Specialist team will assist with co-ordinating this process. It is currently owned by Finance.

**Hour Tracker**

Each year a spreadsheet with the project details and hours logged against the work done on the project(s) will need to be created. This is for the entire year. The current hour tracker is located in the WBSO folder on the Google Drive, and shared only with the developers that need to log their hours (located in the Netherlands), Total Rewards Analysts, Finance and the WBSO consultant. Once the projects have been completed for the year, the WBSO consultant will submit the hours and project details to claim the R&D grant from the [RVO](https://english.rvo.nl/). The WBSO consultant will contact Total Rewards Analysts should they have any queries.


## Disaster Recovery Plan

When a natural disaster or weather event occurs in a location that is near the home of a team member, the People Operations Specialists will send a notification email to all relevant team members, using the [template](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/natural_disaster_notification.md) found in the People Group project.

GitLab provides free transport to all team members to get out of the location they are in, via a vendor of choice e.g.: Uber/ Taxify/ Lyft/ Via etc. to a safe location.
This section outlines what action will be taken during a natural disaster:
* A People Operations Specialist will reach out in the timezone of the team members affected by a natural disaster/ etc. via email by using this [template](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/natural_disaster_notification.md). A Slack message will also be posted on the location Slack channel and if unavailable in the #team-member-updates channel or direct message. The People Operations Specialist should also keep the leaders of legal, security and the people group updated on their course of action and whether all team members have been reached during this process.
* If there is no response from the team member/s within 24 hours, the People Operations Specialist team will send an email to the team member/s personal email address. Due to the vast number of time zones we operate in, and for transparency, please cc peopleops@domain in all communications.
* If there is still no response after 48 hours, a People Operations Specialist will reach out to next-of-kin (this info can also be found in BambooHR) via email, and if no response, followed by a phone call.

## Women in Sales Mentorship Pilot Program

GitLab launched a Women in Sales Mentorship Pilot for the start Q2 of FY21. If the pilot is considered successful, we will consider expanding the program to other cohorts. For more information on the program and how to apply, visit the [Women In Sales Mentorship Pilot Program](/handbook/people-group/women-in-sales-mentorship-pilot-program) handbook page!
