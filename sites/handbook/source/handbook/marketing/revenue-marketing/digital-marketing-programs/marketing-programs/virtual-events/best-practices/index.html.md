---
layout: markdown_page
title: "Virtual Events Best Practices"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

General best practices for virtual event planners, speakers, and attendees that apply across all [virtual event](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/) types. You may also want to review the [GitLab Video Playbook](/handbook/communication/video-playbook/), which offers guidance on creating engaging video content, much of which also applies to virtual events.

## Event planners
* Choose tech based on your event goals and structure of event/ presentations. 
* Consider your narrative - people will be more likely to tune in if you are telling a story throughout the day vs. it being one of talks that aren't related. 
* Have all presenters test their A/V setup in advance of event.
* Ask speakers who will be presenting live to test (and if necessary troubleshoot) their wifi connection prior to the event. Especially important for live demos and coding.
* If you have time, do speaker run through with all speakers to review slides and give feedback and answer any flow questions.
* Have a final check in meeting with MC's, moderators, and speakers. If you can have moderator meet their speaker on video call before the event to build rapport. 
* Give moderators extensive instructions, especially if coordinating in-person as well as online presenters.
* Do the same for the speakers. What do they have access to that's different than the attendees? 
* Give quick video instructions on how attendees can use the platform. Also provide written instructions. 
* Have presenters sign in at least 20 min before to retest their audio and setup. Once they are all set, allow them to go odd and make the most use of their time and ask that they rejoin 5 mins before starting. This will give proper time to troubleshoot if needed. 
* Make sure no one other than specific people can close the stream as people might accidentally otherwise. 
* Prep back up plans for speakers who have technical issues, don't show, or other things that pop up. Have a back up for all live demos.
* If possible make sure people don't need to re-join (i.e. it's one long hangout/zoom) through the day, attrition greatly increases if they keep needing to re join.
* Have intermissions/ transition slides with tips for the audience. You've got your audience, make the best use of it! 
* Keeping the audience engaged:
  * Survey the audience.
  * Add a giveaway or chance to get swag - be sure to build the process on the back end on how you can execute this in advance. i.e. if your swag vendor will allow you to ship out 25 items at no cost, then do a raffle for 25 or use a swag platform where people can enter in their own contact information. 
  * Create a Slack channel or something similar for attendees to build community & keep the convo going there -- Remember your CTA? 
  * If possible allow community/ attendees engage with speakers post talk.
* Find ways to keep the discussion going- the event shouldn't end with the camera stops. Decide on a CTA. Let folks know next steps and how to continue to engage and educate themselves. 
* Have a plan for content use and distribution post-event. Be sure to communicate to the audience if they will receive the slides/recording and also a timeframe in which they will receive the content. 

## Speakers
* Test your technology in advance - including any applications you will be using (ex: Zoom, WebEx).
* Have backup headphones and a phone available in case you need to dial in. 
* Have a dedicated microphone (as oppossed to using your laptop mic). We recommend a wired microphone (can be connected to your headphones) as many Bluetooth microphones suffer from latency and quality issues. 
* If you can, have someone fielding questions for you.
* Have slides or visuals to supplement your talk if it is just you so that it doesn't feel like just a meeting or lecture. 
  * Share slides in advance when possible and at the beginning of your session. This can help people follow along in the event of any technical difficulties.
* Wear camera-ready colors- [see doc](https://docs.google.com/document/d/1rPvewsTWm8uqGv-6Wr4-_j4ZmBVjL75fU5_YGV98d24/edit?ts=5e74125a)
* Remember your background: this will be recorded and published online. 
    * Recommend using GitLab branded virtual backgrounds. Please refer to [New Background GitLab Connect Day] (https://drive.google.com/drive/search?q=new%20backgrounds%20gitlab%20connect%20day) for examples. 
* Slides: use a large clear font. You do not know what device the viewer will be on. 
* Don’t move around a lot.
* Look at the camera.
* Dress as if you were at an actual conference.

## Attendees: How to get the most out of attending a virtual event
* Guide and enable people to share on social .
* Remind them how they will be able to access content post-event.
* Teach them how to use the platform you are doing the event on.

## Partners
* Best practices for managing a virtual booth- suggestions to get the most out of your investment, have the most personable interactions and give best attendee experience. 
  * Always have a virtual offering, discount or giveaway.
  * If you can have a person or multiple people staff the booth and be there to answer questions.
  * have a video or digestible slide deck ready for viewing.

