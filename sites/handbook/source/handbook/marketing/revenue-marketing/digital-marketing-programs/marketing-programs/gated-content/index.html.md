---
layout: markdown_page
title: "Gated Content"
---

## Overview
This page focuses on gated content as an offer for our marketing channels and integrated campaigns, including in-house created content, analyst relations content, and downloadable competitive content. If you have any questions about content planning, please visit the [Global Content handbook page](/handbook/marketing/growth-marketing/content/#what-does-the-global-content-team-do), and with questions about the process of tracking and landing page creation below, please post in the [#marketing_programs Slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

#### Types of Content Programs
* Gated Content: We have the piece of content offered on our website via landing page.
    * [GitLab Created Content](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#internal-content-created-by-the-gitlab-team): We created the content in house
    * [Analyst Content](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#analyst-content-delivered-by-analysts): We have bought the rights to use the content from an analyst such as Gartner, Forrester, etc.
    * [On-Demand Webcasts](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)
* [Content Syndication](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication): We have promoted our content through a third-party, but do not direct back to our website. In these cases, we often have given them the resource to make available for download to their audience, and recieve the leads to be uploaded.

#### Gating Criteria

| Gated                                                | Not Gated                                                                          |
|------------------------------------------------------|------------------------------------------------------------------------------------|
| Whitepapers                                          | Blogs                                                                              |
| eBooks                                               | Infographics                                                                       |
| Reports                                              | Technical Training Resources                                                       |
| On-Demand Webcasts                                   | Customer Testimonials                                                              |
| Comparison PDFs                                      | Case Studies                                                                       |

> Note: if a new type of gated content needs to be introduced, and it is expected that it will be a regularly created piece of content (for example, as we begin to roll out Comparison PDFs), please post in [#marketing_programs slack channel(https://gitlab.slack.com/archives/CCWUCP4MS) to determine if steps need to be taken with Marketing Ops and Sales Ops for automation and reporting.

### Internal Content (created by the GitLab team)

Alignment to Campaigns: All gated content should be aligned to a content pillar (owned by Content Marketing) and a primary integrated campaign (owned by Marketing Programs). If no active or planned campaign is aligned, a campaign brief (including audience data) must be created for use by Digital Marketing Programs. _There should be a clear reason why we are moving forward with content that doesn't align to an integrated campaign._

Reporting: when someone submits a form on the gated content landing page, an online bizibe touchpoint is created containing rich utm parameters. The campaign landing page often uses different language aligned to the integrated campaign messaging.

#### Organizing content pillar epics and issues

🏷️ **Label statuses:**
* **status:plan**: content is in a brainstormed status, not yet approved and no DRI assigned
* **status:wip**: content is approved, with launch date determined and DRI assigned

1. **Content Pillar Epic:** `Content DRI` creates content pillar epic
1. **Content Asset Epics:** `Content DRI` creates content asset epics (using code below) and associates to pillar epic
1. **Related Issues:** `Content DRI` creates the issues as designated in the epic code, and associates to the content asset epic

[View the workback timeline calculator here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1648326617)

```
> Name this epic using the following format: *[type] Name of Asset* (ex. [eBook] A beginner's guide to GitOps)
> Start Date = date epic opened, Due Date = launch date
**DELETE THIS LINE AND ABOVE ONCE EPIC EDITED**

## Launch date: 

## [Live landing page link]() - `to be added when live`

#### :key: Key Details
* **Content Marketing:**  
* **Marketing Programs:** 
* **Product Marketing:** 
* **Official Content Name:** 
* **Official Content Type:** 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()

## :memo: Documents to Reference
* [asset copy draft]() - `doc to be added by Content Marketing`
* *phase out?* [landing page copy]() - `doc to be added by Content Marketing` ([clone the template here](https://docs.google.com/document/d/1xHnLKPCaXrpEe1ccRh_7-IqgNbAlzQsZVc-wr1W4ng8/edit#))
* [creative requirements]() - `Content DRI to link to new tab on relevant use case creative googledoc` - [handbook for more info]()
* [creative final]() - `Design DRI to link to repo in GitLab`

### :books: Issues to Create
<details>
<summary>Expand below for quick links to issues to be created and linked to the epic.</summary>

[Use the workback timeline calculator to assign correct due dates based off of launch date](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1648326617)

* [ ] Prep: [Asset Copy Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issuable_template=content-resource-request) - Content
* [ ] Prep: [Landing Page Copy Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=marketo-landing-page-copy) - Content
* [ ] Prep: [Asset Design Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issuable_template=design-request-content-resource)
* [ ] Prep: [Digital Design Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/-/issues/new?issuable_template=) - Design `need issue temlpate`
* [ ] Develop: [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=pathfactory_request)
* [ ] Develop: [Marketo Landing Page & Automation Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-creation)
* [ ] Activate: [Resource Page Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=resource-page-addition)
* [ ] Activate: [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template)
* [ ] Activate: [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request)
* [ ] Activate: [Website Merchandising Issue]() - Content `need issue template`
* [ ] Activate: [Blog Merchandising Issue]() - Content `need issue template`
* [ ] Activate: [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion)
</details>

/label ~"Content Marketing" ~"Gated Content" ~"Marketing Programs" ~"mktg-status::plan"
```


### `Analyst Content` (delivered by analysts)

Analyst reports (i.e. Forrester Wave, Garter Magic Quadrant, etc.) follow a different timeline with different steps to launch and activate. As soon as **Analyst Relations** is aware of an upcoming report that we are likely to purchase and tentative launch timeline, an issue should be created, notifying Marketing Programs.

When **Analyst Relations** determines that they will be purchasing a report to be gated, they must [submit an issue using the analyst gating template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Request-Analysts-MPM).

If the analyst content is thought leadership (i.e. a whitepaper or non-competitor comparison report), it should be planned in advance of purchase with `at least a 30 business day time to launch date`. This allows time to plan activation into existing and future integrated campaigns and content pillars.

**Timeline for content launch & activation: (BD = business days)**

* 📅 **T-30 BD** *Prep date:* The deadline for overall timeline to be agreed by all team members involved. All team members should aknowledge that they are aware of the content and agree to the timelines.
* 📝 **T-27 BD** *LP copy date:* The deadline for the landing page copy
* 🚀 **T-0 BD** *Content load AND launch date:* The deadline for the asset to be added to a Pathfactory track and placed in a nurture stream. For analyst assets, the load date is also the date the gated content landing page will go live, and content is activated across channels.

[View the workback timeline calculator here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1648326617)

#### Organizing analyst content epics and issues
1. **Analyst Relations Asset Epic:** `Analyst Relations DRI` creates epic (using code below)
1. **Related Issues:** `Analyst Relations DRI` creates the issues as designated in the epic code, and associates to the analyst relations asset epic

*Please note that if details and landing page copy are not provided, the MPM is blocked and will not be able to gate the resource.*

```
> Name this epic using the following format: *[report] Date/Quarter Name Exactly as Appears On Report* (ex. [report] 2020 Gartner MQ for Application Security Testing (AST))

> Start Date = date epic opened, Due Date = launch date

**DELETE THIS LINE AND ABOVE ONCE EPIC EDITED**

## Launch date: 

## Live Pages: [/analyst/ commentary page]() and [download page]() - `to be added when live`

#### :key: Key Details
* **Analyst Relations:** 
* **Product Marketing:** 
* **Marketing Programs:** 
* **Official Content Name:** 
* **Validity / Expiration Date:** (start date - end date)
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()

## :traffic_light: Green light?

:white_check_mark: *If the decision is made to purchase the report, AR opens the below documents and issues to request work from the relevant teams to execute toward launch.**

:no_entry: *If the decision is made **not** to purchase the report, AR closes out the epic.*

## :memo: Documents to Reference
* [content draft]() - `link to temporary unlicensed version when available`
* **phase out?** [landing page copy]() - `doc to be added by Analyst Relations` ([clone the template here](https://docs.google.com/document/d/1xHnLKPCaXrpEe1ccRh_7-IqgNbAlzQsZVc-wr1W4ng8/edit#))
* [promotion creative requirements]() - `Design DRI to link to new tab on relevant use case creative googledoc` - [handbook for more info]()
* [promotion creative final]() - `Design DRI to link to repo in GitLab`

### :books:  Issues to Create
<details>
<summary>REGARDLESS IF PURCHASED Expand below for quick links to issues to be created and linked to the epic.</summary>
* [ ] Develop: [/analysts/ commentary page and /analysts/ page]() - AR `need issue template`
* [ ] Activate: [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request)
* [ ] Activate: [Blog Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch)
* [ ] Activate: [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement)
* [ ] Activate Internally: [Sales Enablement Issue]() - `or confirm it's on agenda for cadenced updates to sales from AR`
</details>

<details>
<summary>IF PURCHASED Expand below for quick links to issues to be created and linked to the epic.</summary>
* [ ] Manage: [Asset Expiration Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Expiration-Analysts-MPM)
* [ ] Prep: [Digital Design Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/-/issues/new?issuable_template=NEED THISSSSS) - Design `need issue template`
* [ ] Prep: [Landing Page Copy Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=marketo-landing-page-copy)
* [ ] Develop: [Marketo Landing Page & Automation Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-creation)
* [ ] Develop: [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=pathfactory_request)
* [ ] Activate: [Resource Page Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=resource-page-addition)
* [ ] Activate: [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template)
* [ ] Activate: [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion)
</details>

cc @jgragnola 

/label ~"Content Marketing" ~"Gated Content" ~"Analyst Relations" ~"Marketing Programs" ~"mktg-status::plan"
```

🏷️ **Label statuses:**
* **status:plan**: analyst content has not yet been purchased, not yet approved and no DRI assigned
* **status:wip**: analyst content is approved for purchase, delivery date and launch date determined, DRI assigned


## Where to upload final asset

To be completed by the Marketing Program Manager.

**Add to Pathfactory**
* Follow handbook instructions [here](/handbook/marketing/marketing-operations/pathfactory/#how-to-upload-content)

**Add to /downloads/ repository** (only available and recommended for assets under 2 MB size)
*The purpose of this step is to make it possible to flip the autoresponder if Pathfactory were to have an outtage, at which point, we would still have the PDF version available in Marketo for a quick turnaround.*

1. Save the pdf to your computer with naming convention `[asset-type]-short-name-asset`, ex: `ebook-agile-delivery-models`
1. Navigate to the (de-indexed) [`resource/download`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources) directory
1. Where it says `www-gitlab-com / source / resources / +`, click the plus drop down and select "Upload File"
1. Upload the file you've saved to your computer with the naming convention above
1. For commit message, add `Upload [Asset Type]: [Asset Name]`, check box for "create new merge request", name your merge request, and click "Upload file"
1. Add description to MR, complete the author checklist, assign to `@jgragnola` and click "Submit Merge Request"
1. In your Marketo program, for the `pdfVersion` My Token, add the naming convention above which will be available when the MR is merged. (the token should look like `http://about.gitlab.com/resources/downloads/add-file-name-here.pdf`)

## Marketo automation and landing page creation

[Watch the video tutorial >]() - `to be created`

:exclamation: Dependencies: delivery of final asset, completion of final landing page copy, and final asset added to pathfactory and placed in a track must be complete before setting up the Marketo program.

* **Create Marketo program, tokens, and SFDC campaign sync**
  * Clone the [Marketo Gated Content Template](https://app-ab13.marketo.com/#PG5111A1) and name new program using naming convention (YYYY_Type_AssetName, i.e. 2020_report_GarnterVOC_ARO)
  * Create SFDC program (Program Summary > `Salesforce campaign sync` > click "not set" and choose "Create New" from dropdown) - leave the name as auto-populates, and add the epic url to the description and "Save"
  * Update all tokens (Program Summary > "My Tokens" tab)
* **Edit landing page url**
  * Right click the landing page object > "URL Tools" > "Edit URL Settings"
  * Input new URL (format: `resources-type-name-of-asset`, i.e. `resources-ebook-ci-best-practices`)
  * Leave "Throw away" existing url selected and click save
* **Activate smart campaign(s)**
  * Click to `01 Downloaded Content` smart campaign
  * Smart List: it's all set! check that it references the program landing page (it should do this automatically)
  * Flow: it's all set! review the flows (but they are all using tokens, so it should be ready to go automatically)
  * Schedule tab: click "Activate" (note: the settings should be that "each person can run through the flow once every 7 days" - this is to avoid bots resubmitting repeatedly)
* **Update SFDC campaign**
  * Navigate to [https://gitlab.my.salesforce.com/701?fcf=00B61000004NY3B&page=1&rolodexIndex=-1] campaigns in Salesforce
  * `Campaign Owner` should be the campaign creater
  * `Active` field should be checked
  * `Description ` must include the epic url
  * `Start Date` should be date of launch
  * `End Date` should be one year later
  * `Budgeted Cost` is required, fill in `0` if internally created content
* **Test live landing page**
  * Click to the landing page object and click "View Approved Page"
  * Final QA of all copy
  * Submit the form
  * Final QA that the resulting link sends to Pathfactory with the tracking for the email address (`&lb_email=[email submitted in form]`)
  * Check that you received the follow up email
  * Final QA of confirmation email copy
  * Final QA that the confirmation email link sends to Pathfactory with the tracking for the email address  (`&lb_email=[email submitted in form]`)

## Add new content to the Resources page
1. Begin a new MR from [the resources yml](https://gitlab.com/gitlab-com/www-gitlab-com/edit/master/data/resources.yml)
2. Use the code below to add a new entry with the relevant variables
3. Add commit message `Add [resource name] to Resources page`, rename your target branch, leave "start a new merge request with these changes" and click "Commit Changes"
5. Assign the merge request to yourself
6. When you've tested the MR in the review app and all looks correct (remember to test the filtering!), assign to `@jgragnola`
7. Comment to `@jgragnola` that the MR is ready to merge

**Code:**
```
- title: 'Add name of resource - shorten if necessary'
  url: 
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - 
    - 
  solutions:
    - 
    - 
  teaser: 'Add a teaser that relates to the contents of the resource'
```

**Example:**
```
- title: '10 Steps Every CISO Should Take to Secure Next-Gen Software'
  url: /resources/ebook-ciso-secure-software/
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - DevSecOps
    - Security
  solutions:
    - Security and quality
  teaser: 'Learn the three shifts of next-gen software and how they impact security.'
```

**IMAGES to choose from (select one):**
*[Shortcuts to Images Folder](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/resources)
* `/images/resources/cloud-native.png`
* `/images/resources/code-review.png`
* `/images/resources/continuous-integration.png`
* `/images/resources/devops.png`
* `/images/resources/git.png`
* `/images/resources/gitlab.png`
* `/images/resources/security.png`
* `/images/resources/software-development.png`

**TOPICS to choose from (add all that apply):**
* Agile
* CD
* CI
* Cloud Native
* DevOps
* DevSecOps
* Git
* GitLab
* Public Sector
* Security
* Single Applicaton
* Software Development
* Toolchain

## How to retire analyst assets when they expire

* An [Expiration Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Expiration-Analysts-MPM) will be open for each analyst asset, and related to the overarching Epic upon gating (with due date for when the asset is set to expire)
* At times, we will extend the rights to an asset if it is heavily used by sales or performing well in later stage nurture. In that case the decisio nis indicated in the Expiration Issue, and the team is updated.


## How to gate a piece of content on a standard gated content landing page

PHASING OUT EVERYTHING BELOW THIS LINE.


**The below is being phased out and being replaced with a tokenized program and Marketo landing pages built within the template for efficiency gains.**

**If you are going to be uploading your gated asset as a PDF instead of PathFactory link**, follow the below steps first by creating a MR for the PDF before MR for landing page. *If you create the landing page MR before your PDF MR, you will get a 404 error when clicking the download link after the form fill.*
1. Upload your PDF to the [`resource/download`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources) directory
    *  Ex: ebook-agile-delivery-models
2. Leave commit message including the asset type and asset name you are uploading
    *  In your MR, scroll down to the Changes tab and on your asset click the button `view on about@gitlab.com` and use that as your URL anywhere that you would be using a PathFactory link in the remaining steps (landing page code, Marketo token)
3. Follow the remaining steps for the Gated Content process

#### 1️⃣  Start with the Landing Page on about.gitlab
1. Navigate to the [/resources/ repo](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources)
1. Click the `+` dropdown and select `New Directory`
1. Add the directory name using the following syntax: `[type]-short-name`
   * *Name is very important as this is the url for the landing page!*
   * Keep it short, and use hyphens between words. (i.e. `ebook-ciso-secure-software`)
1. Add commit message to name your Merge request using syntax `Add [content type] landing page - [name of content]` (i.e. `Add ebook landing page - CISO Secure Software`)
1. Create a name for the target branch - NEVER leave it as the master (i.e. `jax-ebook-ciso`)
1. On the next screen (New Merge Request), add `WIP: ` to the beginning of the title and add a quick description (`Add LP for [content name]` will suffice)
1. Assign to Jackie Gragnola and scroll down, check the box for “Delete source branch when merge request is accepted”
1. Click `Submit Merge Request`
1. You’ve now created the merge request, but you need to add the file (code) for the landing page itself within the directory
1. Click on the link to the right of “Request to Merge” and you will enter the branch you’ve created with your MR
1. Click the `Source` folder, then `Resources` and click into the new directory as you named it
1. Click the `+` dropdown and select `New File`
1. Where it says “File Name” at the top, type in `index.html.haml`
1. Copy the following code: (see below)

```
---
layout: default
title: `add title here, don’t use colons`
suppress_header: true
extra_css:
  - styles-2018.css
destination_url: "`add the pathfactory url when available&lb_email="
form_id: "1002"
form_type: "resources"
cta_title: "Download the `type: eBook, etc`"
cta_date: 
cta_subtitle: 
link_text: "Click here to download the `type: eBook, etc`."
success_message: "You will also receive a copy of the `type: eBook, etc`sent to your inbox shortly."
---

.wrapper
  .page-illustrated-header-container
    = partial "includes/icons/gitlab-icon-pattern-header.svg"
    .container
      .header-container-content
        %h1.page-headline `add headline from copy doc`
        %h2.page-headline `add subhead from copy doc`

  .content-container
    .wrapper.container{ role: "main" }
      .row
        .col-xs-12.col-md-6.col-md-offset-1
          .content-section
            %p `Add P1 from copy doc`
            
            %p `Add P2 from copy doc`

            %h3 What you’ll learn in this `type: eBook, etc.`:
            %ul
              %li `add bullet 1`
              %li `add bullet 2`
              %li `add bullet 3`

            %p `add closing paragraph`

        .col-md-4.col-md-offset-1
          = partial "includes/form-to-resource", locals: { destination_url: current_page.data.destination_url, form_id: current_page.data.form_id, form_type: current_page.data.form_type, cta_title: current_page.data.cta_title, cta_date: current_page.data.cta_date, cta_subtitle: current_page.data.cta_subtitle, link_text: current_page.data.link_text, success_message: current_page.data.success_message }
```

1. Back in your MR, paste, the code where the file begins with 1.
1. Update the variables in the codeIn all the places in `snippet code`
  * *Be sure to remove the back-tick code symbols* so that the copy `does not turn red on the landing page`. This is just to be used as a guideline while editing the landing page.
1. If you don’t yet have a pathfactory link, leave the copy in that place. When you have  the Pathfactory link in a later step, you’ll edit the MR with the new link before pushing live (see later step)
1. Important! If there is a colon `:` in your title, replace with a dash `-` for the page title, as it will break the page.
1. In the Commit Message below your code, add a note that you’re adding the code for the landing page
1. Click `Commit Changes`
1. Now, while you wait for the pipeline to approve, move onto the next step in Marketo to facilitate the flows and tracking of the program.

#### 2️⃣  Create Marketo Program
1. Clone the [gated content template](https://page.gitlab.com/#PG2524A1) in Marketo
1. Choose clone to `A campaign folder`
1. Use naming syntax `[yyyy]_[type]_NameOfAsset` (i.e. 2019_eBook_CISOSecureSoftware) - keep this short
1. Add to folder: `Gated Content` > `Content Marketing`
1. Leave description blank and select “create”
1. When the new program loads, copy the url of the Marketo program and add to the description of the gating issue under `DRIs and Links`

#### 3️⃣  Create Salesforce Program
1. Create SFDC program (Program Summary > `Salesforce campaign sync` > click "not set" and choose "Create New" from dropdown) - leave the name as auto-populates, and add the epic url to the description and "Save"
1. Click save, and navigate to the [campaigns view](https://gitlab.com/groups/gitlab-com/marketing/-/epics/400) in Salesforce
1. Choose the “Gated Content” campaign view and make sure it is in order by Created Date (first column) from latest to earliest creation. Your campaign should appear at the top.
1. Click into your campaign and change the owner to your name
1. Copy the url of the Salesforce campaign and add to the description of the gating issue under `DRIs and Links`

#### 4️⃣  Update the Marketo Tokens
1. Navigate to the `My Tokens` section of the Marketo program, and make the following edits:
  * Content Download URL: add the PathFactory link after the asset is loaded into Pathfactory and added to a track. This is provided by the Pathfactory DRI. You may need to update the confirmation email button link to follow [PathFactory link criteria](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/pathfactory-management/#appending-utms-to-pathfactory-links) or to remove extra tokens that will break the PDF token.
  * Content Epic URL: the url to the overall epic of the content
  * Content Title: the content title as it appears on the asset - not necessarily what is displayed as the landing page header on the copy doc (i.e. `10 Steps Every CISO Should Take to Secure Next-Gen Software`)
  * Content Type: select the appropriate content type (i.e. leave just `eBook`)
  * UTM: leave the code that is originally there, and at the end of the code, add the name of the Marketo program, removing any underscores or special characters (i.e. `2019eBookCISOSecureSoftware`)
  * Copy and paste the 3 bullets from the content copy document into the 3 tokens aligned to the bullets

#### 5️⃣  Preview the emails
1. Preview the confirmation email to make sure that the tokens appear correctly.
1. If you haven’t uploaded to Pathfactory and received a final link, your button will be broken. When you add that to the Marketo tokens, the button will work properly.

#### 6️⃣  Edit, Review, and Activate Marketo Smart Campaign
1. In the `01 Downloaded Content` smart campaign, under `Smart List`add the url created in step 1 to the `/resources/` link in the referrer constraint (i.e. add `ebook-ciso-secure-software` after`/resources/` so that it is `/resources/ebook-ciso-secure-software`
  * In the `Flow` section, review the steps. It should include:
  * Send Email: Confirmation email from your program (this automatically pulls in)
  * Remove from flow with “choice” that if email address contains `@gitlab.com` it should not continue the steps
  * Change program status in your program to `Gated Content  > Downloaded`
  * Interesting moment using tokens with “Web” type, and description: `Downloaded the {{my.content type}}: “{{my.Content Title}}`
  * Send alert to `None`and {{my.mpm owner email address}} (reps receive MQL alerts, so this only triggers to the MPM and can be filtered out of your email if desired).
  * Change data value if `Acquisition Program` is empty to now be the current program
  * Change data value if `Person Source` is empty to be “Gated Content - {{my.content type}}”
  * Change data value if `Initial Source` is empty to be “Gated Content - {{my.content type}}”
1. In the Schedule section, check that it’s set for each person to run through the flow every 7 days and click `Activate`

#### 7️⃣  Test Your Page in the Review App
1. Navigate back to your MR in GitLab
1. When your pipeline has complete, click “View App” and add the URL you chose for your page (i.e. add `/resources/ebook-ciso-secure-software`to the end of the view app url
1. Fill out the form using your test lead (using email syntax [your email]+[name]@gitlab.com - i.e. `jgragnola+george@gitlab`)
1. Check in the Marketo program that it triggered the autoresponder, addition to the program, and alert.
1. In your inbox, check the confirmation copy and urls.
1. If this fires properly, return to the MR and assign to Jackie with a comment to ping her and let her know it’s ready to merge.
1. Check off all completed actions on the issue.
1. When the MR merges into the master, it will appear at the URL you determined.
1. When it has appeared, test it once more with a different test lead email address, and confirm that everything fires as desired.

#### 8️⃣ Add Your Page to the Rsesources Page
1. Begin a new MR from [the resources yml](https://gitlab.com/gitlab-com/www-gitlab-com/edit/master/data/resources.yml)
2. Use the code below to add a new entry with the relevant variables
3. Add a commit message, rename your target branch, leave "start a new merge request with these changes" and click "Commit Changes"
4. Update the name of your merge request to be `Add [resource name] to Resource Center`
5. Assign the merge request to yourself
6. When you've tested the MR in the review app and all looks correct (remember to test the filtering!), assign to Jackie
7. Comment to Jackie that the MR is ready to merge




#### 🏁  Notify the Team
1. Comment in the issue with the final URL and a message that the page is now live and working as intended.
1. Close out the issue.