---
layout: handbook-page-toc
title: "Virtual Events"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

At GitLab, we believe everyone can contribute. Because nearly anyone with an internet connection and a device can participate in virtual events, as a speaker or an attendee, virtual events are an important part of fulfilling that mission. While our expertise in this area is still developing, this page documents the best practices we have studied and established for virtual events at GitLab. 

## GitLab virtual events decision tree

We have developed a decision tree to help you determine what type of GitLab-hosted virtual event is the best fit for you: 

![image](/images/handbook/marketing/marketing-programs/DecisionTree-05042020.png)

## Virtual event types


### Self-service

This is a light weight virtual event that can be hosted on GitLabber's personal zoom. This is recommended for smaller virtual events (200 attendees max) and **allows you to break the audience into smaller groups during the event. Attendees are able to be interactive in this event type, having the option to share both audio/video if allowed by the host. We can track registration, but there is NO Marketo integration, which requires manual list upload to Marketo post event. For this virtual event type, no MPM program support pre/post event will be required. [Self-service handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/)

### Self-service w/ promotion

This is the same format as a self-service event, except **MPM will support Marketo/SFDC program creation, 1-2 invitations, assist with list handoff, and a send follow-up email**. [Self-service handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/)

### Webcast

This is a Marketing Programs hosted virtual event with `Webcast` type configuration in zoom. This is the preferred setup for larger GitLab hosted virtual event (up to 1,000 attendees) that **requires registration** due to the integration with Marketo for automated lead flow and event tracking. GitLab hosted webcast type is a single room virtual event that allows multiple hosts. Attendees cannot share audio/video unless manually grated access. For this virtual event type, MPM supports program creation, driving registration, infrastructure, live moderation, and followup. [Webcast handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)

### Virtual Conference

Please work with the Corporate Marketing Events team on planning these large scale events. [Virtual Conference handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/virtual-conference/) This event type is only intended if you need more tracked attendees than is capable with zoom and/ or you need multiple simultaneous tracks. It also allows for a partner expo hall. The tool we have in place allows for a cleaner experience for the end user than switchign between zoom calls. It more mirrors a physical event experience, with a keynote stage, tracks, expo halls and networking options. 

### Sponsored virtual events

Note: these events are not included in the above decision tree as they are not GitLab-hosted events. [External virtual events handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/)

* **Sponsored demand gen webcast:** This is webcast hosted on an external partner/vendor platform (e.g: DevOps.com). The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform. Mktg-OPs will be responsible for uploading the list to our database and MPMs will be responsible for sending post-event follow-up emails. *[Link to Marketo program template.](https://app-ab13.marketo.com/#ME4634A1)*
* **Sponsored Virtual Conference:** This is not a webcast but rather a virtual conference where we pay a sponsorship fee to get a virtual booth and sometimes a speaking session slot. Marketing programs will primarily be responsible for sending the post-event follow-up emails. *[Link to Marketo program template.](https://app-ab13.marketo.com/#ME4739A1)*


### GitLab-hosted Virtual events details

|                           |  Self-service |  Self-service w/ promotion   | Webcast | Virtual Conference  |
|---------------------------|---------|----------------|------------------|----------|
| Attendee Size             | 1-100              | 1-100                 | 201-1000 | 500+
| SLA                       |  5 BD              | 21 BD                 | 45 BD   |45 BD+  |
| Type                      |  Meeting           | Meeting               | Webcast |Conference  |
| Tracking                  |  No                | No                    | Yes     | Yes  |
| LP/Registration           |  Optional<br>(Zoom) | Optional<br>(Zoom)    | Yes     | Yes  |
| Mktg<br>Promotion         |  No                | Yes                   | Yes     | Yes  |
| Mktg<br>Moderate          |  No                | No                    | Yes     | Yes |
| Mktg <br>Followup         |  No                | Yes                   | Yes     | Yes  |
| Breakout<br>Rooms         |  Yes               | Yes                   | No      | Yes  |
| Polls/Q&A                 |  Yes*/No           | Yes*/No               | Yes/Yes | Yes |
| Confirmation/<br>Reminder |  Yes/No*           | Yes/No*               | Yes/Yes | Yes  |

**FAQ & Notes:**
* Breakout rooms CANNOT be selected by attendees. Only the host can add attendees to rooms. Within each breakout room, Co-hosts can record when given permission by the host.
* For self-service type events, the requestor is responsible for the zoom setup, live moderation, recording and upload (GitLab Unfiltered - Unlisted). Self-service w/ promotion events follow the same requestor responsibilities, with the addition of providing MPM with email and follow-up copy. 
* Review the features to compare [Zoom capabilities]() between meeting type and webcast type

## GitLab virtual events calendar

We have 3 separate calendars to manage different types of virtual events in GitLab. ([webcast](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t), [self-service](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9uMnNibXZmMjlqczBzM3BiM2ozaHRwa3FmZ0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t), [external/sponsored](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xOGVqOHN0NmlxajZpYXB1NTNrajUzNHBsa0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)). All 3 calendars will be consolidated into a single GitLab virtual events calendar view (below). **The purpose of the consolidated view is to help event organizer(s) minimize topic overlap with other GitLab virtual events happening around the same time and to provide executive visibility into ALL virtual events that GitLab is running/participating in.**

#### Glossary of calendar event naming convention:

* `[Hold WC Hosted] Webcast title` - GitLab hosted webcast still in planning
* `[WC Hosted] Webcast title` - Confirmed GitLab hosted webcast
* `[DR WC Hosted] Webcast title` - Dry run for GitLab hosted webcast
* `[Hold self-service] Event title` - Self-service virtual event still in planning
* `[Self-service] Event title` - Confirmed self-service virtual event 
* `[Hold WC sponsored] Webcast title` - Sponsored webcast still in planning
* `[WC sponsored] Webcast title` - Confirmed sponsored webcast
* `[Hold VC sponsored] Conference title` - Sponsored virtual conference still in planning
* `[VC sponsored] Conference title` - Confirmed sponsored virtual conference

<figure>
<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23B39DDB&amp;ctz=America%2FLos_Angeles&amp;src=Z2l0bGFiLmNvbV9uMnNibXZmMjlqczBzM3BiM2ozaHRwa3FmZ0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Z2l0bGFiLmNvbV8xOGVqOHN0NmlxajZpYXB1NTNrajUzNHBsa0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23039BE5&amp;color=%239E69AF&amp;color=%23009688&amp;title=All%20GitLab%20Virtual%20Events&amp;showCalendars=1" style="border:solid 1px #777" width="1000" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

## How to go live?

### Self-service virtual events 

In the spirit of efficiency, we encourage team members to host self-service events when they have interesting content to share with the GitLab community. See the project planning and best practices for producing self-service events in the [self-service virtual events execution page](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/index.html) to get started. 

### Webcasts 

Currently, only the Marketing Programs team can run GitLab hosted virtual event with `Webcast` type configuration in zoom since this configuration requires a special Zoom license and integration with Marketo. To begin a webcast request, visit the [webcast page](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/) and follow the instructions.

## How to promote your virtual event?

### Self-service

**If you have not yet identified if a self-service virtual event is the correct event type of event for you, please scroll to the top of this page to find out.**

Please see the self-service virtual [event promotion guide](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/) for best practice instructions on how to promote.

### Self-service with promotion

Below are Marketing promotion options for self-service virtual event, including requirements and DRI to contact to get the promotions started. Issue links are included in the [epic code template](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#epic-code-for-self-service-with-promotion) for self-service with promotion. The requestor will create the relevant promotion issues for the event and assign to DRI. 

* **Organic Social Post:** 1 post, requires 5 BD turnaround, DRI = Social team
* **Targeted email blast:** 1-2 sends, requires 5 BD turnaround, DRI = MPM

### Webcasts

Below is a summary of the promotion plan for webcasts. MPMs will be responsible for creating all promotion request issues as part of the webcast project management process. The requestor will start with a [virtual event request issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM_VirtualEvent_Request.md) and MPMs will create the necessary epic and related issues. 

* **Blog merchandising:** 1 post, -30 days prior to live event, DRI = Content team
* **Organic Social:** 1-2 post, -14 to -7 days prior to live event, DRI = Social team (requires image for sharing)
* **Paid Ads:** n/a, -21 days prior to live event, DRI = DMP (requires gitlab owned pages - about.gitlab or pages.gitlab)
* **Targeted Email:** 2-3 sends, -14 and -7 days prior to live event, DRI = MPM (copy provided by requestor)
* **Newsletter:** 1 post, -15 to -1 day prior to live event, DRI = MPM (only support English language)

## Virtual event operations 

#### Zoom capabilities

When scheduling a self-service event, this table can help guide you towards the right event type to select based on the features you would like to use during the event. GitLab hosted virtual events will need to fit into either: Zoom Webcast type or Zoom Meeting type.
 
|                                       | Zoom Webcast                | Zoom Meeting                                  |
|---------------------------------------|-----------------------------|-----------------------------------------------|
| Screenshare                           | Yes                         | Yes                                           |
| Video/Audio sharing                   | Host/Panelist only          | All participants                              |
| Participant<br>List                   | Visible to<br>host/panelist | All participants                              |
| Multiple Hosts                        | Yes                         | Yes                                           |
| Attendee Limit                        | 1,000                       | 200                                           |
| Registration<br>Zoom                  | Yes<br>(Optional)           | Yes<br>(Optional)                             |
| Registration<br>Marketo               | Yes                         | No                                            |
| Marketo<br>Integration                | Yes                         | No                                            |
| Automated Tracking<br>and Lead Flow   | Yes                         | No                                            |
| Confirmation Email                    | Yes <br>(from Zoom)         | Yes<br>(from Zoom)                            |
| Auto Reminder<br>Email Send           | Yes<br>(from Zoom)          | No*                                           |
| Practice Session                      | Yes                         | Yes*<br>(requires activating<br>waiting room) |
| Breakout Rooms /<br>Max participants  | 0 / 0                       | 50 / 200                                      |
| Polls                                 | Yes                         | Yes                                           |
| Q&A                                   | Yes                         | No                                            |
| Chat                                  | Yes                         | Yes                                           |
| Raise Hand                            | Yes                         | No                                            |
| Livestream                            | Yes                         | Yes                                           |

### Participant engagement

#### Resources

*  [Managing participants in webcast](https://support.zoom.us/hc/en-us/articles/115004834466-Managing-Participants-in-Webinar)
*  [Managing participants in a meeting](https://support.zoom.us/hc/en-us/articles/115005759423-Managing-participants-in-a-meeting)

#### Chat

*  [In-meeting chat](https://support.zoom.us/hc/en-us/articles/203650445-In-Meeting-Chat) / [Save in-meeting chat](https://support.zoom.us/hc/en-us/articles/115004792763-Saving-In-Meeting-Chat)
*  [Webcast chat](https://support.zoom.us/hc/en-us/articles/205761999-Webinar-Chat)

**Chat announcements**

Chat announcements allow specific users to send one-way announcements to everyone in the same account. Account owners and admin can add up to 50 users who can send announcements.

[Using chat announcements](https://support.zoom.us/hc/en-us/articles/360037120072-Using-Chat-Announcements)

#### Q&A

The question & answer (Q&A) feature for webcasts allows attendees to ask questions during the webcast and for the panelists, co-hosts and host to answer their questions. With the public Q&A feature, attendees can answer each other's questions and if enabled, they may also upvote each others questions.

*  [Getting started with question & answer](https://support.zoom.us/hc/en-us/articles/203686015-Getting-Started-with-Question-Answer)

#### Polling

You can enable polling in your meeting or virtual event to survey your attendees. See the [prerequisites](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#prerequisites) for how to enable polling.

You can also download the results of your poll - see [reporting](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#reporting).

*  [Polling for webcasts](https://support.zoom.us/hc/en-us/articles/203749865-Polling-for-Webinars)
*  [Polling for meetings](https://support.zoom.us/hc/en-us/articles/213756303-Polling-for-Meetings)
*  [3 Ways To Use Polls In Meetings](https://blog.zoom.us/wordpress/2016/09/20/3-ways-to-use-polls-in-meetings/)

#### Networking

There are currently no networking features available in Zoom outside of the participation features listed above. However, there are some creative ways to continue the conversation after your event is over:

*  Create a custom Twitter hashtag for your event and encourage participants to join the conversation 
*  Create a LinkedIn group and invite participants prior to the event
*  If you're doing a self-service event, use the breakout rooms to create a more intimate setting for discussion and opportunity for 1:1's

### Security

Please follow the [prerequisites](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#prerequisites) when setting up your virtual event. The [prerequisites](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#prerequisites) include steps for ensuring your virtual event and account are secure.

### GDPR and privacy compliance

In order to stay compliant with all regulatory policies as it relates to marketing, please adhere the following guidelines when working with registration or attendee lists and event confirmation emails: 

- Password protect sheets for lists (use under GitLab ID only, GitLab only access)
- Delete info in Zoom after event
- Copy of spreadsheet should be deleted after use
- Add opt-out link in confirmation email

Please add the following language when creating your Zoom registration landing page:

>By registering for this event, you agree that Gitlab may email you about its products, services and events. You may opt-out at anytime by unsubscribing in emails or visit our [communication preference center](https://about.gitlab.com/company/preference-center/).

If you are creating a self-service partner virtual event, please add the following language when creating your Zoom registration landing page and replace `[partner name]` with the name of the partner:

>By registering for this GitLab and `[partner name]` event, you agree that Gitlab and `[Partner name]` may email you about their products, services and events. You may opt-out at anytime by unsubscribing in emails or visiting the relevant company's preference center.

### Reporting

#### Resources

*  [Getting started with reporting](https://support.zoom.us/hc/en-us/articles/201363213-Getting-started-with-reports)

There are two types of reports you can export for virtual events from Zoom:

1. **Usage:** view meetings, participants, and meeting minutes within a specified time range
1. **Meeting:** view registration reports and poll reports for meetings

For registration lists:

1. Navigate to `Reports` > `Meeting`
1. Select your report type. For a registration report, select `Registration Report`. If you had polling enabled for your virtual event or meeting, you can select `Poll Report` to download the results of your poll.
1. Select the date of your event. Maximum report duration is one month.
1. Select the checkbox next to your event and click `Generate`.
1. A pop-up box will appear for you to choose the type of registrants (all, approved, or denied).  Choose `all` and click `Continue`.
1. After processing, you are redirected to the `Report Queue` where you can download your results as a .csv file.

For attendee lists:

1. Navigate to `Reports` > `Usage`
1. Select the date of your event and click `Search`. Maximum report duration is one month. The report displays information for meetings that ended at least 30 minutes ago.
1. You can toggle the visible columns you want shown in the results from the `Toggle columns` drop down on the right-side of the screen (optional).
1. Once your event is shown in results, scroll right to the `Participants` column to view the number of participants hyperlinked in blue. Click the linked number of participants.
1. A pop-up box will appear called `Meeting Participants`. From here, you can select the checkbox whether you want to export this list with meeting data or not. Meeting data includes the meeting ID, duration (minutes), # of participants, topic, start time, end time, and user email. 
1. Click `Export`. It exports the list as a .csv file.

### List uploads

If you have a **minimum of 20 leads** to upload from a virtual event, you can utilize the [same list upload process](https://about.gitlab.com/handbook/marketing/marketing-operations/list-import/) through marketing operations. Please create an issue in the marketing operations project using the [`event-clean-list-upload.md`](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/blob/master/.gitlab/issue_templates/event-clean-upload-list.md) issue template and assign it to `@jburton`.

### Zoom landing page registration source tracking

When driving webcast registration to a Zoom landing page, Zoom registration source tracking can be useful for tracking where the registrants are coming from.

[Registration source tracking on zoom landing page](https://support.zoom.us/hc/en-us/articles/360000315683-Webinar-registration-source-tracking) is only available for MPM run `Webcast` type and not the self-service virtual events `Meeting` type set up.

Currently, we do not use Zoom's landing page for webcast registration as MPMs manage the webcast registration process by creating an about.gitlab landing page and integrating form fills to Zoom via Marketo. The Zoom integration tracks sources that drove the registration using UTM parameters. See [full documentation on when and how to use utms](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#utms-for-url-tagging-and-tracking).

### Live streaming

Due to current security concerns, live streaming to YouTube from Zoom has been [temporarily shut down](https://gitlab.com/gitlab-com/gl-security/secops/operations/-/issues/871). There is also a security concern regarding Zoom meetings that are [recorded to the cloud](https://gitlab.com/gitlab-com/gl-security/secops/operations/-/issues/833). If you wish to record your virtual event and upload to YouTube later, record to your local machine (not to the cloud) and open an issue in the [digital production project](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/digital-production) to have the digital production team review your recording prior to being uploaded to YouTube.

### Virtual events best practices

Review GitLab's general [virtual events best practices](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/best-practices/) for additional suggestions on how to make your virtual event a success. You may also want to review the [GitLab Video Playbook](/handbook/communication/video-playbook/), which offers guidance on creating engaging video content, much of which also applies to virtual events. 
