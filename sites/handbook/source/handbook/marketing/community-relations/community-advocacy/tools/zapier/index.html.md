---
layout: handbook-page-toc
title: "Community advocacy tools: Zapier"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Zapier subscription

We use Zapier to automate the task of identifying mentions of GitLab across our [Community Response channels](/handbook/marketing/community-relations/community-advocacy/#community-response-channels) and generally convert them to actionable Zendesk tickets

The Zapier subscription runs on the [Team](https://zapier.com/app/billing/plans) plan, and is shared with the rest of the GitLab team

### Zapier access

- URL: [https://zapier.com](https://zapier.com)
- Account: search for the shared Zapier account in 1Password's `Team` vault 

Once logged in, you can access, edit and create Zaps in the [Community Relations](https://zapier.com/app/zaps/folder/275996) folder

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> After editing or creating a new Zap, remember to turn it on with the toggle switch next to the task's name on the Zap's list.
{: .alert .alert-warning}

### Current Zaps

| Zap | Description |
| --- | --- |
| about.gitlab.com + devops-tools comments | GitLab blog comments and DevOps Tools pages comments to Zendesk and #devoptools-comments Slack channel |
| docs.gitlab.com comments | GitLab documentation comments to Zendesk |
| GitLab Forum New Posts | GitLab forum posts to Zendesk |
| GitLab Swag Store | GitLab store order to Printfection order |
| Hackernews: comments and stories as Zendesk tickets | Hackernews: comments and stories as Zendesk tickets |
| Hackernews: Slack notifications for front page mentions | Hackernews: front page stories to #community-advocates Slack channel |
| Hackernews: Slack Notifications for mentions | Hackernews: mentions to #hn-mentions Slack channel |
| Monthly Swag Report Reminder | Recurring calendar event on the [Monthly Swag Report Reminder calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9ndW9vdmxnbmk1dDh0ZWtpYjlvOGhwcjFyMEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to Zendesk ticket in the Merchandise view |
| Reddit: new mention | Reddit mentions to Zendesk |
| Speaker Request: to Zendesk ticket | Speaker request spreadsheet to Zendesk |
| Stack Overflow (time filter) | Stack Overflow mention to Zendesk (with added delay) |
| Twitter: @GitLabStatus tweets to #gitlabstatus-twitter-updates | @GitLabStatus mentions to #gitlabstatus-twitter-updates |
| Twitter: @movingtogitlab mentions to Zendesk | @movingtogitlab mentions to Zendesk |
| Twitter: Hashtag Challenge to Zendesk and Slack | All Twitter #GitChallenge mentions to Zendesk and #git-challenge Slack channel |
| Community Relations MR Updates | All merged MRs with `Community Relations` label posted to [#community-relations-fyi](https://gitlab.slack.com/archives/C015YDXTREK) |
