---
layout: handbook-page-toc
title: "Pilot program"
---

## Overview

The Community Advocacy team may choose to run workflow changes or new programs as a pilot program for 90 days. 

The team is allowing and implementing pilots in order to:

* be sure a new workflow or program is effective
* avoid overriding previous programs or workflows while trying out something new
* encourage advocates to consider ideas that are divergent from the status quo with less fear of it not working

## Workflow

1. Identify goals
The first step is to outline clear goals and objectives for the pilot program. 

2. Identify handbook changes
Identify which program or process the pilot will replace. Document which parts of the handbook will be overwritten.

3. Create timeline (90 days max)
Once you have determined your goals, set up 4 calendar reminders (1 weeks, 1 months, 2 months, 3 months) to check in on program progress, problems, and finally to determine whether or not to finalize the pilot as a formal program. 

4. Identify a DRI 
Determine who is responsible for the program and who that person will be working closely for the duration of the pilot.

5. Analyzing data
Data is essential to make informed decisions. Metrics should indicate, clearly, where you are in relation to your goals. Good metrics are always trackable, important, and explainable. If you’re unable to formally track and analyze data, ensure a group vote is taken at the end of the pilot to determine whether the team agrees to formalize the pilot.

 ## Current Pilots
