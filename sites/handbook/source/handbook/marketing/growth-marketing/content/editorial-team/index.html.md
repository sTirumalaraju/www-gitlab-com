---
layout: handbook-page-toc
title: "Editorial team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What this page is for

This part of the handbook is for the blog editorial team. If this isn't you, you may find what you are looking for in the [blog handbook](/handbook/marketing/blog), which covers the process for opening an issue and merge request for your blog post, as well as getting reviewed and published.

## Other related pages
- [Blog handbook](/handbook/marketing/blog)
- [GitLab Unfiltered blog handbook](/handbook/marketing/blog/unfiltered/)
- [Brand personality](/handbook/marketing/corporate-marketing/#brand-personality)
 and [tone of voice guidelines](/handbook/marketing/corporate-marketing/#tone-of-voice)
 
## Goal: Grow our audience, engage readers, and convert readers into subscribers

 The GitLab blog exists for our audience and we serve our audience first. Without an audience, blog content cannot support our objectives.
 Every blog post published on the GitLab blog should be crafted with our audience needs and wants in mind. The GitLab blog is not self-serving; if content is purely promotional and adds no value to our readership, it will not be published.

 **We measure success by tracking the following metrics:**

 1. Engagement: # of page views
 1. Reach: # of new visitors
 1. Conversion: # of subscribers both net new and total

## Objectives

 1. Build credibility and authority.
 1. Increase awareness of GitLab the company, product, and community.
 1. Increase organic search rankings and traffic.
 1. Contribute to lead generation and revenue.

## Scope
 **Mission statement:** Create, curate, and elevate stories that increase awareness of the benefits of a single application for the entire DevOps lifecycle, as well as awareness of GitLab as a pioneer in all-remote work.

 Please see [Attributes of a successful blog post](/handbook/marketing/blog/#attributes-of-a-successful-blog-post) below for examples of stories that perform well on our blog.

 The blog is not the permanent place for tutorials, which should live in the docs and should be linked to when relevant.

### Ideal content mix

 <iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ7K736eLyPnzmoA7z4VoHN9a4DhnC4e3zmzfmCpHawjUx7xQXtoIUgqL0IxPdy6Qoj0vw0O9jnvxI9/pubchart?oid=1874991175&amp;format=image"></iframe>

#### Definitions

 1. **Technical:** Articles which dive deep into a technical topic and provide technical details on how to use, implement, or solve something. Technical articles often include code snippets or screen shots, and provide tangible results or actions for the reader.
    - Examples:
      - [What tracking down the missing TCP Keepalives taught me about Docker, Golang, and GitLab](/blog/2019/11/15/tracking-down-missing-tcp-keepalives/)
      - [The Consul outage that never happened](/blog/2019/11/08/the-consul-outage-that-never-happened/)
      - [Improve your productivity by tracking your time and measuring your E-factor](/blog/2019/11/26/e-factor-productivity/)
 1. **Features:** Articles which provide scope, depth, and interpretation of trends, events, topics, or people. Includes articles covering customer stories, trends and/or data analysis, and journalistic reporting on a topic, event, or person.
    - Examples:
       - [The cloud adoption roadmap](/blog/2019/12/05/cloud-adoption-roadmap/)
       - [A brief guide to multicloud security](/blog/2019/11/21/multi-cloud-security/)
       -  [The DevOps tool landsacpe](/blog/2019/11/01/devops-tool-landscape/)
 1. **Product:** Articles that cover GitLab's product evolution, features, and capabilities. Includes release posts, integrations, feedback solicitations, and use case articles centered around how to use GitLab to do something.
    - Examples:
      - [Why GitLab CI/CD?](/blog/2019/04/02/why-gitlab-ci-cd/)
      - [How we are interating on Group Single Sign On for GitLab.com](/blog/2019/01/17/iterating-on-sso/)
      - [3 Major improvements coming to GitLab Epics](/blog/2020/01/21/epics-three-features-accelerate-your-workflow/)
 1. **Remote work:** Articles that cover all-remote work. Includes articles about work/life balance, tricks and tips, building an all-remote company, etc.
    - Examples:
      - [The GitLab handbook by  numbers](/blog/2019/04/24/the-gitlab-handbook-by-numbers/)
      - [Mastering the all-remote environment: My top 5 challenges and solutions](/blog/2019/12/30/mastering-the-all-remote-environment/)
      - [6 Ways to improve communicationin your company](/blog/2019/12/23/six-key-practices-that-improve-communication/)
 1. **Corporate news:** Articles covering GitLab news and announcements such as:
    - Changes to GitLab policies, product, and services. Examples:
      - [Update on free software and telemetry](/blog/2019/10/10/update-free-software-and-telemetry/)
      - [Why we're ending support for MySQL](/blog/2019/06/27/removing-mysql-support/)
    - GitLab company news. Example:
      - [Announcing $268 million in Series E funding](/blog/2019/09/17/gitlab-series-e-funding/)
    - Announcements and updates about internal programs. Example:
      - [Inside the GitLab public bug bounty program](/blog/2019/04/29/inside-the-gitlab-public-bug-bounty-program/)
    - Joining a new program or foundation. Examples:
      - [GitLab leads the industry forward with the CD foundation](/blog/2019/03/12/gitlab-joins-cd-foundation/)
      - [GitLab is now a member of the OWASP Foundation](/blog/2020/01/21/gitlab-is-now-a-member-of-the-owasp-foundation/)
    - Awards. Example:
      - [GitLab achieves AWS DevOps Competency certification](/blog/2018/11/28/gitlab-achieves-aws-devops-competency-certification/)
    - Analyst reports. Example:
      - [GitLab named a 'Visionary' in 2019 Gartner Enterprise Agile Planning Tool Magic Quadrant](/blog/2019/05/22/gitlab-identified-by-gartner-as-eapt-visionary/)
    - Partner announcements. Examples:
      - [GitLab on AWS Marketplace](/blog/2019/06/11/gitlab-on-aws-marketplace/)
      - [GitLab + Salesforce](/blog/2019/05/29/sfdx-promo-trailhead-blog/)   
      - [ZEIT launches Now for GitLab](/blog/2019/04/01/zeit-launches-now-for-gitlab/)
    - Offers. Example:
      - [Extending free use of CI/CD for GitHub on GitLab.com](/blog/2019/03/21/six-more-months-ci-cd-github/)
      
## Top-performing blog posts

Based on [analyses of traffic to blog posts](#blog-traffic-analyses), below are some topics and types of blog posts that usually resonate with our audience and draw the most traffic.

- Tutorials, including integration demos
- Newsjacking, where appropriate (see [best practices for newsjacking](/handbook/marketing/blog/#newsjacking))
- Opinion/taking a stance on a popular subject, e.g.,
  - [The problem with Git flow](/blog/2020/03/05/what-is-gitlab-flow/)
  - [The trouble with technical interviews? They aren't like the job you're interviewing for](/blog/2020/03/19/the-trouble-with-technical-interviews/)
- How we built something/how we use something internally (where readers can learn and apply)
- Git
- See more examples in [Attributes of a successful blog post](/handbook/marketing/blog#attributes-of-a-successful-blog-post)

### Posts that do not draw much traffic

Below are some examples of types of posts that we have found not to be successful with our audience. To prioritize [results](/handbook/values/#results), we try to avoid publishing these types of posts, and have made suggestions for alternatives that may perform better.

For context, we aim for a blog post to achieve a minimum of >1,000 unique views in its first month.
  
#### Recaps

Straightforward event or webcast recaps don't perform well, in part because people look to other major or tech-specific news sources for recaps of major events. You can see [a more detailed audit of event-supporting blog posts here](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/1588).

Examples:
- [Highlights from AWS re:Invent 2019](/blog/2019/12/13/updates-from-aws-reinvent/)
- [From monolith to microservices: How to leverage AWS with GitLab](/blog/2020/03/24/from-monolith-to-microservices-how-to-leverage-aws-with-gitlab/)
- [How to deploy AWS Lambda applications with ease](/blog/2020/04/29/aws-gitlab-serverless-webcast/)

Please see [best practices for event coverage](/handbook/marketing/blog/#gitlab-or-industry-events).
{: .alert .alert-info}

#### Analyst report announcements

Straightforward analyst report announcements do not get many unique views.

Examples:
- [GitLab named Challenger in Gartner Magic Quadrant for Application Release Orchestration 2019](/blog/2020/01/16/2019-gartner-aro-mq/)
- [GitLab named a ‘Visionary’ in 2019 Gartner Enterprise Agile Planning Tool Magic Quadrant](/blog/2019/05/22/gitlab-identified-by-gartner-as-eapt-visionary/)
- [GitLab is named a Challenger in The Forrester Wave™: Software Composition Analysis, Q2 2019](/blog/2019/04/12/gitlab-is-an-sca-contender/)

Please see [best practices for sharing AR news](/handbook/marketing/blog/#analyst-report-news).
{: .alert .alert-info}

#### Highlights or "year in review" round-ups

These are often requested from within GitLab, but our external audience has less of an appetite for these types of posts! 

Examples: 
  - [Highlights from 2019](/blog/2020/01/09/2019-year-in-review/) 
  - [Version 12 year in review](/blog/2020/05/21/version-12-year-in-review/)
  
A better approach is to focus on a specific theme or product area, as in [GitLab CI/CD's 2018 highlights](/blog/2019/01/21/gitlab-ci-cd-features-improvements/). Think about what information someone might be searching for as opposed to what we want to tell them – they are probably looking for a solution to a problem or how to improve on something, rather than simply interested in what GitLab has been doing.
{: .alert .alert-info}

#### Visionary/speculative posts

Visionary/speculative posts don't seem to hold much appeal for our readers, who are often looking for more tactical or informative posts on our blog. 

Examples: 
  - [GitLab and Red Hat: Automation to enhance secure software development](/blog/2020/04/29/gitlab-and-redhat-automation/)
  - [What's coming for Auto DevOps](/blog/2020/04/30/auto-devops/)
  
There needs to be real news, opinion, or practical content for a post to do well.
{: .alert .alert-info} 
      
## Blog traffic analyses 

We review traffic to blog posts on a quarterly basis. You can view past analyses below:

- [FY21-Q1](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/50)
- [FY20-Q4](https://docs.google.com/spreadsheets/d/1UzxsrnVVBUYx2Jrznj6Y4iDhWA4x4GaN0NGn2L01SO0/edit?usp=sharing)
- [FY20-Q3](https://docs.google.com/spreadsheets/d/1sHnJESIV1m7lHgPd8SFqKfEmsQHq5lFGad6ES-oE4M8/edit?usp=sharing)

### Conducting a blog analysis

Pull information on all blog posts to document how many sessions each post received in the month, and how many sessions they received of all time. Categorize them by type, bracket, total sessions in month, total sessions to date, category, theme, and topic. Eventually add first touch point revenue data.
Search Google Drive for `Blog Performance` to find the appropriate sheet to work from.

- Blog post links should be added as they are published and category, audience, theme, and topic should be filled out.
- The Managing Editor and Manager, Content Marketing should review last month on the 1st of the month to fill out session information and make observations
- Review 1st of each quarter to update total sessions

**Column explanations:**

- Type: helps identify the frequency of which certain types of information is shared on our current blog
- Bracket: helps quickly sort blog posts by performance level
- Category: indicates class of information
- Total sessions in month: how many sessions the post received in the month it was published
- Audience: indicates who we expect to reach
- Theme: indicates the structure of the post
- Topic: indicates the main subject covered


## Blog vision

 The following section is based on a [presentation which you can view here](https://docs.google.com/presentation/d/12IydvTCVdM0rt1y3OcHqYpLqtRNzQR4vQT0rs74YhxU/edit?usp=sharing).

### Goals

 **Become a household name**: As GitLab grows and works towards becoming a public company, we want to become a household brand name. In order to do so, we need a corporate message that connects with a consumer audience. 

 **Become the #1 collaboration tool**: Effectively popularizing GitLab beyond technical audiences will help us achieve our BHAG to become the most popular collaboration tool for knowledge workers in any industry.

 **Anchor remote work to our brand**: All-remote is not a value – it’s what enables us to practice our values. Any message we commit to needs to encompass the full spectrum of what work at GitLab is. This is as much about being able to work from anywhere as it is about empowering people to manage themselves, their time, and their space. 

### Tactics – launch a branded editorial site

 The trend in content marketing right now is to take corporate blogs and turn them into digital magazines with a scoped theme. 

 Invision has done this with their [Inside Design blog](https://www.invisionapp.com/inside-design/), Intercom's [Inside Intercom blog](https://www.intercom.com/blog/), and Atlassian recently did this with their blog, [Work Life](https://www.atlassian.com/blog).

 The aim is to build an editorial-first space, with a view to becoming a trusted resource. 

### Blog concept 

 **Work** in Progress.

 Everything is in draft. Anyone and anything can be questioned.

 It’s this attitude that inspired us to go all in on all-remote, and all in on a single application – one iteration at a time. No one else was doing either, but that didn’t stop us.  

 WIP is a state of mind, an approach to work that is threaded throughout GitLab the product and the company. By considering nothing to be set in stone, we’ve been able to do things differently right from the beginning. 

 Work in Progress is a play on WIP status. 

 This applies to:
 - Technical stories (challenges, how-tos) 
 - DevOps transformations
 - Open source developments

 **Progress** in Work.

 Work in Progress can also mean progress in attitudes to work:
 - Results over hours
 - Location agnostic
 - Transparent and iterative

 | **Pros**                                                                         | **Cons**                                     |
 |------------------------------------------------------------------------------|------------------------------------------|
 | Not industry specific – appeals beyond our technical audience                | May be perceived as “under construction” |
 | Ties neatly into technical stories, collaboration & workflow, and all-remote | Slack podcast (2017) of the same name    |
 | Implicit connection to the product                                           |                                          |

 Note: The Slack podcast has not been updated since 2017 and is not particularly high ranking. 
 {: .note}

#### Internal feedback 

 We sought [feedback on the shortlisted concepts from GitLab blog contributors from across our Engineering function](https://docs.google.com/presentation/d/12IydvTCVdM0rt1y3OcHqYpLqtRNzQR4vQT0rs74YhxU/edit#slide=id.g705f523280_2_12).

#### Proposed editorial program outline

##### Mission statement

 Inspiring innovation by trial and error.

##### Editorial themes 

 Iteration, innovation, learning from mistakes

##### Content buckets

 [New blog categories](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/76) – TBC:

 - Innovations
 - Engineering
 - DevOps
 - All-remote
 - Unfiltered
 - Product & News
   - Releases
   - Product news
   - Company news
   
 [Spin-off editorial programs are proposed in the presentation](https://docs.google.com/presentation/d/12IydvTCVdM0rt1y3OcHqYpLqtRNzQR4vQT0rs74YhxU/edit#slide=id.g65734fb722_0_6).
 
# Editorial team processes

## Publishing schedule

 With the exception of time-sensitive announcements and news, we are aiming to have blog posts scheduled two weeks ahead.

 - We will publish 2-3 posts per week, aiming to have a variety of topics covered within the same week.
 - We will stagger publishing, generally avoiding posting on Fridays and in the last few days of the month.
 - We don't consider operational posts (patch release announcements, for example) as conflicting with other publishing, so we may publish a post on the same day that one of those goes live.
 - We aim to ensure that posts that are likely to be popular are live in time to be included in the bi-weekly newsletter that goes out on the 10th and 25th of each month.

## Blog calendar

 The calendar below documents when posts will be published, as well as industry awareness days and anniversaries we may cover. Please bear this in mind when requesting a specific publish date for a post.

 _Please note that all dates are subject to change to accommodate urgent posts and announcements._

 <iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_ame9843c6094ffc475vea9ftn4%40group.calendar.google.com&ctz=America%2FLos_Angeles" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
 
## Labels

  - `blog post`: every blog post idea, proposal, draft, etc. **MUST** have this label
  - `priority`: blog posts that should be worked on immediately (please provide a rationale in your issue description)
  - `Blog::Pitch`: blog post ideas that are waiting for triage
  - `Blog::Planning/in progress`:  accepted pitches and blog posts that are assigned to a member of the Content Marketing/Editorial team
  - `Blog::Review`: blog posts that are ready for an editorial review
  - `Blog::Freeze`: blog post is being reviewed by the Editorial team. No additional changes should be made until the label is lifted.
  - `Blog::Waiting for author`: blog posts that have been reviewed by the Editorial team and assigned back to the author to address feedback or approve for scheduling
  - `Blog::Scheduled`: Blog posts that have completed reviews and are scheduled for publishing
  - `Guest/Partner post`: blog posts that are authored and submitted from a contributor outside of GitLab
  - `customer story`: blog posts that include a customer interview
  - `CEO interview`: blog posts submitted by the GitLab CEO and require their subject matter expertise
  - `Content hack day`: blog posts that have been accepted from the Content Hack Day project.

## Reviewing blog post pitches

 Every Monday, a member of the Editorial team reviews [blog post pitches](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues?label_name=Blog%3A%3APitch). There is a recurring event on the [Editorial calendar](/handbook/marketing/corporate-marketing/content/#editorial-calendar) for every Monday. The Editorial team member reviewing pitches that week is invited to the event. 

### What to look for

- Has another member of the Editorial team already reviewed the pitch and responded? If so, no need to review.
- Should the pitch be an [announcement request](/handbook/marketing/corporate-marketing/#requests-for-announcements) instead?
- If the post is a tutorial explaining how to do something with GitLab, does the relevant documentation exist? This needs to be in place before we can publish a blog post about it (see note in [the blog handbook](/handbook/marketing/blog/#how-to-pitch-a-blog-post)). 
- Does the pitch meet the requirements and answer the questions outlined in [How to get your pitch accepted](/handbook/marketing/blog/#how-to-get-your-blog-post-pitch-accepted)
  - What's in it for the audience?
- Does the pitch show potential to help us meet our [goals for the blog](/handbook/marketing/blog/#goal-grow-our-audience-engage-readers-and-convert-readers-into-subscribers)?
- Have we published anything on an overlapping topic recently? 
  - Does this pitch offer something new?
- Could we tweak the angle of the post to better align with our requirements and goals?
- How have similar posts performed in the past? 
  - If there are recent examples of similar topics or angles, pull the number of unique views from [Google Analytics](https://analytics.google.com/analytics/web/#/report/content-pages/a37019925w65271535p67064032/). A two-month window from date of publishing usually gives a good idea of whether something did well with our audience. 
- Is the pitch better suited to another channel? Sometimes the information is more appropriate for sharing in:
  - **The handbook** – This is great for evergreen content, documenting how we do things at GitLab or why we do things a certain way. Blog posts generally should have an element of "newness" or news to them. If the topic of a pitched blog post won't age, or the audience is primarily internal, the handbook is a better option. Because our handbook is public, pages from it can be shared in the same way as a blog post. 
    - Example: This page on [Why GitLab uses the term all-remote to describe its 100% remote workforce](/company/culture/all-remote/terminology/) was originally suggested as a blog post, but works better as a handbook page, because the content is evergreen and can be built on over time.
  - **The [Unfiltered blog](/handbook/marketing/blog/unfiltered/)** – This is a good choice for blog posts that are more personal in nature. Think of the Unfiltered blog like a peer-to-peer publishing platform. This can lend greater credibility to personal stories because they haven't gone through Marketing, and it relieves the burden of having to align with the blog strategy. The Unfiltered blog is also a good home for posts that may appeal to a smaller, more niche audience (including internal) than the branded blog. We have seen plenty of Unfiltered blog posts succeed because they resonate with readers, regardless of where they were published.
    - Example: [Contribute through the eyes of a new GitLabber](/blog/2020/02/25/contribute-through-the-eyes-of-a-new-gitlabber/) is a personal account of one team member's experience of their first Contribute, and the intended audience is primarily internal.
  - **The writer's personal blog** – Similar to the Unfiltered blog, this makes sense for personal stories that don't lend themselves to the branded blog.
    - Example: [First time all remote: My first week at GitLab](https://dnsmichi.at/2020/03/09/first-time-all-remote-my-first-week-at-gitlab/)
   
### Responding to pitches 

Following the review above, the pitch should fall into one of three categories:

- Aligns with blog goals and strategy: Approved and `Blog::Planning/in progress` label applied
- Needs adjusted angle/focus: Approved with recommendations, `Blog::Planning/in progress` label applied
- Better suited to another channel: Alternative suggested

When responding to the person who pitched, be sure to include your rationale for your response and any relevant data or examples to justify your decision.

## Checklist for blog post issues 

If you are planning to write or even just considering writing a blog post: 

- Start by opening an issue in the [gitlab.com/gitlab-com/www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/issues) project, using the `Blog post` template 
- Under `Proposal`, describe your proposed blog post topic
- Include links to any relevant background information or resources in the issue description
- Review and check off any relevant checklist items in the blog post issue template
- Make sure the appropriate [labels](/handbook/marketing/blog#labels) are applied
- If you are definitely going to write the blog post, choose a publish date that is available on the [Editorial calendar](/handbook/marketing/growth-marketing/content/editorial-team/#blog-calendar) and include that date in your issue title
- Give the issue a due date a minimum of two working days before your chosen publish date 
- Apply the appropriate milestone to the issue (e.g. `April blogs 2020`)
- Assign the issue to yourself
- [Add an item to the Editorial calendar](/handbook/marketing/corporate-marketing/content/#adding-an-event-to-the-editorial-calendar) for your blog post on your chosen publish date, using the appropriate emoji

### If you reschedule a blog post

Please ensure that you update: 
- Issue title
- Due date
- Milestone (if applicable) 
- Calendar entry

If the post is postponed and you don't yet have a specific reschedule date, please remove:
- Date in the title (replace with TBD)
- Due date 
- Milestone
- Calendar entry

## Choosing featured posts

Each week, one blog post is selected as the [featured post](/handbook/marketing/blog/#featured) on the blog homepage. 

To decide on the featured post for a given week, the managing editor will open an issue in the [Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/), listing the blog posts scheduled to go live in the coming week.

Editorial team members will nominate a post to be featured, based on the following criteria:

- Past performance of similar topics
- Supporting a key activity or campaign
- High-profile company/product announcement

When the featured post has been agreed on:

1. Update the `weekly featured post` issue description
1. Leave a comment to confirm in the issue for the featured post (or MR, if one exists already)
1. Link the blog post issue to the `weekly featured post` issue where this was decided. 
1. Close the `weekly featured post` issue. 

## Blog style guide

We use American English by default.

### Acronyms

When using [acryonyms or initialisms](https://www.dailywritingtips.com/acronym-vs-initialism/),
ensure that your first mention uses the full term and includes the shortened
version in parentheses directly afterwards. From then on you
may use the acronym or initialism instead.

> Example: A Contributor License Agreement (CLA) is the industry standard for open
source contributions to other projects.

Below are some common acronyms and initialisms we use which don't need to be
defined first (but use sparingly, see [Tone of voice](/handbook/marketing/corporate-marketing/#tone-of-voice) above):

- AFAIK - as far as I know
- ICYMI - in case you missed it (for social only)
- IIRC - if I recall correctly
- IRL - in real life
- TL;DR - too long; didn't read

### Ampersands

Use ampersands only where they are part of a company's name or the title of a
publication or similar. Example: Barnes & Noble

Do not use as a substitute for "and."

### Capitalization

See [below](#word-list) for styling of specific words.

#### Case

Use [sentence case](https://www.thoughtco.com/sentence-case-titles-1691944) for
all titles and headings.

#### Feature names

All GitLab feature names must be capitalized. If referring to a GitLab feature as part of a workflow rather than speaking about the feature itself, use lower case.

> Examples: "GitLab Issue Boards are a powerful project management and collaboration tool." vs "The editorial team uses an issue board to track the progress of blog posts."

#### Job titles

We do not capitalize job titles, regardless of whether they appear before or after a person's name.

#### GitLab functions/departments/teams

These are elements that make up GitLab the company's [organizational structure](/company/team/structure/#organizational-chart). Capitalize the name of the element, but not the word after:

> Example: Engineering function, Security department

#### Brand and publication names

Ensure you style brand names consistently with how the company does.

> Example: WiFi Tribe, DigitalOcean

The only exception to this is for brand names that are in all upper or lower case. Always capitalize the first letter, regardless of how it is styled in a company's logo.

> Examples: Reddit, Lego

If the word "the" forms part of a brand or publication's name, capitalize it:

> Examples: The Wall Street Journal, The Times

You can drop the "The" entirely if used as follows:

> "We spoke to a Wall Street Journal reporter"

### Contractions

We favor contractions ("can't," "didn't," "it's," "we're") to sound more human and
less formal.

### Dates

#### Months

Spell out, unless using the full date (see below).

#### Specific dates

Jan. 3, 2019 (abbreviate month, no `rd` after 3)

### Headlines

Please see [headline advice](/handbook/marketing/blog/#title) in the blog handbook.

### Lists

Use `*` or `-` to create a bulleted list in Markdown. No period is necessary at the end of each bullet point.

### Numbers

Numbers with four or more digits should include a comma.

> Examples: 2,000; 100,000

#### In body copy

Spell out one to nine. Use numerals for 10 upwards. Try to avoid beginning a
sentence with a number, but if unavoidable, spell it out.

#### In headings/subheadings

Use numerals. If at the beginning of a heading, capitalize the first word following.

> Example: 3 Strategies for implementing a microservices architecture

### Pronouns

Unless referring to someone in particular, use gender-neutral pronouns ("they", "them").

### Punctuation

Only one space after a period is necessary.

Include one space after ellipses (... )

See [below](#word-list) for when to hyphenate specific words.

We use en dashes (–) rather than em dashes (—). Include a space before and after the dash.

Use % instead of "percent" at all times.

### Quotes

#### Quotation marks

Use double quotation marks for direct quotes, and single quotation marks for a quote within a quote. Single quotation marks may also be used for specialist terms or sayings.

- Include periods and commas inside ending quotation marks.
- Leave semi-colons, colons, and dashes outside of the quotation marks.
- Include other punctuation based on the sentence meaning.

Examples: 
> Recently, an article was published stating, "Software is eating the world."

> What do you think of the claim, "Software is eating the world"?

> "Do you agree that software is eating the world?" wrote the author.


#### Tense

When including direct quotations from interviewees in blog posts, we prefer to use the feature journalism style of present tense for verbs such as "said," "explained" etc.

> Example: "Ruby was optimized for the developer, not for running it in production," says Sid.

#### Referring to interviewees

For blog posts, prefer referring to interviewees by their first names as this is less formal and more in keeping with our [tone of voice](/handbook/marketing/corporate-marketing/#tone-of-voice).

### Voice

Active voice is preferred to passive voice in blog posts. *Voice* describes whether the subject of a sentence receives or performs the action of a verb.
> Example: "The GitLab community submitted 1 million merge requests in March 2019." (active) vs. "One million merge requests were submitted by the GitLab community in March 2019." (passive)

### Word choice

When in doubt, use the "future" styling of a word. So, "internet" is not capitalized,
"startup" is not hyphenated, etc.

### Word list

How to spell and style commonly used words.

- Agile
   - A is capitalized when referring to [Agile methodology](https://en.wikipedia.org/wiki/Agile_software_development)
- all remote
    - We refer to GitLab as an all-remote company (not remote friendly, remote first, or remote only)
    - hyphenated only when appearing before a noun ("GitLab is an all-remote organization"/"GitLab is all remote")
- Board
    - when in reference to the GitLab Board or Directors or our Board members, Board is capitalized
    - a board in general does not need to be capitalized 
- cloud native
    - not capitalized, and no hyphen, regardless of how it is used
- co-founder
    - hyphenated, not capitalized
- continuous delivery, deployment, integration
    - not capitalized
- developer
    - use the abbreviation "dev" sparingly; do not capitalize
- DevOps
    - D and O always capitalized
- E-Group
    - references to E-Group should alays include a capital E, hyphen, and a capital G
- Git
    - always capitalized
- GitHub
- GitLab
    - G and L are always capitalized, even in GitLab.com
- internet
    - not capitalized
- Kubernetes
    - always capitalized, never abbreviated to K8s (except on social)
- multicloud
    - one word, no hyphen, lowercase m, lowercase c
- open source
    - no hyphen, regardless of how it is used
- operations
    - use "ops" sparingly; do not capitalize
- set up/setup
    - two words for the verb, one for the noun ("How to set up a Kubernetes cluster"/"Let's walk through the Kubernetes cluster setup process")
- sign up/signup
    - two words for the verb, one for the noun ("Sign up for a GitLab.com account"/"Upon signup, you will be sent a confirmation email")
- startup
    - no hyphen
- web
    - not capitalized

### Appendix B: UK vs. American English

Use American spelling in your communications. Please consult [this list of
spelling differences](https://en.oxforddictionaries.com/spelling/british-and-spelling).

## SEO guidelines

Each blog post should be optimized for search engines. 

A primary keyword is a word/phrase that is repeated a few times in the blog post/headline (see below) and will help a search engine make our content "findable." A secondary keyword reinforces the first keyword by giving someone searching for content on the internet another way to find what they're looking for. In the spirit of MVC, let's focus on just making sure each blog post has a primary keyword (but you'll get bonus points if you can work a secondary keyword in!) A primary keyword might be "DevSecOps" while a secondary keyword might be "DevOps security tools" or "secure software development." Please note that while "keyword" is the commonly used term that does not mean it has to be a single word; it can certainly be a short phrase (also known as a long-tail keyword.)

We are in the process of switching SEO tools so if you need help please reach out to a member of the editorial team for guidance. In general, though, choose a keyword and remember that often a long-tail version might work just as well and have less competition from other sources (example: DevSecOps vs. DevOps security tools). 

**In an ideal world, your keyword or keyword phrase should be in the URL, the headline and the first paragraph.** 

If some or all of that is not possible, alt-text for a photo, a caption or the tweet that appears on the bottom of the blog post are great ways to "sneak in" keywords.

Here is another more DIY SEO option.

1. Start with the main topic of the post (example: DevSecOps)
1. Visit [Google Trends](https://trends.google.com), enter your term and make sure to choose "worldwide." You can also enter other terms to compare it to (example: software development security). Scroll down and you'll see how popular your keyword is and you'll also be offered some alternate suggestions.
1. Consider your keyword in context. The term "DevOps" will be popular but that also means there will be a lot of competition for it. But a long-tail keyword like "DevOps security tools" will have less competition.
1. Once you've settled on a keyword it would be ideal, but not mandatory, to have it in the URL, headline and in the first paragraph.

Finally, digital marketing is putting together a searchable database of relevant keywords for GitLab that will be updated monthly. When that is ready a link will be available here.

## Editorial review checklist 

### Style and language

- Does the post adhere to our style guide?
- Is it written in [American English](#appendix-b-uk-vs-american-english)?
- Are all titles and headlines in [sentence case](#case)?

### SEO 

- Does the post adhere to our [SEO guidelines](#seo-guidelines)?
- Have related links been included? 
  - If there are relevant blog posts on the same topic, please include some as suggested reading at the bottom of the blog post. You can [see an example here](/blog/2020/03/27/gitlab-ci-on-google-kubernetes-engine/#other-resources).
  - It's also good to also include related links throughout the post, within the text, where appropriate. 

### Formatting

- Is the filename all in lowercase, with no special characters or numbers other than the date?
- The filename will become the blog post URL when it is published. Does it make sense as the URL? Does it include the primary keyword for SEO?
- Does the filename reflect the publish date accurately? (final review only)
- Has all the relevant [frontmatter](/handbook/marketing/blog/#frontmatter) been included correctly?
- Has the correct [category](/handbook/marketing/blog/#categories) been entered? If not, the build will fail.
- Has `gitlab` been added to the `author_twitter` field if the author doesn't wish to use their own handle?
- Does the `Description` field copy fit on the tile on [/blog](/blog)?
- Is it appropriate to include a [trial CTA](/handbook/marketing/blog/#ee-trial-banner) on this post? Generally, if a post falls under `culture`, `engineering`, or `open source` it is best to remove it.
- Has the appropriate [merchandizing](/handbook/marketing/blog/#merch-banner-and-sidebar) been included and are the sidebar and banner rendering correctly?
- Are all [images formatted correctly](/handbook/marketing/blog#inline-images) and are they <1MB?
- Are any image filenames all in lower case, with no special characters or spaces?
- Does any of the images need a [caption](/handbook/marketing/blog#image-captions)?
- Check the file extension `.html.md.erb` and that the [newsletter sign-up form is included in the post](/handbook/marketing/blog/#newsletter-sign-up-form), if appropriate. Don't include it if there are other CTAs in the post (for example, directing people to sign up for a webcast or watch a video).
- Is there an [image credit](/handbook/marketing/blog#image-attribution), if needed?
- Does the click-to-tweet copy make sense? Could it include any other handles or hashtags? [Edit it](/handbook/marketing/blog/#twitter-text) if necessary.
- Do all links work?
- Are all about.gitlab.com [links relative](/handbook/markdown-guide/#relative-links)?
- [Wherever possible, does the text for links make sense out of context?](/handbook/markdown-guide/#links) Would someone only seeing that text know what to expect from the link if they click on it?

## Checklist for merging blog posts

When you are ready to merge a scheduled blog post, check the review app for the blog post:

![Link to review app](/images/handbook/marketing/link-to-review-app.png){: .shadow.medium.center}

- Is the date on the blog post correct?
- Does the filename (which will become the URL for the post) correspond with the headline?
- Should this blog post be [featured](#choosing-featured-posts)? Add or remove `featured: yes` from the frontmatter accordingly.
- Are all images and formatting rendering as expected?
- Has the blog post been reviewed by any other necessary parties (as indicated in issue/MR description)?
- Is the post likely to get a lot of attention/engagement/traffic? Is it about a hot-button topic like compensation or big industry news? Please give the community advocates a heads up in #community-advocates on Slack before you publish.

When you've checked all these, you can tick the boxes for `Delete source branch` and `Squash commits` but this isn't strictly necessary.

Go ahead and hit `Start merge train` or `Add to merge train`. If the blog post does not have to be live ASAP, feel free to join the train. Otherwise you can just choose "Merge immediately" from the dropdown menu to the right.

 ![Merge train options](/images/handbook/marketing/merge-train.png){: .shadow.medium.center}

You can find [more information about merge trains here](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/#immediately-merge-a-merge-request-with-a-merge-train).

Once you've started or joined a merge train, go to the [Pipelines page](https://gitlab.com/gitlab-com/www-gitlab-com/pipelines), which you can find in the menu on the left under CI/CD.

![Shortcut to pipelines page](/images/handbook/marketing/shortcut-to-pipelines-page.png){: .shadow.small.center}

Merging a blog post requires two pipelines to pass. Find your first merge pipeline, which will probably be near the top of the page. It will say `Merge branch 'your-branch-name' into 'master'`.

![Merge pipeline](/images/handbook/marketing/merge-pipeline.png){: .shadow.large.center}

You can watch its progress there. Depending on your notification settings, you may get an email when your pipeline passes, or if it fails.

If it passes, a final pipeline will kick off:

![Merge pipeline](/images/handbook/marketing/final-pipeline.png){: .shadow.large.center}

When this passes, proceed to the next step.

### When your pipeline passes

Go to the [blog homepage](/blog) and your post should be visible there. Sometimes this takes a few minutes. When you see it, grab the link and share it in the #content-updates channel in Slack.

### What happens if my pipeline fails?

If a pipeline fails when you try to merge something, it is usually not something you have done wrong! You can retry it, but if it still doesn't work, it's probably quickest to get an answer by sharing your MR link in #mr-buddies or #questions on Slack.

## MR buddies

If you run into issues with your MR, pipeline, or Terminal, it's usually quickest to ask for help in the #mr-buddies, #git-help, or #questions channel on Slack (be sure to include a link to your MR!). If you don't get an answer that way, you can reach out to a [Merge Request Buddy](/handbook/people-group/general-onboarding/mr-buddies/). The following team members have volunteered to be called on for assistance. You can also search for "Merge Request Buddy" on the [team page](/company/team).

- Charlie Ablett
- Mayra Cabrera
- Phil Calder
- William Chia
- John Coghlan
- Mario de la Ossa
- Jean du Plessis
- Dan Gordon
- Kenny Johnston
- Markus Koller
- Lyle Kozloff
- Jason Yavorska
- Cynthia Ng
- Brendan O’Leary
- Axil Pipinellis
- Emilie Schario
- Abubakar Siddiq Ango
- Ronald van Zon
- Thomas Woodham


## Featuring Unfiltered posts

The Editorial team will monitor the GitLab Unfiltered blog for posts that are suitable for featuring on the branded blog on a weekly basis.

### Process for reviewing and featuring Unfiltered posts

Every Monday, a member of the Editorial team will read new Unfiltered posts published during the previous week and select any posts that are likely to perform well on the branded blog for a full editorial review.

#### Criteria for a featured post

- Post has >500 sessions already, according to the [Content Marketing dashboard](https://datastudio.google.com/open/1NIxCW309H19eLqc4rz0S8WqcmqPMK4Qb), and/or
- Post has the [attributes of an historically successful blog post](/handbook/marketing/blog/#attributes-of-a-successful-blog-post)

It's an extra bonus if your post links to related posts either on the main blog or to other posts on Unfiltered. Cross-linking helps the reader and boosts your search engine optimization, meaning more people will see what you've written!

This is not an exhaustive list of criteria, as the Editorial team member will also use their best judgment regarding what tends to perform well on the branded blog.

#### Review process

##### Issue

If an Unfiltered blog post has been identified as a good candidate for featuring, the Editorial team member should open an issue in the [gitlab.com/gitlab-com/www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues) with the title "Feature Unfiltered post: [title of Unfiltered post]", using the `feature-unfiltered-post` issue template.

Include a link to the original post in the issue description, and @ mention the author of the post so that they are aware it is being reviewed for featuring on the branded blog.

##### Merge request review

Once the issue is open, the Editorial team member can create an MR for the full editorial review of the post, in the same way as net new posts that are submitted to the branded blog for review.

**Checklist for reviewer:**

- Add the following statement to the top of the blog post file:

```
This blog post was originally published on the [GitLab Unfiltered blog](/blog/categories/unfiltered/). It was reviewed and republished on yyyy-mm-dd.
{: .alert .alert-info .note}
```
- Remove Unfiltered [disclaimers](/handbook/marketing/blog/unfiltered/#disclaimers) from top and bottom of blog post
-  Change [category](/handbook/marketing/blog/#categories) from `unfiltered` to another
- Add links to related blog posts, where relevant
- Keep filename the same. Do not update to the current date (this will change the URL for the blog post and result in a 404 for existing links to the original post)
- Assign the MR to the post's original author for review of any changes

When the author has reviewed and all outstanding threads resolved, they should reassign the MR to the Editorial team member to merge the changes.

##### Merging

Featured Unfiltered posts can be merged straight away, and don't need to be scheduled like brand new posts. When the edited post is live, the Editorial team member who merged should share a link in the #content-updates channel on Slack so the social team is aware that the post is now on the branded blog and can be shared on GitLab social channels.

The team will aim to have selected posts featured within a week of creating the issue. This may change depending on the availability of the post's author to review changes.
