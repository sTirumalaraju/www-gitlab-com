---
layout: handbook-page-toc
title: "Strategic Marketing Metrics"
---



## North Star Metrics

Every marketing function should have at least one north star metric that they aspire to improve over time to help GitLab's business growth.
Below is a list of metrics by different marketing functions in Strategic Marketing.

| Team | Primary Metric | Secondary Metric | Eventual Metric Goal |
|---- |----------|---------|---------|
| PMM | Impressions | Content Created (pages, docs, talks, blogs, etc) | IACV Attribution |
| TMM | Impressions | Content Created (demos, videos,  etc) | IACV Attribution |
| AR  | Impressions | Analsyt Coverage (reports, mentions, MQs, Waves, etc.) | IACV Attribution |
| Cust Ref | Impressions | Content Created (Case Studies, Quotes, References, etc) | IACV Attribution |
| CI | Impressions | Content Created (Comparison Pages,  etc) | IACV Attribution |



## Content Produced

* [Strategic Marketing Content Inventory Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/3353)
* [MVC1 on Google Sheets](https://docs.google.com/spreadsheets/d/1W5oAlbPV610-ylM7LWv_zc6bEqQK9dI0H9Hrn2f6Jwc/edit#gid=0)


## Web Traffic Analysis

* [Website and handbook pages](https://datastudio.google.com/u/0/reporting/1jhpxOcfWp9B44smdc6Uv-tGMNyWoludX/page/JKsTB)
* [YouTube Metrics](https://studio.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5AC)

* Learning Links:
    * [Shane Rice - youtube video on how to build such a dashboard](https://youtu.be/tNOD5qrH6Ao)
    * [Google Analytics for Beginners](https://analytics.google.com/analytics/academy/course/6)


## Marketing Attribution Model

* [Marketing attribution](https://app.periscopedata.com/app/gitlab/556414/Marketing-Linear-Attribution)
* Learning: [Marketing attribution - quick overview](https://www.bizible.com/blog/marketing-attribution-models-complete-list)


## Customer Reference Analytics

Current metric - number of cases published per month - current target - 2 / month
   * Metric not most efficient or in control of customer reference team
      * Number of case studies published not always in control - example, several “completed” case studies stuck in pipeline for reasons such as - hold at pre-publish stage for PR purposes (example - EMEA), still to be approved by customer legal or marketing department
      * Does not reflect true value to business.  Examples:
         * References help obtain results in important AR reports - Gartner MQs or Forrester Waves
         * References help close opportunities (sales references)
         * References help generate pipeline (example - customer speakers for webinars or conferences)

Need additional metrics to measure value of customer reference activities and value delivered to the business.  Additional metrics to consider:
   * [Customer Reference Edge Program](https://gitlab.my.salesforce.com/01Z4M000000slL1)
   * [Monthly Marketing Key Metrics Slides](https://docs.google.com/presentation/d/1BGcMMqNfayuGsJVjqFCOXPrt5VK8S-KwHQiGr3d5KII/edit)
   * [Case Studies Tracking Board](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=%E2%9C%93&state=opened)

## Competitive Intelligence Analytics
* Ideas to consider:
    1. Track usage of DevOps Landscape comparisons page
       * Page visits
       * Number of sessions
       * New visitors
       * Return visitors
       * Avg. Time on page
       * Bounce Rate
       * Click Through Rate
       * Conversion Rate   
    1. Track usage of competitive resources page
       * Number of persons accessing
       * Number of inputs/comments
       * Number of inbound requests
       * Competitive Intelligence Material attribution to sales
    1. Impact of competitive resources to sales close - Deal Support and ROI Analysis
       * Number of deal specific enquiries responded to
       * Opportunity value of deal specific enquiries
       * Number of ROI analysis
       * Opportunity value of ROI analysis conducted
       * Opportunity value of deals won that included ROI analysis
    1. Outbound (Blogs, videos):
        * Visits (bounce rate, time on page)
        * Engagement (low threshold CTAs clicked)
        * Leads (clicked on a register/signup)
    1. Campaign (gated asset)
        * Visits (to landing page) - (isn’t this also a valid measure, yes)
        * Leads
    1. Internal (tied to sales ops)
        * Opportunities influenced (decks, showing up, ROI, cust ref, etc)
        * Internal views/downloads (enablement)


## Analyst References Analytics

What do we measure today?

* [Baseline survey data at FY2021 SKO on sales usage of analyst assets](https://docs.google.com/forms/d/10A8ConBSuaxj7D-91jV0PNFMKIIWzu3eq9w1TtFY9co/edit#responses)
* Gartner reprint [info](https://drive.google.com/file/d/1LVaBSFwOwHZ9KJ05TyzB3NHG_Ct0Q68x/view?usp=sharing) slides, and [historical](https://drive.google.com/file/d/14DkA5Ab8E6ZpGgeK6YeMU2FVAzMiljc6/view?usp=sharing) (views, etc.)
Forrester reprint info (views, d/l, dwell)
* [Customer Reference metrics](https://docs.google.com/presentation/d/1ajOzG91G3hxlqWo8uOuVUR8wrslTRVA8-0lFzjYXhZ0/edit?usp=sharing) - slide 2 is an example of current metrics (pool and attributes, requests and attributes, starting to measure some revenue attribution/impact, etc)


* Ideas to consider:
    * Analyst Newsletter metrics
    * Number of briefings and inquiries led
    * Coverage heat map - overall
    * Coverage heat map - changes over time
    * Analyst Relations web traffic
    * Impact on sales (pipeline, conversion, win/loss) - SFDC touch points
    * TEI Metrics
    * Satisfaction - a regular internal and external surveying/measurement metric



## Examples to consider

* [ChangeLog](https://about.gitlab.com/handbook/CHANGELOG.html)
* Surveys (Sales team)
* Analytics on issues managed - open/closed, internal vs. external
* Pathfactory
* SM Issues Analysis
* SM Budget Spend analysis
* CAB Data (to consider)
    * x% that have case studies
    * y% are active in sales
    * z% are speaking in events (gitlab, industry, etc) for/with GitLab
    * a% are active in analyst reference calls
    * b% provide messaging, positioning, and persona feedback
    * c% “decline” for our requests?
    * STRETCH Goal:  Can we analyze if we have been able to sell more into these organizations since they joined the CAB




## CMO (Todd) Challenge on how to measure

* View of output metrics:
    1. Views/reads - Google Analytics dashboard is great because I can set goals.  Some suggested fixes:
        * Tagging youtube and blog content so you can filter
        * Changing Use case pages to public website pages
    1. Linear Attribution of content in programs and campaigns
    1. Value of Strategic Marketing to other teams
        * Impact to sales (Survey?)
        * Rest of marketing (Survey?)
* Other metrics to consider:
    * Website first (MRs?)
    * Metrics on views, etc.
    * Maybe update BOM colors/links at every stand up

## Strategic Marketing (Ashish) thoughts on metrics we should consider:

Many levels / lenses of “operational metrics” needed to build the narrative such as:

1. WHAT do we produce?
    * Example: 5 WPs, 16 Demos, 2 customer case studies
    * Assign to right category (Outbound, Campaign, Internal)
1.  Content created vs. curated
1. List of activation methods
1. WHERE does it show up? What are the various ways we activate content?
    * Example: Website, SFDC, Email campaigns, Event booths, Campaign design, Conferences, enablement sessions, 
1. How MANY people see (view) it? Frequency
    * Example: 3000 website visitors per quarter, 45 sales people/month, 25,000 prospects for demo x in 12 events in a quarter, Handbook views, How many sales/channel got enabled?
    * Why: Indicates overall activity for a given asset in a given time frame.  It is an indicator about the effectiveness of the Title, description, landing page, and campaigns related to the asset.  It does not tell us ANYTHING about the quality or effectiveness of the internal content.  What will it tell us: The degree to which a given  asset is active or not.
1. What HAPPENS with the content? 
    * Linear attribution of content in programs?
    * How many converted?
    * How many MQLs did the asset create?
        * Why - Why: Indicates how an asset is contributing to MQL.  This indicates the frequency that a given asset is contributing to overall marketing qualification of leads.  MQL What it will tell us: <CHALLENGE> This metric probably does not provide any more insight than Asset Views.  MQL simply is a cumulative set of activity by a given lead/contact to reach MQL threshold.  MQL may be  simply a refined view of Views.
    * How many SAOs did the asset create?
    * How many opportunities did the asset help create?
    * How much $ (ARR) did the asset help create?
        * Why: Indicates that a given asset supported in revenue.  We might be able to imply something about the quality of asset content, because end users have consumed the asset and then continued the buying process, however will require more analysis of other data points.  What it will tell us: This will help us understand which assets are most connected with generating revenue and should therefore be promoted and re-used.

Challenge:  Why do we measure?  What are our next steps / actions based on what we learn?

Other considerations:
    * Program Budget spend analytics

## Useful links

* [Marketing KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#marketing-kpis)
* Slack Channel - #keymonthlymarketingmetric
* [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0)
    * [Pathfactory](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/)
