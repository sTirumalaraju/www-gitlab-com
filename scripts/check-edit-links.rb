#!/usr/bin/env ruby

require_relative '../lib/lint/check_edit_links'
script_instance = Lint::CheckEditLinks.new
script_instance.process
