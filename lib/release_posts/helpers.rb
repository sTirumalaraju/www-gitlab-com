# frozen_string_literal: true

module ReleasePosts
  module Helpers
    Abort = Class.new(StandardError)
    Done = Class.new(StandardError)

    MAX_FILENAME_LENGTH = 99 # GNU tar has a 99 character limit

    def capture_stdout(cmd)
      IO.popen(cmd, &:read)
    end

    def fail_with(message)
      raise Abort, "\e[31merror\e[0m #{message}"
    end

    def git_add(src)
      capture_stdout(['git', 'add', src])
    end

    def git_commit(message)
      capture_stdout(['git', 'commit', '--message', message])
    end

    def git_mv(src, dst)
      capture_stdout(['git', 'mv', src, dst])
      $stdout.puts "\e[34mmoved\e[0m #{src} to #{dst}"
    end

    def replace(old, new, file)
      expr = "%s##{old}##{new}#|x"
      $stdout.puts "\e[34mchanged\e[0m #{old} path to #{new} in #{file}"
      capture_stdout(['ex', '-sc', expr, file])
    end
  end
end
