# https://docs.gitlab.com/ee/user/project/code_owners.html
# Each CODEOWNERS entry requires at least 2 or more individuals OR at least 1 group with access to the www-gitlab-com repository.
# The order in which the paths are defined is significant: the last pattern that matches a given path will be used to find the code owners.

.gitlab/CODEOWNERS @jeanduplessis @vyaklushin @cwoolley-gitlab @sytses

# NB Do not move this entry from the top of this file
/sites/handbook/source/handbook/ @sytses @streas

# Alliances
/sites/handbook/source/handbook/alliances/ @bjung @mayanktahil

# Acquisitions
/sites/handbook/source/handbook/acquisitions/ @eliran.mesika @sfwgitlab
/sites/handbook/source/handbook/acquisitions/acquisition-process/ @eliran.mesika @sfwgitlab

# Active Tests
/source/pricing/ @shanerice @dmor @tbarr

# Blog
/source/blog/ @rebecca @laurenbarker
/source/blog/blog-posts/ @rebecca @erica @vsilverthorne @skassabian

# Business Operations
/sites/handbook/source/handbook/business-ops/ @bryanwise
/sites/handbook/source/handbook/business-ops/data-team/how-we-work/ @gitlab-data
/sites/handbook/source/handbook/business-ops/data-team/how-we-work/calendar/ @gitlab-data
/sites/handbook/source/handbook/business-ops/data-team/how-we-work/duties/ @gitlab-data
/sites/handbook/source/handbook/business-ops/data-team/organization/ @jjstark @kathleentam @rparker2
/sites/handbook/source/handbook/business-ops/data-team/platform/ @gitlab-data/gitlab-data-engineers @kathleentam
/sites/handbook/source/handbook/business-ops/data-team/platform/infrastructure/ @gitlab-data/gitlab-data-engineers
/sites/handbook/source/handbook/business-ops/data-team/platform/periscope/ @kathleentam @jjstark
/sites/handbook/source/handbook/business-ops/data-team/platform/permifrost/ @jjstark @tayloramurphy @msendal
/sites/handbook/source/handbook/business-ops/data-team/platform/python-style-guide/ @jjstark @tayloramurphy
/sites/handbook/source/handbook/business-ops/data-team/platform/snowplow/ @jjstark @tayloramurphy @mpeychet
/sites/handbook/source/handbook/business-ops/data-team/platform/dbt-guide/ @jjstark @tayloramurphy @mpeychet
/sites/handbook/source/handbook/business-ops/data-team/platform/sql-style-guide/ @gitlab-data
/sites/handbook/source/handbook/business-ops/data-team/data-quality/ @gitlab-data
/sites/handbook/source/handbook/business-ops/data-team/data-quality-process/ @kathleentam @derekatwood @jjstark @tayloramurphy
/sites/handbook/source/handbook/business-ops/data-team/documentation/ @jjstark @tayloramurphy @kathleentam
/sites/handbook/source/handbook/business-ops/data-team/learning-library/ @gitlab-data
/sites/handbook/source/handbook/business-ops/data-team/kpi-index/ @kathleentam @jjstark
/sites/handbook/source/handbook/business-ops/data-team/programs/data-for-product-managers/ @mpeychet @kathleentam
/sites/handbook/source/handbook/business-ops/employee-enablement/ @pkaldis
/sites/handbook/source/handbook/business-ops/enterprise-applications/ @bryanwise @ccnelson

# CEO and Chief of Staff Team
/sites/handbook/source/handbook/ceo/ @sytses @cheriholmes
/sites/handbook/source/handbook/ceo/chief-of-staff-team/ @streas
/sites/handbook/source/handbook/ceo/chief-of-staff-team/readmes/emilie/ @emilie
/sites/handbook/source/handbook/leadership/ @sytses
/sites/handbook/source/handbook/communication/ @streas @rappleby
/source/company/ @sytses @streas
/source/company/strategy/ @sytses
/source/company/stewardship/ @sytses
/source/company/okrs/ @streas
/source/company/history/ @sytses
/sites/handbook/source/handbook/ceo/shadow/ @cheriholmes @sytses

# Compensation
/data/contract_factors.yml @brittanyr @mwilkins
/data/currency_conversions.yml @brittanyr @mwilkins
/data/location_factors.yml @brittanyr @mwilkins
/data/role_levels.yml @brittanyr @mwilkins
/data/job_families.yml @brittanyr @mwilkins
/data/entity_mapper.yml @brittanyr @lienvdsteen @Vatalidis
/sites/handbook/source/handbook/total-rewards/ @brittanyr @mwilkins
/sites/handbook/source/handbook/total-rewards/compensation/ @brittanyr @mwilkins
/data/country_payment_information.yml @brittanyr @Vatalidis @jnguyen28
/data/variable_pay_frequency.yml @mwilkins

# Conversion
/source/install/ @dmor @shanerice
/source/free-trial/ @dmor @shanerice
/source/free-trial/self-managed/ @dmor @shanerice
/source/stages-devops-lifecycle/ @dmor @shanerice

# Culture
/source/company/culture/ @dmurph @jessicareeder @bchurch
/source/company/culture/all-remote/ @dmurph @jessicareeder @bchurch
/source/company/culture/contribute/ @emily @Lconway
/source/company/culture/cross-culture-collaboration-guide/ @glucchesi @brittanyr
/source/company/culture/gitlab-101/ @cheriholmes @streas
/source/company/culture/inclusion/ @cwilliams3 @cteskey
/source/company/culture/internal-feedback/ @cteskey @brittanyr @mwilkins

# Customer Success
/sites/handbook/source/handbook/customer-success/ @dsakamoto @jfullam @spatching @klawrence @michael_lutz
/sites/handbook/source/handbook/customer-success/tam/ @dsakamoto @spatching
/sites/handbook/source/handbook/customer-success/demo-systems/ @dsakamoto @jeffersonmartin
/sites/handbook/source/handbook/customer-success/solutions-architects/ @dsakamoto @jfullam
/sites/handbook/source/handbook/customer-success/professional-services-engineering/ @dsakamoto @michael_lutz

# EBA team
/sites/handbook/source/handbook/eba/ @cheriholmes

# Engineering
/data/projects.yml @edjdev
/sites/handbook/source/handbook/engineering/ @edjdev
/sites/handbook/source/handbook/engineering/development/ @clefelhocz1
/sites/handbook/source/handbook/engineering/development/ci-cd/ @darbyfrey @dcroft
/sites/handbook/source/handbook/engineering/development/ci-cd/verify/continuous-integration/ @darbyfrey @cheryl.li @samdbeckham
/sites/handbook/source/handbook/engineering/development/ci-cd/verify/runner/ @darbyfrey @erushton
/sites/handbook/source/handbook/engineering/development/ci-cd/verify/testing/ @darbyfrey @rickywiens @samdbeckham
/sites/handbook/source/handbook/engineering/development/ci-cd/package/ @jhampton
/sites/handbook/source/handbook/engineering/development/ci-cd/release/progressive-delivery/ @csouthard  @nicolewilliams
/sites/handbook/source/handbook/engineering/development/ci-cd/release/release-management/ @nicolewilliams
/sites/handbook/source/handbook/engineering/development/database/ @craig-gomes @abrandl
/sites/handbook/source/handbook/engineering/development/dev/ @timzallmann
/sites/handbook/source/handbook/engineering/development/dev/create-editor-be/ @dsatcher
/sites/handbook/source/handbook/engineering/development/dev/create-knowledge-be/ @dsatcher
/sites/handbook/source/handbook/engineering/development/dev/create-source-code-be/ @m_gill @nick.thomas @kerrizor @marc_shaw @robotmay_gitlab @dskim_gitlab @igor.drozdov @garyh @patrickbajao
/sites/handbook/source/handbook/engineering/development/dev/create-static-site-editor/ @jeanduplessis
/sites/handbook/source/handbook/engineering/development/dev/fe-plan/ @donaldcook @jlear @johnhope
/sites/handbook/source/handbook/engineering/development/dev/manage/ @lmcandrew
/sites/handbook/source/handbook/engineering/development/dev/plan/ @donaldcook @jlear @johnhope
/sites/handbook/source/handbook/engineering/development/dev/plan-certify-be/ @donaldcook @jlear @johnhope
/sites/handbook/source/handbook/engineering/development/dev/plan-portfolio-management-be/ @donaldcook @jlear @johnhope
/sites/handbook/source/handbook/engineering/development/dev/plan-project-management-be/ @donaldcook @jlear @johnhope
/sites/handbook/source/handbook/engineering/development/enablement/ @cdu1
/sites/handbook/source/handbook/engineering/development/enablement/distribution/ @twk3 @mendeni
/sites/handbook/source/handbook/engineering/development/enablement/geo/ @nhxnguyen @fzimmer @cdu1
/sites/handbook/source/handbook/engineering/development/enablement/search/ @changzhengliu
/sites/handbook/source/handbook/engineering/development/enablement/memory/ @craig-gomes
/sites/handbook/source/handbook/engineering/development/enablement/database/ @craig-gomes @abrandl
/sites/handbook/source/handbook/engineering/development/growth/ @bmarnane @gitlab-org/growth/engineering-managers
/sites/handbook/source/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/ @jeromezng @gitlab-org/growth/engineering-managers
/sites/handbook/source/handbook/engineering/development/growth/be-fulfillment/ @jameslopez @gitlab-org/growth/engineering-managers
/sites/handbook/source/handbook/engineering/development/growth/expansion/ @pcalder @gitlab-org/growth/engineering-managers
/sites/handbook/source/handbook/engineering/development/growth/fe-fulfillment/ @chris_baus @gitlab-org/growth/engineering-managers
/sites/handbook/source/handbook/engineering/development/growth/retention/ @pcalder @gitlab-org/growth/engineering-managers
/sites/handbook/source/handbook/engineering/development/ops/ @sgoldstein
/sites/handbook/source/handbook/engineering/development/ops/monitor/apm/ @mnohr @ClemMakesApps
/sites/handbook/source/handbook/engineering/development/ops/monitor/health/ @crystalpoole @ClemMakesApps
/sites/handbook/source/handbook/engineering/development/secure/ @tstadelhofer
/sites/handbook/source/handbook/engineering/development/threat-management/ @whaber @lkerr @plafoucriere @thiagocsf
/sites/handbook/source/handbook/engineering/frontend/ @timzallmann
/sites/handbook/source/handbook/engineering/infrastructure/ @brentnewton @marin
/sites/handbook/source/handbook/engineering/infrastructure/team/scalability/ @rnienaber @marin
/sites/handbook/source/handbook/engineering/infrastructure/team/delivery/ @amyphillips @marin
/sites/handbook/source/handbook/engineering/infrastructure/team/reliability/ @dawsmith @AnthonySandoval @albertoramos @brentnewton
/sites/handbook/source/handbook/engineering/infrastructure/incident-management/ @dawsmith @AnthonySandoval @albertoramos @brentnewton
/sites/handbook/source/handbook/engineering/infrastructure/emergency-change-processes/ @marin @brentnewton
/sites/handbook/source/handbook/engineering/monitoring/ @marin @brentnewton @sloyd
/sites/handbook/source/handbook/engineering/releases/ @marin
/sites/handbook/source/handbook/engineering/infrastructure/production/kubernetes/ @marin
/sites/handbook/source/handbook/engineering/quality/ @meks
/sites/handbook/source/handbook/engineering/quality/guidelines/ @meks @at.ramya @jo_shih @vincywilson @tpazitny @kwiebers
/sites/handbook/source/handbook/engineering/quality/dev-qe-team/ @at.ramya
/sites/handbook/source/handbook/engineering/quality/ops-qe-team/ @jo_shih
/sites/handbook/source/handbook/engineering/quality/growth-qe-team/ @vincywilson
/sites/handbook/source/handbook/engineering/quality/secure-enablement-qe-team/ @tpazitny
/sites/handbook/source/handbook/engineering/quality/engineering-productivity-team/ @kwiebers
/sites/handbook/source/handbook/engineering/security/ @JohnathanHunt
/sites/handbook/source/handbook/engineering/security/operations/ @jurbanc
/sites/handbook/source/handbook/engineering/security/security-assurance/ @Julia.Lake
/sites/handbook/source/handbook/engineering/ux/ @clenneville @vkarnes @asmolinski2 @jackib @mvanremmerden @nudalova @jmandell @mikelong @tauriedavis

# Finance
/sites/handbook/source/handbook/finance/ @pmachle
/sites/handbook/source/handbook/finance/financial-planning-and-analysis/ @cmestel @wwright
/sites/handbook/source/handbook/finance/financial-planning-and-analysis/Sales-Finance/ @fkurniadi @alcurtis @ysun3
/sites/handbook/source/handbook/finance/financial-planning-and-analysis/Sales-Finance/Professional-Services-Finance/ @fkurniadi @sidmalik
/sites/handbook/source/handbook/spending-company-money/ @pmachle @daleb04 @jnguyen28

# Job Families
/source/job-families/ @sytses @streas
/sites/handbook/source/handbook/hiring/job-families/ @Ahoffbauer @streas @brittanyr

# Legal
/sites/handbook/source/handbook/people-group/code-of-conduct/ @rschulman
/sites/handbook/source/handbook/legal/ @rschulman
/sites/handbook/source/handbook/legal/global-compliance/ @rschulman
/source/includes/terms/consultancy.haml @rschulman
/source/includes/terms/edu-oss.haml @rschulman
/source/includes/terms/gitlab-com.haml @rschulman
/source/includes/terms/subscription.html.haml @rschulman
/source/privacy/ @rschulman
/source/terms/ @rschulman

# Marketing
/source/index.html.haml @dmor @tbarr
/data/analyst_reports.yml @cfletcher1 @Tompsett
/data/applications.yml @TinaS
/data/redirects.yml @shanerice @mnguyen4
/data/resources.yml @shanerice @jgragnola @erica
/data/webcasts.yml @shanerice @jgragnola @aoetama
/data/learn.yml @dangordon
/source/analysts/ @cfletcher1 @Tompsett
/source/compare/ @shanerice @mnguyen4 @jgragnola
/source/why/ @shanerice @mnguyen4 @jgragnola
/source/just-commit/ @shanerice @mnguyen4 @jgragnola
/source/community/ @dplanella @melsmo
/source/community/contribute/ @rpaik
/source/community/contributing-orgs/ @rpaik
/source/community/core-team/ @rpaik
/source/community/evangelists/ @johncoghlan
/source/community/hackathon/ @rpaik
/source/community/heroes/ @johncoghlan
/source/community/issue-bash/ @markglenfletcher
/source/community/meetups/ @johncoghlan
/source/community/mvp/ @rpaik
/source/community/top-annual-contributors/ @rpaik
/source/community/virtual-meetups/ @johncoghlan
/source/customers/ @cfletcher1 @KimLock
/sites/handbook/source/handbook/marketing/ @tbarr @melsmo @dmor
/sites/handbook/source/handbook/marketing/growth-marketing/ @dmor @rreich @erica @sbouchard1 @shanerice
/sites/handbook/source/handbook/marketing/growth-marketing/brand-and-digital-design/ @brandon_lyon @sbouchard1 @laurenbarker @mpreuss22
/sites/handbook/source/handbook/marketing/growth-marketing/content/ @erica @dmor
/sites/handbook/source/handbook/marketing/growth-marketing/content/content-marketing/ @erica @bmatturro
/sites/handbook/source/handbook/marketing/growth-marketing/content/editorial-team/ @rebecca @erica
/sites/handbook/source/handbook/marketing/growth-marketing/content/case-studies/ @bmatturro @erica
/sites/handbook/source/handbook/marketing/growth-marketing/content/content-hack-day/ @rebecca @erica
/sites/handbook/source/handbook/marketing/growth-marketing/content/digital-production/ @atflowers @erica
/sites/handbook/source/handbook/marketing/growth-marketing/inbound-marketing/ @shanerice @dmor
/sites/handbook/source/handbook/marketing/growth-marketing/inbound-marketing/analytics/ @shanerice @dmor
/sites/handbook/source/handbook/marketing/growth-marketing/inbound-marketing/testing/ @shanerice @brandon_lyon @dmor
/sites/handbook/source/handbook/marketing/campaigns/ @jgragnola @tbarr @melsmo @dmor
/sites/handbook/source/handbook/marketing/career-development/ @EWhelchel
/sites/handbook/source/handbook/marketing/community-relations/ @dplanella @melsmo
/sites/handbook/source/handbook/marketing/community-relations/code-contributor-program/ @rpaik @dplanella
/sites/handbook/source/handbook/marketing/community-relations/community-advocacy/ @bvanderkolk @dplanella
/sites/handbook/source/handbook/marketing/community-relations/education-program/ @c_hupy @dplanella
/sites/handbook/source/handbook/marketing/community-relations/evangelist-program/ @johncoghlan @dplanella
/sites/handbook/source/handbook/marketing/community-relations/opensource-program/ @nuritzi @dplanella
/sites/handbook/source/handbook/marketing/community-relations/program-resources/ @dplanella @melsmo
/sites/handbook/source/handbook/marketing/community-relations/project-management/ @dplanella @melsmo
/sites/handbook/source/handbook/marketing/community-relations/technical-evangelism/ @abuango @brendan @dnsmichi @dplanella
/sites/handbook/source/handbook/marketing/corporate-marketing/ @melsmo @dmurph @tbarr
/sites/handbook/source/handbook/marketing/corporate-marketing/social-marketing/ @wspillane @ksundberg
/sites/handbook/source/handbook/marketing/emergency-response/ @sdaily @dmor @melsmo @tbarr @rkohnke
/sites/handbook/source/handbook/marketing/events/ @Emily @Lconway @melsmo
/sites/handbook/source/handbook/marketing/localization/ @erica @lblanchard
/sites/handbook/source/handbook/marketing/marketing-operations/ @sdaily @dmor @tbarr @rkohnke
/sites/handbook/source/handbook/marketing/product-marketing/ @kuthiala @johnjeremiah @dangordon @williamchia
/sites/handbook/source/handbook/marketing/product-marketing/analyst-relations/ @cfletcher1 @Tompsett @rragozzine
/sites/handbook/source/handbook/marketing/product-marketing/competitive/ @kuthiala @mskumar
/sites/handbook/source/handbook/marketing/product-marketing/customer-reference-program/ @cfletcher1 @KimLock @FionaOKeeffe @jlparker
/sites/handbook/source/handbook/marketing/product-marketing/demo/ @dangordon
/sites/handbook/source/handbook/marketing/product-marketing/mrnci/ @cfletcher1
/sites/handbook/source/handbook/marketing/product-marketing/partner-marketing/ @TinaS
/sites/handbook/source/handbook/marketing/product-marketing/pmmteam/ @johnjeremiah @williamchia
/sites/handbook/source/handbook/marketing/product-marketing/usecase-gtm/version-control-collaboration/ @jordi_mon @warias
/sites/handbook/source/handbook/marketing/product-marketing/usecase-gtm/ci/ @parker_ennis @iganbaruch
/sites/handbook/source/handbook/marketing/product-marketing/usecase-gtm/devsecops/ @cblake @fjdiaz
/sites/handbook/source/handbook/marketing/product-marketing/usecase-gtm/simplify-devops/ @supadhyaya @dangordon
/sites/handbook/source/handbook/marketing/product-marketing/usecase-gtm/gitops/ @williamchia @csaavedra1
/sites/handbook/source/handbook/marketing/product-marketing/sales-resources/ @johnjeremiah @williamchia @dcsomers
/sites/handbook/source/handbook/marketing/product-marketing/technical-marketing/ @dangordon
/sites/handbook/source/handbook/marketing/project-management-guidelines/ @johnjeremiah @dplanella @jgragnola
/sites/handbook/source/handbook/marketing/readmes/ @dmurph
/sites/handbook/source/handbook/marketing/revenue-marketing/ @EWhelchel
/sites/handbook/source/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/ @jgragnola @dpduncan
/sites/handbook/source/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/direct-mail/ @jgragnola @lblanchard
/sites/handbook/source/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/ @jgragnola @nbsmith @mmmitchell
/sites/handbook/source/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/ @jgragnola @zbadgley @erica
/sites/handbook/source/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/landing-pages/ @jgragnola @brandon_lyon
/sites/handbook/source/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/surveys/ @jgragnola @lblanchard
/sites/handbook/source/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/ @aoetama @zbadgley @jgragnola
/sites/handbook/source/handbook/marketing/social-media-guidelines/ @wspillane @ksundberg
/sites/handbook/source/handbook/marketing/website/ @brandon_lyon @laurenbarker
/sites/handbook/source/handbook/marketing/blog/ @rebecca @erica
/sites/handbook/source/handbook/marketing/blog/unfiltered/ @rebecca @erica
/source/images/heroes/ @johncoghlan
/source/includes/customers/ @cfletcher1 @KimLock @FionaOKeeffe @jlparker
/source/includes/contact-sales-public-sector.html.haml @dmor @shanerice @sbouchard1
/source/includes/contact-sales.html.haml @dmor @shanerice @sbouchard1
/source/learn/ @dangordon
/source/resources/ @shanerice @jgragnola
/source/solutions/ @johnjeremiah
/source/solutions/dev-sec-ops/ @cblake
/source/solutions/education/ @c_hupy
/source/solutions/open-source/ @nuritzi

# Meltano
/sites/handbook/source/handbook/meltano/ @DouweM

# People
/sites/handbook/source/handbook/people-group/leadership-toolkit/ @jarmendariz @lwebster @jkalimon @cbednarz @rtakken @glucchesi
/sites/handbook/source/handbook/underperformance/ @cteskey @jarmendariz @lwebster @jkalimon @cbednarz @rtakken @glucchesi
/sites/handbook/source/handbook/people-group/ @cteskey @Tknudsen @Vatalidis @brittanyr @davegilbert @jarmendariz @jkalimon @cbednarz @Josh_Zimmerman @Ahoffbauer
/sites/handbook/source/handbook/people-group/contracts-and-international-expansion/ @Vatalidis @cgudgenov @rhendrick @hwangui
/sites/handbook/source/handbook/people-group/general-onboarding/ @brufener @ashjameson @mowry @alex_venter @lienvdsteen
/sites/handbook/source/handbook/people-group/promotions-transfers/ @cteskey @brufener @Vatalidis @brittanyr
/sites/handbook/source/handbook/people-group/offboarding/offboarding_guidelines/ @brufener @ashjameson @mowry @alex_venter @lienvdsteen
/sites/handbook/source/handbook/people-group/offboarding/ @cteskey @brufener @Vatalidis
/sites/handbook/source/handbook/people-group/employment-branding/ @Ahoffbauer @davegilbert @Apshegodskaya

# People Engineering
/sites/handbook/source/handbook/people-group/engineering/ @lienvdsteen @Vatalidis
/data/benefits_calculator.yml @lienvdsteen @brittanyr
/data/equity_calculator.yml @lienvdsteen @brittanyr
/source/frontend/components/salary-calculator/SalaryCalculator.vue @lienvdsteen @brittanyr
/source/frontend/components/salary-calculator/SalaryCalculatorBenefits.vue @lienvdsteen @brittanyr
/source/frontend/components/salary-calculator/SalaryCalculatorEquity.vue @lienvdsteen @brittanyr
/source/frontend/components/salary-calculator/BenefitDetail.vue @lienvdsteen @brittanyr

# Product
/data/categories.yml @sfwgitlab @adawar @sytses
/data/sections.yml @sfwgitlab @adawar @edjdev @sytses
/data/stages.yml @sfwgitlab @adawar @edjdev @sytses
/data/personas.yml @sfwgitlab @adawar
/data/application_types.yml @sfwgitlab @adawar
/source/direction/ @sfwgitlab @adawar @sytses @fseifoddini
/sites/handbook/source/handbook/product/ @sfwgitlab @adawar @sytses @fseifoddini @kokeefe
/sites/handbook/source/handbook/product-development-flow/ @sfwgitlab @adawar @sytses @fseifoddini
/sites/handbook/source/handbook/product/performance-indicators/ @sfwgitlab @fseifoddini @adawar
/sites/handbook/source/handbook/product/product-manager-role/ @sfwgitlab @adawar
/sites/handbook/source/handbook/product/product-categories/ @edjdev @sfwgitlab @adawar @sytses

# Product - Dev Section
/source/direction/dev/ @ebrinkman @gl-product-leadership
/source/direction/dev/strategies/ @ebrinkman @jeremy @justinfarris @jramsay @gl-product-leadership
/source/direction/manage/ @gl-product-leadership @ebrinkman @jeremy
/source/direction/manage/auth/ @gl-product-leadership @ebrinkman @jeremy @mushakov
/source/direction/manage/subgroups/ @gl-product-leadership @ebrinkman @jeremy @mushakov
/source/direction/manage/users/ @gl-product-leadership @ebrinkman @jeremy @mushakov
/source/direction/manage/audit-events/ @gl-product-leadership @ebrinkman @jeremy @mattgonzales
/source/direction/manage/audit-reports/ @gl-product-leadership @ebrinkman @jeremy @mattgonzales
/source/direction/manage/compliance-frameworks/ @gl-product-leadership @ebrinkman @jeremy @mattgonzales
/source/direction/manage/compliance-management/ @gl-product-leadership @ebrinkman @jeremy @mattgonzales
/source/direction/manage/compliance-controls/ @gl-product-leadership @ebrinkman @jeremy @mattgonzales
/source/direction/manage/importers/ @gl-product-leadership @ebrinkman @jeremy @hdelalic
/source/direction/manage/internationalization/ @gl-product-leadership @ebrinkman @jeremy @hdelalic
/source/direction/manage/templates/ @gl-product-leadership @ebrinkman @jeremy @hdelalic
/source/direction/manage/devops-reports/ @gl-product-leadership @ebrinkman @jeremy @jshackelford
/source/direction/manage/code-analytics/ @gl-product-leadership @ebrinkman @jeremy @jshackelford
/source/direction/manage/planning-analytics/ @gl-product-leadership @ebrinkman @jeremy @jshackelford
/source/direction/manage/value_stream_management/ @gl-product-leadership @ebrinkman @jeremy @jshackelford
/source/direction/plan/issue_tracking/ @gl-product-leadership @ebrinkman @justinfarris @gweaver
/source/direction/plan/kanban_boards/ @gl-product-leadership @ebrinkman @justinfarris @gweaver
/source/direction/plan/time_tracking/ @gl-product-leadership @ebrinkman @justinfarris @gweaver
/source/direction/plan/jira_importer/ @gl-product-leadership @ebrinkman @justinfarris @gweaver
/source/direction/plan/project_management/ @gl-product-leadership @ebrinkman @justinfarris @gweaver
/source/direction/plan/portfolio_management/ @gl-product-leadership @ebrinkman @justinfarris @kokeefe
/source/direction/plan/epics/ @gl-product-leadership @ebrinkman @justinfarris @kokeefe
/source/direction/plan/roadmaps/ @gl-product-leadership @ebrinkman @justinfarris @kokeefe
/source/direction/plan/quality_management/ @gl-product-leadership @ebrinkman @justinfarris @mjwood
/source/direction/plan/requirements_management/ @gl-product-leadership @ebrinkman @justinfarris @mjwood
/source/direction/plan/service_desk/ @gl-product-leadership @ebrinkman @justinfarris @mjwood
/source/direction/create/source_code_management/ @gl-product-leadership @ebrinkman @jramsay @danielgruesso
/source/direction/create/code_review/ @gl-product-leadership @ebrinkman @jramsay @danielgruesso
/source/direction/create/web_ide/ @gl-product-leadership @ebrinkman @jramsay
/source/direction/create/snippets/ @gl-product-leadership @ebrinkman @jramsay
/source/direction/create/live_preview/ @gl-product-leadership @ebrinkman @jramsay
/source/direction/create/editor_extension/ @gl-product-leadership @ebrinkman @jramsay
/source/direction/create/gitaly/ @gl-product-leadership @ebrinkman @jramsay
/source/direction/create/design_management/ @gl-product-leadership @ebrinkman @cdybenko
/source/direction/create/design_system/ @gl-product-leadership @ebrinkman @cdybenko
/source/direction/create/wiki/ @gl-product-leadership @ebrinkman @cdybenko
/source/direction/create/static_site_editor/ @gl-product-leadership @ebrinkman @ericschurter
/source/direction/create/gitlab_handbook/ @gl-product-leadership @ebrinkman @ericschurter
/source/direction/create/gitlab_docs/ @gl-product-leadership @ebrinkman @ericschurter
/source/direction/create/gitter/ @gl-product-leadership @ebrinkman @ericschurter
/source/direction/create/ecosystem/ @gl-product-leadership @ebrinkman @deuley

# Product Section - Sec
/source/direction/security/ @david @gl-product-leadership
/source/direction/secure/ @david @gl-product-leadership
/source/direction/secure/static-analysis/sast/ @david @tmccaslin
/source/direction/secure/static-analysis/secret-detection/ @david @tmccaslin
/source/direction/secure/dynamic-analysis/dast/ @david @derekferguson
/source/direction/secure/dynamic-analysis/pki-management/ @david @derekferguson
/source/direction/secure/dynamic-analysis/iast/ @david @derekferguson
/source/direction/secure/fuzz-testing/fuzz-testing/ @david @stkerr
/source/direction/secure/vulnerability-research/security-benchmarking/ @david @stkerr
/source/direction/secure/vulnerability-research/vulnerability-database/ @david @stkerr
/source/direction/secure/composition-analysis/dependency-scanning/ @david @NicoleSchwartz
/source/direction/secure/composition-analysis/container-scanning/ @david @NicoleSchwartz
/source/direction/secure/composition-analysis/license-compliance/ @david @NicoleSchwartz
/source/direction/secure/vulnerability_management/ @david @matt_wilson
/source/direction/defend/ @david @gl-product-leadership
/source/direction/defend/web_application_firewall/ @david @sam.white
/source/direction/defend/container_host_security/ @david @sam.white
/source/direction/defend/container_network_security/ @david @sam.white
/source/direction/defend/ueba/ @david @matt_wilson @sam.white

# Product Section - Other
/source/direction/ops/ @kencjohnston @gl-product-leadership
/source/direction/growth/ @hilaqu @gl-product-leadership
/source/direction/enablement/ @joshlambert @gl-product-leadership
/source/direction/monitor/ @kbychu @sarahwaldner @dhershkovitch
/source/direction/configure/ @kbychu @nagyv-gitlab

# Recruiting
/sites/handbook/source/handbook/hiring/ @Ahoffbauer @davegilbert @Apshegodskaya
/sites/handbook/source/handbook/hiring/recruiting-framework/ @Ahoffbauer @asjones @Apshegodskaya @davegilbert
/source/jobs/ @Ahoffbauer @davegilbert @Apshegodskaya
/source/job-families/people-ops/recruiting-operations-insights/  @sytses @streas @Ahoffbauer @davegilbert
/source/job-families/people-ops/recruiter/  @sytses @streas @davegilbert @Ahoffbauer @Apshegodskaya
/source/job-families/people-ops/recruiting-sourcer/  @sytses @streas @davegilbert @Apshegodskaya @Ahoffbauer
/source/job-families/people-ops/candidate-experience/  @sytses @streas @davegilbert @Ahoffbauer @asjones

# Sales
/sites/handbook/source/handbook/sales/ @jbrennan1
/sites/handbook/source/handbook/sales/commercial/  @aasimkhan @chrisanderton @hmason @ja-me-sk @jpotter1 @kshirazi @mhennessy @ronell
/sites/handbook/source/handbook/sales/territories/ @james_harrison @lschoenfeld

# Security
/sites/handbook/source/handbook/security/ @JohnathanHunt

# Security Compliance
/source/security/ @Julia.Lake

# Support
/sites/handbook/source/handbook/support/ @gitlab-com/support
/sites/handbook/source/handbook/support/workflows/how-to-set-up-shift-coverage-for-an-event.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/administration.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/discontinued_plans.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/account_deactivation.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/cmoc_workflows.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/dormant_username_policy.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/service_desk_troubleshooting.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/unverified_email_requests.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/rbl_delisting.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/sending_notices.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/ip-blocks.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/account_verification.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/account_changes.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/internal_requests.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/hard_delete_project.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/importing_projects.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/500_errors.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/managing_spam.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/confirmation_emails.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/admin_note.html.md @gitlab-com/support/dotcom
/sites/handbook/source/handbook/support/workflows/gdpr_article-15.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/data_processing_addendums.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/information-request.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/log_requests.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/gdpr_account_deletion.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/pii_removal_requests.html.md @gitlab-com/support/managers @gitlab-com/gl-security/abuse-team
/sites/handbook/source/handbook/support/workflows/dmca.html.md @gitlab-com/support/managers
/sites/handbook/source/handbook/support/workflows/acceptable_use_violations.html.md @gitlab-com/support/managers @gitlab-com/gl-security/abuse-team
/sites/handbook/source/handbook/support/workflows/reinstating-blocked-accounts.html.md @gitlab-com/support/managers @gitlab-com/gl-security/abuse-team
/sites/handbook/source/handbook/support/workflows/working_with_security.html.md @gitlab-com/support/managers @gitlab-com/gl-security
/sites/handbook/source/handbook/support/workflows/zendesk_admin.html.md @gitlab-com/support/managers @gitlab-com/support/support-ops
/sites/handbook/source/handbook/support/workflows/unbabel_translation_in_zendesk.html.md @gitlab-com/support/managers @gitlab-com/support/support-ops
/sites/handbook/source/handbook/support/workflows/zendesk-automations.html.md @gitlab-com/support/managers @gitlab-com/support/support-ops
/sites/handbook/source/handbook/support/workflows/setting_ticket_priority.html.md @gitlab-com/support/managers @gitlab-com/support/support-ops
/sites/handbook/source/handbook/support/workflows/zendesk-instances.html.md @gitlab-com/support/managers @gitlab-com/support/support-ops
/sites/handbook/source/handbook/support/workflows/zendesk-triggers.html.md @gitlab-com/support/managers @gitlab-com/support/support-ops

# Technical Writing
/sites/handbook/source/handbook/engineering/ux/technical-writing/ @susantacker @cnorris
/sites/handbook/source/handbook/documentation/ @susantacker @cnorris
/source/job-families/engineering/technical-writer/ @susantacker @cnorris
/source/job-families/engineering/technical-writing-manager/ @susantacker @cnorris
/sites/handbook/source/handbook/style-guide/ @susantacker @cnorris @mikelewis
/sites/handbook/source/handbook/markdown-guide/ @marcia @mikelewis @cnorris
/sites/handbook/source/includes/stages/tech-writing.html.haml @marcia

# Releases blog posts
/source/releases/posts/ @gitlab-org/delivery, @gl-product-leadership

# Delivery team
/data/release_managers.yml @gitlab-org/delivery
